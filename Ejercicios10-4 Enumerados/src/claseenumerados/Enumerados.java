package claseenumerados;

public class Enumerados {

	public static void main(String[] args) {
		// tipo datos nombre variable
		Transporte tp;
		tp = Transporte.AVION;
		System.out.println("Valor inicial del ENUM transporte " + tp);
		System.out.println();
		System.out.println("Asigno un nuevo valor al ENUM");
		tp = Transporte.TREN;
		System.out.println("Nuevo Valor del ENUM transporte " + tp);

		System.out.println();
		System.out.println("Comparación de dos valores ENUM");
		if (tp == Transporte.TREN) {
			System.out.println("ENUM tiene el valor TREN");
		}

		System.out.println();
		System.out.println("Uso de un ENUM en un switch");
		switch (tp) {
		case COCHE:
			System.out.println("Elegido el valor coche");
			break;
		case CAMION:
			System.out.println("Elegido el valor camion");
			break;
		case AVION:
			System.out.println("Elegido el valor avion");
			break;
		case TREN:
			System.out.println("Elegido el valor tren");
			break;
		case BARCO:
			System.out.println("Elegido el valor barco");
			break;

		}

	}

}
