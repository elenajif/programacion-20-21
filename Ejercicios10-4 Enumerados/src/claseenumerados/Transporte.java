package claseenumerados;

// sirven para representar grupos de constantes
// se le da un nombre y define un tipo de datos

public enum Transporte {
	COCHE, CAMION, AVION, TREN, BARCO;
}
