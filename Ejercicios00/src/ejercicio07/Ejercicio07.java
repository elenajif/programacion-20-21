package ejercicio07;

public class Ejercicio07 {

	public static void main(String[] args) {
		// Declarar numero1 entero y darle un valor
		int numero1 = 34;
		// Mostrar numero1
		System.out.println("numero1 " + numero1);
		// Declarar numero2 entero, numero2 ser� numero1 + un numero entero cualquiera
		int numero2;
		numero2 = numero1 + 25;
		// Mostrar numero2
		System.out.println("numero2 " + numero2);
		// Declarar numero3 entero, numero3 ser� numero1 � un numero entero cualquiera
		int numero3;
		numero3 = numero1 - 2;
		// Mostrar numero3
		System.out.println("numero3 " + numero3);
		// Declarar numero4 entero y darle un valor
		int numero4 = 40;
		// Mostrar numero4
		System.out.println("numero4 " + numero4);
		// Incrementar con el operador += un numero entero cualquiera
		numero4 += 4; // numero4=numero4+4
		// Mostrar numero4
		System.out.println("numero4 " + numero4);
		// Declarar numero5 entero y darle un valor
		int numero5 = 325;
		// Mostrar numero5
		System.out.println("numero5 " + numero5);
		// Decrementar con el operador -= un numero entero cualquiera
		numero5 -= 25;
		// Mostrar numero5
		System.out.println("numero5 " + numero5);
		// Declarar numero6 entero y darle un valor
		int numero6 = 15;
		// Mostrar numero6
		System.out.println("numero6 " + numero6);
		// Multiplicar con el operador *= un numero entero cualquiera
		numero6 *= 2;
		// Mostrar numero6
		System.out.println("numero6 " + numero6);
		// Declarar numero7 entero y darle un valor
		int numero7 = 10;
		// Mostrar numero7
		System.out.println("numero7 " + numero7);
		// Dividir con el operador /= un numero entero cualquiera
		numero7 /= 5;
		// Mostrar numero7
		System.out.println("numero7 " + numero7);
		// Declarar numero8 entero y darle un valor
		int numero8 = 50;
		// Mostrar numero8
		System.out.println("numero8 " + numero8);
		// Aumentar su valor con operador ++
		numero8++;
		// Mostrar numero8
		System.out.println("numero8 " + numero8);
		// Declarar numero9 entero y darle un valor
		int numero9 = 100;
		// Mostrar numero9
		System.out.println("numero9 " + numero9);
		// Disminuir su valor con operador --
		numero9--;
		// Mostrar numero9
		System.out.println("numero9 " + numero9);
	}

}
