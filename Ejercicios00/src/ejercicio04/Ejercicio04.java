package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {
		// variable tipo byte
		byte numero = 126;
		System.out.println("La variable byte es " + numero);
		// variable tipo short
		short numero1 = 20000;
		System.out.println("La variable byte es " + numero1);
		// variable tipo int
		int numero2 = 3;
		System.out.println("La variable entera es " + numero2);
		// variable tipo long
		long numero3 = 22222222288888L;
		System.out.println("La variable char es " + numero3);
		// variable tipo float
		float numero4 = 55.6F;
		System.out.println("La variable float es " + numero4);
		// variable tipo double
		double numero5 = 22.5;
		System.out.println("La variable char es " + numero5);
		// variable tipo char
		char letrica = 'b';
		System.out.println("La variable char es " + letrica);
		// variable tipo boolean
		boolean valorBooleano = true;
		System.out.println("La variable booleana es " + valorBooleano);
		// variable tipo String
		String cadena = "hola";
		System.out.println("La variable String es " + cadena);

	}

}
