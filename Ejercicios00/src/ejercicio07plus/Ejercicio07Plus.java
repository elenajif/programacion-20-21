package ejercicio07plus;

public class Ejercicio07Plus {

	public static void main(String[] args) {
		System.out.println("caso1");
		//declaramos una variable y le damos el valor 1
		int numero1 = 1;
		System.out.println("numero1 "+numero1);
		//declaramos otra variable y le damos el valor que tiene numero1
		//despu�s le sumamos 1 (numero1++)
		int numero2 = numero1 ++; 
		//int numero2=numero1; 
		//numero1++;
		//primero asigno, despu�s incremento
		System.out.println("numero1 "+numero1);
		System.out.println("numero2 "+numero2);
		 
		System.out.println("caso2");
		//Asigno a numero1 el valor 1
		numero1 = 1;
		System.out.println("numero1 "+numero1);
		//restamos 1 a numero1 y asignamos numero1 a numero2
		numero2 = --numero1;
		//--numero1;
		//numero2=numero1;
		//primero decremento y despues asigno
		System.out.println("numero1 "+numero1);
		System.out.println("numero2 "+numero2);
		

	}

}
