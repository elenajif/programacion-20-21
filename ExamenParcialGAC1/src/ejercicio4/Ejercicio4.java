package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("1.- Comprobar un numero");
		System.out.println("2.- Comprobar un caracter");
		System.out.println("3.- Salir");

		int opcion = input.nextInt();

		if (opcion >= 1 && opcion <= 3) {
			switch (opcion) {
			case 1:
				System.out.println("Introduce un n�mero entero");
				int entero = input.nextInt();
				if (entero < 0) {
					System.out.println("El numero es negativo");
				} else if (entero % 2 == 0) {
					System.out.println("Es par");
				} else {
					System.out.println("Es impar");
				}
				break;
			case 2:
				input.nextLine();
				System.out.println("Introduce una cadena");
				String cadena = input.nextLine();
				if (cadena.length() != 1) {
					System.out.println("Has introducido m�s de un caracter");
				} else {
					int caracterAscii = (int) cadena.charAt(0);

					if (caracterAscii >= 65 && caracterAscii <= 90) {
						System.out.println("Es may�scula");
					} else if (caracterAscii >= 97 && caracterAscii <= 122) {
						System.out.println("Es min�scula");
					} else {
						System.out.println("Es un n�mero");
					}
				}
			}
		} else {
			System.out.println("La opci�n elegida no es correcta, programa terminado");
		}

		input.close();

	}

}

