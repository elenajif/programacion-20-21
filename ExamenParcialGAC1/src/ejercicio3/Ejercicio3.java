package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		String nombre;
		String clave;
		String nombrePrograma="juan";
		String clavePrograma="123456";

		System.out.println("Introduce Nombre de usuario: ");
		nombre = entrada.nextLine();
		System.out.println("Introduce Clave de usuario: ");
		clave = entrada.nextLine();

		if (nombre.equals(nombrePrograma) && clave.equals(clavePrograma)) {
			System.out.println("Bienvenido al Sistema");
		} else {
			System.out.println("Nombre de Usuario o contraseņa Incorrecto");
		}
		entrada.close();

	}
}