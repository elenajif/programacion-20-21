package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
        Scanner entrada=new Scanner(System.in);
        int numero;

        System.out.print("Dame un numero: "); 
        numero=entrada.nextInt();
        
        if(numero < 10){
            System.out.println("El numero "+numero+" tiene 1 cifra");
        }
        else if(numero<100){
            System.out.println("El numero "+numero+" tiene 2 cifras");
        }
        else if(numero < 1000){
            System.out.println("El numero "+numero+" tiene 3 cifras");
        }
        else if(numero < 10000){
            System.out.println("El numero "+numero+" tiene 4 cifras");
        }
        else if(numero < 100000){
            System.out.println("El numero "+numero+" tiene 5 cifras");
        }
        entrada.close();
    }
}
