package ejercicio03;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine();
		
		System.out.println("Introduce una ruta");
		String ruta = input.nextLine();
		
		input.close();
		
		try {
			
			escribeFichero(cadena, ruta);
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
	}

	private static void escribeFichero(String cadena, String ruta) throws FileNotFoundException {
		
		PrintWriter escritor = new PrintWriter(ruta);
		escritor.println(cadena);
		escritor.close();
	}

}
