package ejercicio06;

import java.text.ParseException;
import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una fecha en formato dia/mes/a�o");
		String fecha = input.nextLine();
		input.close();

		long cantidadDias;
		
		try {
			
			cantidadDias = cantidadDias(fecha);
			System.out.println(cantidadDias);
			
		} catch (ParseException e) {
			
			System.out.println("No se ha podido parsear la fecha");
			e.printStackTrace();
		}
		
		
	}

	//En este m�todo quiero lanzar �nicamente una sola excepcion, 
	//en caso de que la fecha no est� en formato correcto
	private static long cantidadDias(String fecha) throws ParseException {
		String[] fechaDividida = fecha.split("/");
		//Controlo que la fecha tiene 3 partes separadas por /
		if(fechaDividida.length != 3){
			throw new ParseException("fecha incorrecta: el formato es dia/mes/anno", 0);
		}
		
		int dias = 0;
		int meses = 0;
		int annos = 0;
		
		//Compruebo si los meses dias y annos son enteros
		//Como solo quiero lanzar mi excepcion, controlo las otras posibles
		try{
			dias = Integer.parseInt(fechaDividida[0]);
			meses = Integer.parseInt(fechaDividida[1]);
			annos = Integer.parseInt(fechaDividida[2]);
		
		}catch(NumberFormatException e){
			//Si no son numeros enteros, lanzo mi excepcion
			throw new ParseException("fecha incorrecta: dia, mes y anno deben ser numeros enteros", 0);
		
		}
		
		if(dias > 0 && dias <= 30 
				&& meses > 0 && meses <= 12
				&& annos > 0){
			
			long diasDiferencia = dias + meses * 30 + annos * 360 - 1;
			return diasDiferencia;
		}
		//Si el rango no es apropiado, lanzo mi excepcion
		throw new ParseException("fecha incorrecta: dia(1-30), mes(1-12), anno(>0)", 0);
	}

}
