package ejercicio07;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Indica la ruta del fichero");
		String ruta = input.nextLine();
		
		File fichero = new File(ruta);
		
		String cadenaLeida;
		do{
			System.out.println("Introduce una cadena");
			cadenaLeida = input.nextLine();
			
			try {
				annadeAFichero(cadenaLeida, fichero);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}while(!cadenaLeida.equals("fin"));
		
		//obtengo la fecha de hoy y la anado al fichero
		String fecha =  LocalDate.now().toString();
		try {
			annadeAFichero(fecha, fichero);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		input.close();
		

	}

	private static void annadeAFichero(String cadena, File fichero) throws IOException   {
		FileWriter escritor1 = new FileWriter(fichero, true);
		escritor1.append(cadena + "\n");
		escritor1.close();
	}

}
