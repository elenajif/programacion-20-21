package programa;

import clases.Biblioteca;

public class Programa {

	public static void main(String[] args) {
		Biblioteca biblio1 = new Biblioteca();
		
		System.out.println("Alta socios");
		biblio1.altaSocio("Jesus");
		biblio1.altaSocio("Andres");

		System.out.println("Alta articulos");
		biblio1.altaArticulo("11111", "Cien a�os de soledad", "Planeta", "Gabriel Garcia Marquez");
		biblio1.altaArticulo("22222", "Del amor y otros demonios", "Planeta", "Gabriel Garcia Marquez");
		biblio1.altaArticulo("33333", "revista3", "Science", true);
		biblio1.altaArticulo("44444", "Revista4", "Historia", false);
		
		System.out.println("Crear prestamos / introducir articulos prestamos");
		biblio1.crearPrestamoSocio(1);
		biblio1.crearPrestamoSocio(2);
		biblio1.introducirArticulosPrestamo(1, "11111");
		biblio1.introducirArticulosPrestamo(2, "33333");
		
		System.out.println();
		System.out.println("Listamos articulos");
		biblio1.listarArticulos();
		
		System.out.println();
		System.out.println("Mostramos prestamos socios");
		biblio1.mostrarPrestamosSocio(1);
		System.out.println("Listamos prestamos");
		biblio1.listarPrestamos();
		
		System.out.println();
		System.out.println("Guardando datos....");
		biblio1.guardarDatos();
		System.out.println("Cargando datos....");
		biblio1.cargarDatos();

	}

}
