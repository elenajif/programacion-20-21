package clases;

import java.io.Serializable;

public class Revistas extends Articulos implements Serializable {

	/**
	 * clase Revistas implementa Serializable, hereda de Articulos 
	 */
	private static final long serialVersionUID = 1L;
	public final static int PUNTOS=2;
	
	private String editorial;
	private boolean online;
	
	public Revistas(String isbn, String titulo, String editorial, boolean online) {
		super(isbn, titulo);
		this.editorial=editorial;
		this.online=online;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	@Override
	public int compareTo(Articulos o) {
		return getIsbn().compareTo(o.getIsbn());
	}

	@Override
	public int calcularPuntos() {
		int totalPuntos=0;
		totalPuntos=totalPuntos+PUNTOS;
		return totalPuntos;
	}

	@Override
	public String toString() {
		return super.toString()+"\n"+
				"Revistas [editorial=" + editorial + ", online=" + online + "]";
	}

}
