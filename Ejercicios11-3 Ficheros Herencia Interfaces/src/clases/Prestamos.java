package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Prestamos implements Serializable{

	/**
	 * clase prestamos implementa serializable
	 */
	private static final long serialVersionUID = 1L;
	
	private int idPrestamo;
	private LocalDate fechaPrestamo;
	private LocalDate fechaDevolucion;
	private Socios socio;
	protected ArrayList<Articulos> listaArticulos;
	
	public Prestamos(int idPrestamo, LocalDate fechaPrestamo, LocalDate fechaDevolucion, Socios socio) {
		this.idPrestamo=idPrestamo;
		this.fechaPrestamo=fechaPrestamo;
		this.fechaDevolucion=fechaDevolucion;
		this.socio=socio;
		listaArticulos=new ArrayList<Articulos>();
	}
	
	public int getIdPrestamo() {
		return idPrestamo;
	}
	public void setIdPrestamo(int idPrestamo) {
		this.idPrestamo = idPrestamo;
	}
	public LocalDate getFechaPrestamo() {
		return fechaPrestamo;
	}
	public void setFechaPrestamo(LocalDate fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}
	public LocalDate getFechaDevolucion() {
		return fechaDevolucion;
	}
	public void setFechaDevolucion(LocalDate fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	public Socios getSocio() {
		return socio;
	}
	public void setSocio(Socios socio) {
		this.socio = socio;
	}
	public ArrayList<Articulos> getListaArticulos() {
		return listaArticulos;
	}
	public void setListaArticulos(ArrayList<Articulos> listaArticulos) {
		this.listaArticulos = listaArticulos;
	}

	public int calcularPuntos() {
		int puntos=0;
		for (Articulos articulos: listaArticulos) {
			puntos+=articulos.calcularPuntos();
		}
		return puntos;
	}
	
	//comprueba que no haya articulos con ese isbn
	public boolean comprobarArticulo(String isbn) {
		for (Articulos articulo: listaArticulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Prestamos [idPrestamo=" + idPrestamo + ", fechaPrestamo=" + fechaPrestamo + ", fechaDevolucion="
				+ fechaDevolucion + ", socio=" + socio + ", listaArticulos=" + listaArticulos + "]";
	}

}
