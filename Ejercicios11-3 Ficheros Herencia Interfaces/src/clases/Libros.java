package clases;

import java.io.Serializable;

public class Libros extends Articulos implements Serializable {

	/**
	 * clase Libros implementa Serializable, hereda de Articulos
	 */
	private static final long serialVersionUID = 1L;
	public final static int PUNTOS = 5;
	private String editorial;
	private String autor;

	public Libros(String isbn, String titulo, String editorial, String autor) {
		super(isbn, titulo);
		this.editorial = editorial;
		this.autor = autor;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	@Override
	public int compareTo(Articulos arg0) {
		return getIsbn().compareTo(arg0.getIsbn());
	}

	@Override
	public int calcularPuntos() {
		int totalPuntos = 0;
		totalPuntos = totalPuntos + PUNTOS;
		return totalPuntos;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "Libros [editorial=" + editorial + ", autor=" + autor + "]";
	}

}
