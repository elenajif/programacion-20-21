package ejercicio02;

import java.util.Scanner;

public class Metodos02 {

	static Scanner input = new Scanner(System.in);

	public static double[] rellenarVector() {
		System.out.println("Rellenar vector");
		double vector[] = new double[5];
		for (int i = 0; i < vector.length; i++) {
			System.out.println("Introduce una nota");
			vector[i] = input.nextDouble();
		}
		return vector;
	}

	public static void contarAprobadosSuspendidos(double[] vector) {
		System.out.println("Contar aprobados, suspendidos");
		int aprobados = 0;
		int suspendidos = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] >= 5) {
				aprobados++;
			} else {
				suspendidos++;
			}
		}
		System.out.println("Total aprobados " + aprobados);
		System.out.println("Total suspendidos " + suspendidos);
	}

	public static void ordenarVector(double[] vector) {
		System.out.println("Vector sin ordenar");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector[i] + " ");
		}
		System.out.println("");

		for (int i = 0; i < vector.length - 1; i++) {
			for (int j = i + 1; j < vector.length; j++) {
				if (vector[j] < vector[i]) {
					double aux = vector[i];
					vector[i] = vector[j];
					vector[j] = aux;
				}
			}
		}

		System.out.println("Vector ordenado");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector[i] + " ");
		}
		System.out.println("");
	}

	public static void visualizarMaximoPar(double[] vector) {
		System.out.println("Maximo par");
		int vector1[] =new int[10];
		for (int i=0;i<vector.length;i++) {
			vector1[i]=(int)Math.round(vector[i]);
		}
		
		System.out.println("Muestro vector enteros");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector1[i] + " ");
		}
		
		int maximo=vector1[0];
		int posicion=0;
		
		for (int i = 0; i < vector.length; i++) {
			if (vector1[i]%2==0) {
				if (vector1[i]>maximo) {
					maximo=vector1[i];
					posicion=i;
				}
			}
		}
		System.out.println("El maximo par es "+maximo);
		System.out.println("La posicion es "+posicion);
	}
}
