package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		double[] vector = Metodos02.rellenarVector();

		int opcion;

		do {
			System.out.println("1.- Contar aprobados, suspendidos");
			System.out.println("2.- Ordenar vector de menor a mayor");
			System.out.println("3.- Visualizar maximo par");
			System.out.println("4.- Salir");

			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				Metodos02.contarAprobadosSuspendidos(vector);
				break;
			case 2:
				Metodos02.ordenarVector(vector);
				break;
			case 3:
				Metodos02.visualizarMaximoPar(vector);
				break;
			case 4:
				System.out.println("Programa finalizado");
				System.exit(0);
				break;
			default:
				System.out.println("Error de introducción de datos");
			}

		} while (opcion != 4);

		input.close();

	}

}
