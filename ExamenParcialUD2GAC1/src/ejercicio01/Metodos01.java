package ejercicio01;

import java.util.Scanner;

public class Metodos01 {
	
	static Scanner input = new Scanner(System.in);
	
	public static String[] rellenarVector() {
		System.out.println("Rellenar vector");
		String frases[] = new String[5];
		for (int i=0;i<frases.length;i++) {
			System.out.println("Introduce la frase "+i);
			frases[i]=input.nextLine();
		}
		return frases;
	}
	
	public static void visualizarPalabrasLetras(String[] frases) {
		System.out.println("Visualizar palabras letras");
		System.out.println("Introduce la componente");
		int componente=input.nextInt();
		System.out.println("La frase de la componente "+componente+ " es "+frases[componente]);
		
		int cantidadDeEspacios=0;
		for (int i=0;i<frases[componente].length()-1;i++) {
			if (frases[componente].charAt(i)==' ') {
				cantidadDeEspacios++;
			}
		}
		
		System.out.println("Sus palabras y letras son: ");
		for (int i=0;i<cantidadDeEspacios+1;i++) {
			String[] partes=frases[componente].split(" ");
			System.out.println(partes[i]+","+partes[i].length()+ " letras");
		}
	}
	
	public static void fraseMasLarga(String[] frases) {
		System.out.println("Frase mas larga");
		int longitudMaxima=0;
		int posicion=0;
		for (int i=0;i<frases.length;i++) {
			if (frases[i].length()>longitudMaxima) {
				longitudMaxima=frases[i].length();
				posicion=i;
			}
		}
		System.out.println("La longitud maxima es "+longitudMaxima);
		System.out.println("La posision es "+posicion);
		
	}

}
