package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Introduce el n�mero de veces que introducir�s n�meros");
		int veces = in.nextInt();
		double suma = 0;
		for (int i = 0; i < veces; i++)

		{
			System.out.println("Introduce un n�mero");
			int numero = in.nextInt();
			if (numero % 3 == 0) {
				System.out.println("El numero es m�ltiplo de 3");
			} else {
				System.out.println("El numero no es m�ltiplo de 3");
			}
			suma += numero;
		}

		System.out.println("La suma de todos los numeros es: " + suma);
		in.close();

	}
}
