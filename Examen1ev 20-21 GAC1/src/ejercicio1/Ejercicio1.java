package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double sumaNotas = 0;
		double nota;
		int cantidadNotas = 0;

		do{
			System.out.println("Introduce nota del alumno");
			nota = input.nextDouble();
			
			if(nota >= 0){
				sumaNotas = sumaNotas + nota;
				cantidadNotas++;
			}
			
		}while(nota >= 0);
		
		if(cantidadNotas != 0){
			System.out.println("La media es: " + (sumaNotas/cantidadNotas));
		}		
		input.close();
	}

}
