package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int opcion = 0;
		do {
			System.out.println("Men� de opciones");
			System.out.println("1.- Sueldo mas alto");
			System.out.println("2.- Contrase�a correcta");
			System.out.println("3.- Salir");
			System.out.println("Elegir opci�n");
			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {
			case 1:
				System.out.println("Introduce la cantidad de sueldos a pedir");
				int cantidadSueldos = input.nextInt();
				double mayor = 0.0;
				for (int i = 0; i < cantidadSueldos; i++) {
					System.out.println("Introduce un sueldo (" + (i + 1) + ")");
					double sueldo = input.nextDouble();
					if (i == 0) {
						mayor = sueldo;
					} else if (sueldo > mayor) {
						mayor = sueldo;
					}
					System.out.println("El sueldo m�s alto es "+mayor);
				}
				break;
			case 2:
				System.out.println("Dame una contrase�a");
				String password = input.nextLine().toLowerCase();
				boolean vocales = false;
				boolean letras = false;
				for (int i = 0; i < password.length(); i++) {
					if (password.charAt(i) == 'a' || password.charAt(i) == 'e' || password.charAt(i) == 'i'
							|| password.charAt(i) == 'o' || password.charAt(i) == 'u') {
						vocales = true;
					}
					if (password.charAt(i) >= 'a' && password.charAt(i) <= 'z') {
						letras = true;
					}
				}
				if (vocales == true && letras == true) {
					System.out.println("Contrase�a correcta");
				} else {
					System.out.println("Contrase�a incorrecta");
				}
				break;
			case 3:
				System.out.println("Salir");
				break;
			default:
				System.out.println("Opci�n incorrecta");
				break;
			}

		} while (opcion != 3);
		input.close();
	}

}
