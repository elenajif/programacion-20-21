package programa;

import clases.Zoo;

public class Programa {

	public static void main(String[] args) {
		System.out.println("1.- creo instancia de zoo");
		int maxAnimales=4;
		Zoo miZoo = new Zoo(maxAnimales);
		
		System.out.println("2.- Doy de alta 4 animales");
		miZoo.altaAnimal("Tim�n", 10, "Suricato", "zoo1");
		miZoo.altaAnimal("Pumba", 30, "Fac�quero", "zoo1");
		miZoo.altaAnimal("Simba", 60, "Le�n", "zoo1");
		miZoo.altaAnimal("Kowalski", 20, "Pung�ino", "zoo2");
		
		System.out.println("3.- Listar animales");
		miZoo.listarAnimales();
		
		System.out.println("4.- Buscar un animal por su nombre");
		System.out.println(miZoo.buscarAnimal("Tim�n"));
		
		System.out.println("5.- Eliminar un animal");
		miZoo.eliminarAnimal("Simba");
		miZoo.listarAnimales();
		
		System.out.println("6.- Almacenamos un nuevo animal");
		miZoo.altaAnimal("Dori", 1, "pez cirujano", "zoo2");
		miZoo.listarAnimales();
		
		System.out.println("7.- Modificar nombre animal");
		miZoo.cambiarNombreAnimal("Dori", "Hola soy Dori");
		miZoo.listarAnimales();
		
		System.out.println("8.- Listar animales de un zoo");
		miZoo.listarAnimalesPorZoo("zoo1");
		

	}

}
