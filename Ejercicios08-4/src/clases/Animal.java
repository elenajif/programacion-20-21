package clases;

public class Animal {
	
	//atributos
	private String nombreAnimal;
	private String especie;
	private double peso;
	private String zoo;
	
	//constructor
	public Animal (String nombreAnimal) {
		this.nombreAnimal=nombreAnimal;
	}
	
	//setter and getter
	public String getNombreAnimal() {
		return nombreAnimal;
	}
	public void setNombreAnimal(String nombreAnimal) {
		this.nombreAnimal = nombreAnimal;
	}
	public String getEspecie() {
		return especie;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getZoo() {
		return zoo;
	}
	public void setZoo(String zoo) {
		this.zoo = zoo;
	}

	//metodo toString
	@Override
	public String toString() {
		return "Animal [nombreAnimal=" + nombreAnimal + ", especie=" + especie + ", peso=" + peso + ", zoo=" + zoo
				+ "]";
	}

}
