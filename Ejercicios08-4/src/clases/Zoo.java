package clases;

public class Zoo {
	// atributos
	// declaracion
	private Animal[] animales;

	// crear un vector de String
	// String[] miVector = new String[3];
	// declaracion -> se coloca al comienzo de la clase
	// String[] miVector;
	// asignación -> se coloca en el constructor
	// miVector=new String[3];

	// constructor
	public Zoo(int maxAnimales) {
		// asignacion
		this.animales = new Animal[maxAnimales];
	}

	// alta animal
	public void altaAnimal(String nombreAnimal, double peso, String especie, String zoo) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] == null) {
				animales[i] = new Animal(nombreAnimal);
				animales[i].setPeso(peso);
				animales[i].setEspecie(especie);
				animales[i].setZoo(zoo);
				break;
			}
		}
	}

	// buscar Animal
	public Animal buscarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					return animales[i];
				}
			}
		}
		return null;
	}

	// eliminar Animal
	public void eliminarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i] = null;
				}
			}
		}
	}

	// listar animales
	public void listarAnimales() {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				System.out.println(animales[i]);
			}
		}
	}
	
	//cambiar nombre Animal
	public void cambiarNombreAnimal(String nombreAnimal, String nombreAnimal2) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i].setNombreAnimal(nombreAnimal2);
				}
			}
		}
	}
	
	//listar animales por zoo
	public void listarAnimalesPorZoo(String nombreZoo) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getZoo().equals(nombreZoo)) {
					System.out.println(animales[i]);
				}
			}
		}
	}

}
