package clasesmascota;

public class Gato extends Animal implements Mascota{
	
	private String codigo;
	
	public Gato(Sexo s, String codigo) {
		super(s);
		this.codigo=codigo;
	}

	@Override
	public String getCodigo() {
		return this.codigo;
	}

	@Override
	public void hazRuido() {
		this.maulla();
		this.ronronea();
	}

	@Override
	public void peleaCon(Animal contrincante) {
		if (this.getSexo()==Sexo.HEMBRA) {
			System.out.println("no me gusta pelear");
		} else {
			if (contrincante.getSexo()==Sexo.HEMBRA) {
				System.out.println("no peleo con hembras");
			} else {
				System.out.println("te vas a enterar ....");
			}
		}
		
	}
	
	public void maulla() {
		System.out.println("miauuuuuu");
	}
	
	public void ronronea() {
		System.out.println("Grrrrrrr");
	}

	@Override
	public void come(String comida) {
		if (comida.equals("pescado"))  {
			super.come();
			System.out.println("Hmmm gracias");
		} else {
			System.out.println("Lo siento, solo como pescado");
		}
		
	}
}
