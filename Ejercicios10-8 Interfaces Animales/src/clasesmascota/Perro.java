package clasesmascota;

public class Perro extends Animal implements Mascota{
	
	private String codigo;
	
	public Perro(Sexo s, String codigo) {
		super(s);
		this.codigo=codigo;
	}

	@Override
	public String getCodigo() {
		return this.codigo;
	}

	@Override
	public void hazRuido() {
		this.ladra();
	}

	@Override
	public void peleaCon(Animal contrincante) {
		if (contrincante.getClass().getSimpleName().equals("Perro")) {
			System.out.println("Te vas a enterar...");
		} else {
			System.out.println("no me gusta pelear");
		}
		
	}
	
	public void ladra() {
		System.out.println("guau guau ");
	}
	
	@Override
	public void come(String comida) {
		if (comida.equals("carne"))  {
			super.come();
			System.out.println("Hmmm gracias");
		} else {
			System.out.println("Lo siento, solo como carne");
		}
		
	}

}
