package clasesmascota;

public abstract class Animal {
	
	private Sexo sexo;
	
	public Animal() {
		sexo=Sexo.MACHO;
	}
	
	public Animal (Sexo x) {
		this.sexo=x;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	@Override
	public String toString() {
		return "Animal [sexo=" + sexo + "]";
	}
	
	public void duerme() {
		System.out.println("Zzzzzzzzz");
	}
	
	public void come() {
		System.out.println("�Qu� rico!");
	}
	
	public void come(String comida) {
		System.out.println("�Qu� rico! me gusta comer "+comida);
	}

}
