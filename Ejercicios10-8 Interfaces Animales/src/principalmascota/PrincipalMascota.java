package principalmascota;

import clasesmascota.Gato;
import clasesmascota.Mascota;
import clasesmascota.Perro;
import clasesmascota.Sexo;

public class PrincipalMascota {
		public static void main(String[] args) {

			Mascota garfield = new Gato(Sexo.MACHO, "34569");
			Mascota lisa = new Gato(Sexo.HEMBRA, "96059");
			Mascota kuki = new Perro(Sexo.HEMBRA, "234678");
			Mascota minu = new Perro(Sexo.MACHO, "778950");
			
			System.out.println("Muestro los codigos de los animales");
			System.out.println("Garfield "+garfield.getCodigo());
			System.out.println("Lisa "+lisa.getCodigo());
			System.out.println("Kuki "+kuki.getCodigo());
			System.out.println("Minu "+minu.getCodigo());
			System.out.println();
			System.out.println("Metodos comer");
			System.out.println("Gato Garfield");
			garfield.come("pescado");
			System.out.println("Gato Lisa");
			lisa.come("hamburguesa");
			System.out.println("Perro kuki");
			kuki.come("pescado");
			System.out.println();
			System.out.println("Metodos pelear");
			System.out.println("Lisa pelea con Garfield");
			lisa.peleaCon((Gato) garfield);
			System.out.println("Minu pelea con kuki");
			minu.peleaCon((Perro) kuki);
		}

}
