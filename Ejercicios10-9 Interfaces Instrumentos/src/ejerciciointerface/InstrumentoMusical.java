package ejerciciointerface;

public interface InstrumentoMusical {
	
	String tipoInstrumento();
	
	void afinar();
	void tocar();
	
}
