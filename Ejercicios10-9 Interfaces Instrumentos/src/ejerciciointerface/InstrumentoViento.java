package ejerciciointerface;

public class InstrumentoViento extends Object implements InstrumentoMusical{

	@Override
	public String tipoInstrumento() {
		return "Instrumento viento";
	}

	@Override
	public void afinar() {
		System.out.println("Afinar instrumento viento");
	}

	@Override
	public void tocar() {
		System.out.println("Tocar instrumento viento");
	}

}
