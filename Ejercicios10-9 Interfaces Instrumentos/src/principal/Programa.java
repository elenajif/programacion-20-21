package principal;

import ejerciciointerface.Flauta;
import ejerciciointerface.InstrumentoViento;

public class Programa {

	public static void main(String[] args) {
		InstrumentoViento miFlauta = new Flauta();
		System.out.println(miFlauta.tipoInstrumento());
		miFlauta.tocar();
		miFlauta.afinar();
	}

}
