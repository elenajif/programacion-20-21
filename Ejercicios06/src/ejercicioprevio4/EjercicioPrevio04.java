package ejercicioprevio4;

public class EjercicioPrevio04 {

	public static void main(String[] args) {
		int n = 5;
		int resultado = EjercicioPrevio04.factorialRecursividad(n);
		System.out.println("Recursividad factorial");
		System.out.println(resultado);
		
		int n1=123;
		System.out.println("Recursividad suma digitos");
		System.out.println(sumaDigitos(n1));
	}

	public static int factorialRecursividad(int numero) {
		// factorial 5 = 5*4*3*2*1

		// factorial 5 = 5 * factorial(4)
		// factorial 4 = 4 * factorial(3)
		// factorial 3 = 3 * factorial(2)
		// factorial 2 = 2 * factorial(1)
		// factorial 1 = 1 * factorial(0)
		// factorial 0 = 1
		int res;
		if (numero==1 || numero==0) {
			return 1;
		} else {
			res=numero*factorialRecursividad(numero-1);
			return res;
		}
	}
	
	public static int sumaDigitos(int numero) {
		//123 = 1+2+3
		if (numero<10) {
			return numero;
		} else {
			System.out.println("numero%10 "+numero%10);
			System.out.println("numero/10 "+numero/10);
			return (numero%10)+sumaDigitos(numero/10);
		}
	}

}
