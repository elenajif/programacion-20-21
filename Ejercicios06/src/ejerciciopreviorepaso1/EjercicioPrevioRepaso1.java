package ejerciciopreviorepaso1;

public class EjercicioPrevioRepaso1 {

	public static void main(String[] args) {
		/* Crear un ejercicio llamado EjercicioPrevioRepaso1 
		static main
		       		  llamara al los metodos uno, dos, tres
		metodo uno -> no devuelve nada, no recibe nada
	      			  muestra la longitud de una cadena
		metodo dos -> devuelve un String, no recibe nada
	      			  concatena un nombre y un apellido
		metodo tres -> devuelve un boolean, recibe dos String
               		  busca la cadena1 en la cadena2
		 */
		System.out.println("metodo uno");
		EjercicioPrevioRepaso1.uno();
		System.out.println("metodo dos");
		String concat=EjercicioPrevioRepaso1.dos();
		System.out.println(concat);
		System.out.println("metodo tres");
		boolean contiene=EjercicioPrevioRepaso1.tres("Hola caracola","caracola");
		System.out.println(contiene);
		boolean contiene1=EjercicioPrevioRepaso1.tres("Hola caracola","adios");
		System.out.println(contiene1);
		
	}
	
	public static void uno() {
		String cadena="hola caracola";
		System.out.println("La longitud de la cadena es "+cadena.length());
	}
	
	public static String dos() {
		String nombre="Pepe";
		String apellidos="Perez Martinez";
		String concatenado=nombre+" "+apellidos;
		return concatenado;
	}
	
	public static boolean tres(String a, String b) {
		return a.contains(b);
	}

}
