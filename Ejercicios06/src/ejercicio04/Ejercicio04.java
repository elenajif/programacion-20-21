package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame un n�mero: ");
		int numero = input.nextInt();
		for (int i=0;i<numero;i++) {
			System.out.println(i+ " es primo? "+Metodos04.esPrimo(i));
		}
		
		input.close();

	}

}
