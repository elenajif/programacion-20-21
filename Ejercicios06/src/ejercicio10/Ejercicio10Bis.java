package ejercicio10;

import java.util.Scanner;

public class Ejercicio10Bis {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame una cadena");
		String cadena = input.nextLine();

		if (esEntero(cadena)) {
			System.out.println("Es un entero");
		} else {
			System.out.println("No es un entero");
		}

		input.close();
	}

	public static boolean esEntero(String cadena) {
		boolean esEntero = true;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) < '0' || cadena.charAt(i) > '9') {
				if ((i == 0 && cadena.charAt(i) == '-') && cadena.length() > 1) {
					continue;
				}
				esEntero = false;
			}
		}
		return esEntero;
	}

}
