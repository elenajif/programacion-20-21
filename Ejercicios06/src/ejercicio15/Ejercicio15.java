package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una serie de palabras "
				+ "separadas por un espacio");
		String palabras = input.nextLine();
		
		System.out.println("La palabra mas larga: " + palabraMasLarga(palabras));
		
		input.close();
	}

	private static String palabraMasLarga(String palabras) {
		//Si la cadena no contiene espacios 
		//la palabra m�s larga es la �nica que hay
		if(!palabras.contains(" ")){
			return palabras;
		}
		//Si hay espacios, cojo la primera palabra
		String palabraLarga = palabras.substring(0, palabras.indexOf(' '));
		String palabraEncontrada;
		
		int espacioInicio = palabras.indexOf(' '); 
		
		for(int i = espacioInicio + 1; i < palabras.length(); i++){
			//Si me encuentro un espacio o el final de la cadena
			if(palabras.charAt(i) == ' ' || i == (palabras.length() - 1)){
				//Extraigo la palabra
				palabraEncontrada = palabras.substring(espacioInicio + 1, i);
				//Si su longitud es mayor a la que ya tenia, la guardo
				if(palabraEncontrada.length() > palabraLarga.length()){
					palabraLarga = palabraEncontrada; 
				}
				espacioInicio = i;
			}
			
			
		}
		return palabraLarga;
	}

}
