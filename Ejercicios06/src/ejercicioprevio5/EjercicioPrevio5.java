package ejercicioprevio5;

import java.util.Scanner;

public class EjercicioPrevio5 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		int opcion;
		do {
			System.out.println("1.- mostrar tabla ascii");
			System.out.println("2.- mostrar primo");
			System.out.println("3.- salir");
			System.out.println("Elegir opcion");
			opcion=input.nextInt();
			
			switch(opcion) {
			case 1:
				Metodos5.mostrarTablaAscii();
				break;
			case 2: 
				System.out.println(Metodos5.esPrimo(15));
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("No has elegido la opcion correcta");
					
			}
			
		} while (opcion != 3);
		
		input.close();
	}

}
