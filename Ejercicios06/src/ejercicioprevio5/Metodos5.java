package ejercicioprevio5;

public class Metodos5 {
	
	public static void mostrarTablaAscii() {
		for (int i=0;i<256;i++) {
			System.out.println((char)i+ " - "+i);
		}
	}
	
	public static boolean esPrimo(int numero) {
		// si me encuentro con un divisor ya no es primo
		for (int i = 2; i < numero; i++) {
			if (numero % i == 0) {
				return false;
			}
		}
		return true;
	}

}
