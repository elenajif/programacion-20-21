package ejerciciopreviorepaso;

public class EjercicioPrevioRepaso {

	public static void main(String[] args) {
		//Math es una clase que tiene todos sus métodos estáticos
		//se llama usando la clase directamente
		// La clase Math está incorporada dentro del paquete java.lang
		//no requiere import
		//Math.metodo
		System.out.println("Uso de la clase Math");
		System.out.println("Raiz cuadrada "+Math.sqrt(2.54));

		//public static void miMetodo()
		//public -> modificador -> accesible desde cualquier lugar
		//static -> permite acceder directamente si crear un objeto de la clase
		//void -> valor de devolucion del metodo -> void no devuelve nada fuera del metodo
		//miMetodo -> nombre del metodo
		//() -> parametros -> en este caso ninguno
		System.out.println("Metodo que no recibe parámetros y no devuelve nada (void)");
		EjercicioPrevioRepaso.metodo1();
		
		//public static int miMetodo()
		//public -> modificador -> accesible desde cualquier lugar
		//static -> permite acceder directamente si crear un objeto de la clase
		//int -> valor de devolucion del metodo -> devuelve un entero -> llevara un return
		//miMetodo -> nombre del metodo
		//() -> parametros -> en este caso ninguno
		System.out.println("Metodo que no recibe parámetros y devuelve un valor");
		System.out.println("con un int");
		int miResta=EjercicioPrevioRepaso.metodo2();
		System.out.println("La resta es "+miResta);
		System.out.println("con un double");
		double miResta1=EjercicioPrevioRepaso.metodo3();
		System.out.println("La resta es "+miResta1);
			
		//public static int miMetodo(int x, double y)
		//public -> modificador -> accesible desde cualquier lugar
		//static -> permite acceder directamente si crear un objeto de la clase
		//miMetodo -> nombre del metodo
		//(int x, double y) -> parametros -> en este caso recibe un entero y un double
		System.out.println("Metodo que si recibe parámetros y devuelve un valor");
		int x=125;
		int y=4;
		System.out.println("con un int");
		int multiplica=EjercicioPrevioRepaso.metodo4(x, y);
		System.out.println("La multiplicación es "+multiplica);
		double n1=12.3;
		double n2=8.5;
		System.out.println("con un double");
		double multiplica1=EjercicioPrevioRepaso.metodo5(n1, n2);
		System.out.println("La multiplicación es "+multiplica1);
		
		//no hemos visto programacion orientada a objetos, por tanto, de momento static
		//hay un caso que si hemos usado con creación de objeto
		//Scanner in = new Scanner(System.in)
		//in es un objeto (el resto lo explicaremos)
		
	}
	
	//Metodo que no recibe parámetros y no devuelve nada (void)
	
	public static void metodo1() {
		int x=2;
		int y=3;
		int suma=x+y;
		System.out.println("La suma de "+x+" mas "+y+" es "+suma);
	}
	
	//Metodo que no recibe parámetros y devuelve un valor
	
	public static int metodo2() {
		int x=27;
		int y=15;
		int resta=x-y;
		return resta;		
	}
	public static double metodo3() {
		double x=2.7;
		double y=1.5;
		double resta=x-y;
		return resta;		
	}
	
	//Metodo que si recibe parámetros y devuelve un valor
	
	public static int metodo4(int a, int b) {
		int mult=a*b;
		return mult;
	}
	
	public static double metodo5(double a, double b) {
		double mult=a*b;
		return mult;
	}
	

}
