package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {

		System.out.println("Sin leer datos por pantalla");
		System.out.println("Maximo "+Metodos02.maximo(5, 2));
		System.out.println("Minimo "+Metodos02.minimo(7, 8));
		
		System.out.println("Con lectura de datos por pantalla");
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame un numero");
		int n1 = input.nextInt();
		System.out.println("Dame otro numero");
		int n2=input.nextInt();
		System.out.println("Maximo "+Metodos02.maximo(n1, n2));
		System.out.println("Minimo "+Metodos02.minimo(n1, n2));
		
		
		input.close();

	}

}
