package ejercicio02;

public class Metodos02 {
	static int maximo(int numero1, int numero2) {
		int mayor;
		if (numero1>numero2) {
			mayor=numero1;
		} else {
			mayor=numero2;
		}
		return mayor;
	}
	
	static int minimo(int numero1, int numero2) {
		int menor;
		if (numero1<numero2) {
			menor=numero1;
		} else {
			menor=numero2;
		}
		return menor;
	}

}
