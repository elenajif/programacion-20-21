package ejercicioprevio;

public class Ejercicio01Previo {

	public static void main(String[] args) {
		// clase Math
		// raiz cuadrada
		System.out.println("La raiz cuadrada de 5");
		System.out.println(Math.sqrt(5));
		// numero aleatorio
		System.out.println("N�mero aleatorio");
		System.out.println(Math.random());
		// redondeo con round
		System.out.println("Redondeamos 5.25");
		System.out.println(Math.round(5.25));
		// maximo
		System.out.println("El m�ximo de 7 y 3");
		System.out.println(Math.max(7, 3));
		// minimo
		System.out.println("El m�nimo de 7 y 3");
		System.out.println(Math.min(7, 3));
		// potencia
		System.out.println("Potencia de 3 elevado a 4");
		System.out.println(Math.pow(3, 4));
		// redondeo ceil
		System.out.println("Redondear 3.25");
		System.out.println(Math.ceil(3.25));
		// valor absoluto
		System.out.println("Valor absoluto de -3");
		System.out.println(Math.abs(-3));
		// logaritmo
		System.out.println("Logaritmo de 2");
		System.out.println(Math.log(2));
		// numero PI
		System.out.println("Numero PI");
		System.out.println(Math.PI);
		// numero E
		System.out.println("Numero E");
		System.out.println(Math.E);

		// clase comienza por Mayuscula
		// Math es una clase
		// Scanner es una clase
		// Las clases que yo puedo usar directamente, son especiales
		// Math.sqrt(5)
		// las clases que yo uso con la palabra new son est�ndar
		// Scanner input = new Scanner(System.in)
		// input.nextInt()
		// Scanner.nextInt() -> mal
		// puedo usar la clase cuando el metodo sea static
		// todos los metodo de la clase Math son static
		// todos los puedo usar directamente usando la clase
		// los metodos nextInt nextDouble ... no son static
		// no puedo usar la clase directamente

	}

}
