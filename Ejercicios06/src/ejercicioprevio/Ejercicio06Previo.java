package ejercicioprevio;

import java.util.Scanner;

public class Ejercicio06Previo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Estoy en el main");
		System.out.println("Dame un numero double");
		double numero1=input.nextDouble();
		System.out.println("Dame otro numero double");
		double numero2=input.nextDouble();
		
		System.out.println("llamo a suma");
		double miSuma=Ejercicio06Previo.suma(numero1,numero2);
		System.out.println("Muestro miSuma desde el main " +miSuma);
		System.out.println("llamo a resta");
		double miResta=Ejercicio06Previo.resta(numero1,numero2);
		System.out.println("Muestro miResta desde el main " +miResta);
		System.out.println("llamo a multiplica");
		double miMultiplica=Ejercicio06Previo.multiplica(numero1,numero2);
		System.out.println("Muestro miMultiplica desde el main " +miMultiplica);
		System.out.println("llamo a divide");
		double miDivision=Ejercicio06Previo.division(numero1,numero2);
		System.out.println("Muestro miDivide desde el main " +miDivision);
		

		
		input.close();
	}
	
	//si el valor de devoluci�n es void, no sale nada del m�todo
	// si el valor de devoluci�n es double, int, ..
	// usar� un return para sacar el valor fuera del m�todo

	public static double suma(double x, double y) {
		System.out.println("Estoy en suma");
		double suma = x + y;
		return suma;
	}
	
	public static double resta(double x, double y) {
		System.out.println("Estoy en resta");
		double resta = x - y;
		return resta;
	}

	
	public static double multiplica(double x, double y) {
		System.out.println("Estoy en multiplicacion");
		double multiplica = x * y;
		return multiplica;
	}

	
	public static double division(double x, double y) {
		System.out.println("Estoy en division");
		double divide = x / y;
		return divide;
	}

}
