package ejercicioprevio;

import java.util.Scanner;

public class Ejercicio04Previo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Estoy en el main");
		System.out.println("Dame un numero");
		int numero1=input.nextInt();
		System.out.println("Dame otro numero");
		int numero2=input.nextInt();
		
		Ejercicio04Previo.suma(numero1,numero2);
		Ejercicio04Previo.resta(numero1,numero2);
		Ejercicio04Previo.multiplica(numero1,numero2);
		
		System.out.println("Dame un numero double");
		double numero3=input.nextDouble();
		System.out.println("Dame otro numero double");
		double numero4=input.nextDouble();
		Ejercicio04Previo.division(numero3,numero4);
		
		input.close();
	}

	public static void suma(int x, int y) {
		System.out.println("Estoy en suma");
		int suma = x + y;
		System.out.println("La suma es " + suma);
	}
	
	public static void resta(int x, int y) {
		System.out.println("Estoy en resta");
		int resta = x - y;
		System.out.println("La resta es " + resta);
	}

	
	public static void multiplica(int x, int y) {
		System.out.println("Estoy en multiplicacion");
		int multiplica = x * y;
		System.out.println("La multiplicacion es " + multiplica);
	}

	
	public static void division(double x, double y) {
		System.out.println("Estoy en division");
		double divide = x / y;
		System.out.println("La division es " + divide);
	}


}
