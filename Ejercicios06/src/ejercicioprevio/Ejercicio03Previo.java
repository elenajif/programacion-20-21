package ejercicioprevio;

public class Ejercicio03Previo {

	public static void main(String[] args) {
		System.out.println("Estoy en el main");
		Ejercicio03Previo.suma(4,5);
		Ejercicio03Previo.resta(5,8);
		Ejercicio03Previo.multiplica(8,7);
		Ejercicio03Previo.division(8.9,8.7);

	}

	public static void suma(int x, int y) {
		System.out.println("Estoy en suma");
		int suma = x + y;
		System.out.println("La suma es " + suma);
	}
	
	public static void resta(int x, int y) {
		System.out.println("Estoy en resta");
		int resta = x - y;
		System.out.println("La resta es " + resta);
	}

	
	public static void multiplica(int x, int y) {
		System.out.println("Estoy en multiplicacion");
		int multiplica = x * y;
		System.out.println("La multiplicacion es " + multiplica);
	}

	
	public static void division(double x, double y) {
		System.out.println("Estoy en division");
		double divide = x / y;
		System.out.println("La division es " + divide);
	}


}
