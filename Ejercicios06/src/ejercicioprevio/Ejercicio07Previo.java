package ejercicioprevio;

public class Ejercicio07Previo {

	public static void main(String[] args) {
		System.out.println("Metodo1");
		Ejercicio07Previo.metodo1();
		System.out.println("Metodo2");
		Ejercicio07Previo.metodo2(8, 7);
		System.out.println("Metodo3");
		int miLongitud=Ejercicio07Previo.metodo3();
		System.out.println("La longitud es "+miLongitud);
		System.out.println("Metodo4");
		double miArea=Ejercicio07Previo.metodo4(15, 17);
		System.out.println("El �rea es "+miArea);
	}
	
	//metodo que no devuelve nada y no recibe nada
	public static void metodo1() {
		System.out.println("Potencia de dos n�meros usando Math");
		System.out.println("El resultado de la potencia usando Math es "+Math.pow(2, 5));
	}	
	
	//metodo que no devuelve nada pero recibe dos enteros
	public static void metodo2(int x, int y) {
		System.out.println("Multiplico los dos n�meros y le resto 205 ");
		int operacion=x*y+205;
		System.out.println("El resultado de operacion es "+operacion);		
	}
	
	//metodo que devuelve un entero pero no recibe nada
	//devuelve -> return
	public static int metodo3() {
		String nombre="Elena";
		int longitud= nombre.length();
		return longitud;
	}
	
	//metodo que devuelve un double y recibe dos doubles
	//devuelve -> return
	public static double metodo4(double base, double altura) {
		double area=base*altura;
		return area;
	}

}
