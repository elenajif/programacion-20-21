package ejercicioprevio;

import java.util.Scanner;

public class Ejercicio05Previo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Estoy en el main");
		System.out.println("Dame un numero double");
		double numero1=input.nextDouble();
		System.out.println("Dame otro numero double");
		double numero2=input.nextDouble();
		
		Ejercicio05Previo.suma(numero1,numero2);
		Ejercicio05Previo.resta(numero1,numero2);
		Ejercicio05Previo.multiplica(numero1,numero2);
		Ejercicio05Previo.division(numero1,numero2);
		

		
		input.close();
	}

	public static void suma(double x, double y) {
		System.out.println("Estoy en suma");
		double suma = x + y;
		System.out.println("La suma es " + suma);
	}
	
	public static void resta(double x, double y) {
		System.out.println("Estoy en resta");
		double resta = x - y;
		System.out.println("La resta es " + resta);
	}

	
	public static void multiplica(double x, double y) {
		System.out.println("Estoy en multiplicacion");
		double multiplica = x * y;
		System.out.println("La multiplicacion es " + multiplica);
	}

	
	public static void division(double x, double y) {
		System.out.println("Estoy en division");
		double divide = x / y;
		System.out.println("La division es " + divide);
	}

}
