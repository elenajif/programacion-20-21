package ejercicioprevio;

public class Ejercicio02Previo {

	public static void main(String[] args) {
		System.out.println("Estoy en el main");
		Ejercicio02Previo.suma();
		Ejercicio02Previo.resta();
		Ejercicio02Previo.multiplica();
		Ejercicio02Previo.division();

	}

	public static void suma() {
		System.out.println("Estoy en suma");
		int x = 3;
		int y = 4;
		int suma = x + y;
		System.out.println("La suma es " + suma);
	}
	
	public static void resta() {
		System.out.println("Estoy en resta");
		int x = 3;
		int y = 4;
		int resta = x - y;
		System.out.println("La resta es " + resta);
	}

	
	public static void multiplica() {
		System.out.println("Estoy en multiplicacion");
		int x = 3;
		int y = 4;
		int multiplica = x * y;
		System.out.println("La multiplicacion es " + multiplica);
	}

	
	public static void division() {
		System.out.println("Estoy en division");
		double x = 3;
		double y = 4;
		double divide = x / y;
		System.out.println("La division es " + divide);
	}


}
