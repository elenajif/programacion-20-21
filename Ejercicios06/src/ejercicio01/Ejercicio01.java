package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame una edad");
		int edadLeida=input.nextInt();
		
		boolean resultado = Metodos01.esAdulto(edadLeida);
		
		if (resultado) {
			System.out.println("Es mayor de edad");
		} else {
			System.out.println("No es mayor de edad");
		}
		
		input.close();

	}

}
