package ejercicioprevio3;

import java.util.Scanner;

public class EjercicioPrevio3 {

	static Scanner leer = new Scanner(System.in);
	
	public static void main(String[] args) {
		//llamo a int leerEntero()
		int numero1=EjercicioPrevio3.leerEntero();
		//llamo a int leerEntero()
		int numero2=EjercicioPrevio3.leerEntero();
		System.out.println("La suma de "+numero1+" "+numero2+" es: ");
		//llamo a sumar(int x1, int x2)
		System.out.println(EjercicioPrevio3.sumar(numero1,numero2 ));
		//llamo a double leerDouble()
		double numero3=EjercicioPrevio3.leerDouble();
		//llamo a double leerDouble()
		double numero4=EjercicioPrevio3.leerDouble();
		System.out.println("La suma de "+numero3+" "+numero4+" es: ");
		//llamo a sumar(double x1, double x2)
		System.out.println(EjercicioPrevio3.sumar(numero3, numero4));
		//llamo a int leerEntero
		int numero5=EjercicioPrevio3.leerEntero();
		System.out.println("La suma de "+numero1+" "+numero2+" "+numero5+" es: ");
		//llamo a sumar(int x1, int x2, int x3)
		System.out.println(EjercicioPrevio3.sumar(numero1, numero2, numero5));
		
		leer.close();

	}

	public static int sumar(int x1, int x2) {
		System.out.println("Estoy en sumar int x1 int x2");
		int sum = x1 + x2;
		return sum;
	}
	
	public static double sumar(double x1, double x2) {
		System.out.println("Estoy en sumar double x1 double x2");
		double sum = x1 + x2;
		return sum;
	}

	public static int sumar(int x1, int x2, int x3) {
		System.out.println("Estoy en sumar int x1 int x2 int x3");
		int sum = x1 + x2 + x3;
		return sum;
	}
	
	public static int leerEntero() {
		System.out.println("Estoy en int leerEntero()");
		System.out.println("Dame un n�mero entero");
		int n=leer.nextInt();
		return n;
	}
	
	public static double leerDouble() {
		System.out.println("Estoy en double leerEntero()");
		System.out.println("Dame un n�mero double");
		double n=leer.nextDouble();
		return n;
	}

}
