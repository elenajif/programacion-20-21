package ejercicioprevio3;

public class EjercicioPrevio31 {

	public static void main(String[] args) {
		System.out.println("La suma de 2+4 es: ");
		System.out.println(EjercicioPrevio31.sumar(2, 4));
		System.out.println("La suma de 2.8+4.5 es: ");
		System.out.println(EjercicioPrevio31.sumar(2.8, 4.5));
		System.out.println("La suma de 2+4+5 es: ");
		System.out.println(EjercicioPrevio31.sumar(2, 4, 5));

	}

	public static int sumar(int x1, int x2) {
		int sum = x1 + x2;
		return sum;
	}

	public static double sumar(double x1, double x2) {
		double sum = x1 + x2;
		return sum;
	}

	public static int sumar(int x1, int x2, int x3) {
		int sum = x1 + x2 + x3;
		return sum;
	}

}
