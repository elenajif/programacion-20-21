package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		System.out.println("Sin lectura de datos");
		System.out.println(Metodos03.esDigito('4'));
		
		System.out.println("Con lectura de datos");
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame un caracter");
		char letra = input.nextLine().charAt(0);
		System.out.println(Metodos03.esDigito(letra));
		input.close();

	}

}
