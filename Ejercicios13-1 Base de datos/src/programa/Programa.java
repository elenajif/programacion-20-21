package programa;

import java.sql.SQLException;

import clases.Festival;

public class Programa {

	public static void main(String[] args) {
		Festival festival = new Festival();

		System.out.println("Alta artistas");
		festival.altaArtista("1234", "Jesus", "Pop", "1234");
		festival.altaArtista("5678", "Maria", "Rock", "5678");
		System.out.println("Alta asistente");
		festival.altaAsistente("2222", "Asistente1", "2001-11-04", "Spain");
		System.out.println("Alta concierto");
		festival.altaConcierto("1111", "Concierto1", "07:11:00", "1234");
		System.out.println("Registrar asistente concierto");
		festival.registrarAsistenteConcierto("1111", "2222");

		System.out.println("Listar artistas");
		festival.listasArtistas();
		System.out.println("Listar asistentes");
		festival.listasAsistentes();

		System.out.println("Guardar datos");
		festival.guardarDatos();
		System.out.println("Cargamos datos");
		festival.cargarDatos();

		System.out.println("Conectamos BBDD");
		festival.conectarBBDD();
		System.out.println("Guardamos artistas pop");
		festival.guardarArtistasBBDD("Pop");

		System.out.println("Cargamos asistentes BBDD spain");
		try {
			festival.cargarAsistentesBBDD("spain");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Listar artistas");
		festival.listasArtistas();
		System.out.println("Listar asistentes");
		festival.listasAsistentes();

	}

}
