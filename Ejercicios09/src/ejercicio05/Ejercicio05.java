package ejercicio05;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio05 {

	public static void main(String[] args) {
		ArrayList<String> cadenas = new ArrayList<>();
		cadenas.add("hola");
		cadenas.add("adios");
		cadenas.add("hola");
		cadenas.add("hola");
		cadenas.add("adios");

		// foreach
		System.out.println("Antes de borrar");
		for (String cadena : cadenas) {
			System.out.println(cadena);
		}

		// llamamos al metodo borrarCadenas que recibe una cadena y un ArrayList
		borrarCadenas("hola", cadenas);

		// foreach
		System.out.println("Despu�s de borrar");
		for (String cadena : cadenas) {
			System.out.println(cadena);
		}
	}

	private static void borrarCadenas(String cadenasABorrar, ArrayList<String> cadenas) {
		Iterator<String> iterador = cadenas.iterator();

		while (iterador.hasNext()) {
			String cadena = iterador.next();
			if (cadena.equals(cadenasABorrar)) {
				iterador.remove();
			}
		}

	}

}
