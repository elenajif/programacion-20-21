package ejercicio02;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<String> lista = new ArrayList<>();

		String cadena;

		do {
			System.out.println("Dame una cadena");
			cadena = input.nextLine();
			lista.add(cadena);
		} while (!cadena.equalsIgnoreCase("fin"));
		
		//llamo a metodo listar estatico y le paso un ArrayList
		listar(lista);

		System.out.println("Dame la cadena que quieres borrar");
		cadena=input.nextLine();
		//llamo a metodo borrar que recibe una cadena y un ArrayList
		borrar(cadena,lista);

		//llamo a metodo listar estatico y le paso un ArrayList
		listar(lista);

		input.close();
	}
	
	private static void borrar(String cadena, ArrayList<String> lista) {
		lista.remove(cadena);
	}
	
	public static void listar (ArrayList<String> lista) {
		for(String string:lista) {
			System.out.println(string);
		}
	}

}
