package ejercicioprevio02;

import java.util.ArrayList;
import java.util.Scanner;

public class EjercicioPrevio02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Double> notas = new ArrayList<Double>();

		double n;
		do {
			System.out.println("Introduce notas doubles, 0 para acabar");
			System.out.println("Nota ");
			n = sc.nextDouble();
			if (n != 0) {
				notas.add(n);
			}
		} while (n != 0);
		System.out.println("Has introducido " + notas.size() + " numeros");
		System.out.println("Mostramos el ArrayList completo");
		System.out.println(notas);

		System.out.println("Recorrido usando un foreach para sumar los elementos");
		double suma = 0;
		for (Double i : notas) {
			suma = suma + i;
		}

		System.out.println("Suma " + suma);
		System.out.println("Media " + suma / notas.size());

		double maximo = notas.get(0);
		double minimo = notas.get(0);
		for (Double i : notas) {
			if (i != 0) {
				if (i > maximo) {
					maximo = i;
				}
				if (i < minimo) {
					minimo = i;
				}
			}
		}

		System.out.println("Maximo " + maximo);
		System.out.println("Minimo " + minimo);

		sc.close();

	}

}
