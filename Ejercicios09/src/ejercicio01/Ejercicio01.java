package ejercicio01;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//atajo -> no necesito indicar a la derecha nuevamente <String>
		//ArrayList<String> lista = new ArrayList<String>();		
		
		ArrayList<String> lista = new ArrayList<>();	
		System.out.println("Indica la cantidad de cadenas");
		int cantidad=input.nextInt();
		input.nextLine();
		
		for (int i=0;i<cantidad;i++) {
			System.out.println("introduce una cadena");
			String cadena = input.nextLine();
			lista.add(cadena);
		}
		
		System.out.println("mostramos");
		for (String cadena: lista) {
			System.out.println(cadena);
		}
		
		System.out.println("Dame la posicion de la cadena a borrar");
		int posicion=input.nextInt();
		System.out.println("Borramos posicion");
		lista.remove(posicion);
		
		System.out.println("mostramos");		
		for (String cadena: lista) {
			System.out.println(cadena);
		}
		
		input.close();

	}

}
