package ejercicioprevio04;

import java.util.ArrayList;
import java.util.Scanner;

public class EjercicioPrevio04 {
	public static void main(String args[]) {
		Scanner leer = new Scanner(System.in);
		System.out.println("Creamos objetos y atributos");
		String nombre;
		double altura;
		ArrayList<Persona> personitas = new ArrayList<Persona>();
		System.out.println("Lectura de datos");
		System.out.println("Dame el numero de personas a introducir");
		int n = leer.nextInt();
		for (int i = 1; i <= n; i++) {
			System.out.println("Introduce datos");
			leer.nextLine();
			System.out.println("Dame el nombre");
			nombre = leer.nextLine();
			System.out.println("Dame la altura");
			altura = leer.nextDouble();
			personitas.add(new Persona(i, nombre, altura));

		}
		System.out.println("Mostramos el ArrayList");
		System.out.println(personitas);
		System.out.println("Calculo la media de la altura");
		double suma=0;
		for (Persona p: personitas) {
			suma=suma+p.getAltura();
		}
		System.out.println("Suma "+suma);
		System.out.println("Media "+suma/personitas.size());
		

		leer.close();
	}

}
