package ejercicioprevio01;

import java.util.ArrayList;

public class EjercicioPrevio01 {

	public static void main(String[] args) {
		System.out.println("Vamos a crear un ArrayList");
		ArrayList<String> nombres = new ArrayList<String>();
		
		System.out.println("A�adimos");
		nombres.add("Ana");
		nombres.add("Luisa");
		nombres.add("Felipe");
		System.out.println("Mostramos");
		System.out.println(nombres);
		
		System.out.println("A�adimos uno mas");
		nombres.add("Pablo");
		System.out.println("Mostramos");
		System.out.println(nombres);
		
		System.out.println("Borramos el 0");
		nombres.remove(0);
		System.out.println("Mostramos");
		System.out.println(nombres);
		
		System.out.println("Ponemos en el 0 a Alfonso");
		nombres.set(0, "Alfonso");
		System.out.println("Mostramos");
		System.out.println(nombres);
		
		System.out.println("Quitamos el ultimo");
		String s =nombres.get(1);
		String ultimo = nombres.get(nombres.size() -1);
		System.out.println("Mostramos");
		System.out.println(s +" "+ultimo);
		

	}

}
