package ejercicioprevio03;

import java.util.ArrayList;
import java.util.Scanner;

public class EjercicioPrevio03 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> numeros = new ArrayList<Integer>();
		
		int n;
		do  {
			System.out.println("Introduce numeros enteros, 0 para acabar");
			System.out.println("Numero ");
			n=sc.nextInt();
			if (n!=0) {
				numeros.add(n);
			}
		} while (n!=0);
		System.out.println("Has introducido "+numeros.size()+ " numeros");
		System.out.println("Mostramos el ArrayList completo");
		System.out.println(numeros);
		
		System.out.println("Recorrido usando un foreach para sumar los elementos");
		double suma=0;
		for (Integer i: numeros) {
			suma=suma+i;
		}
		System.out.println("Suma "+suma);
		System.out.println("Media "+suma/numeros.size());
		
		
		sc.close();

	}

}
