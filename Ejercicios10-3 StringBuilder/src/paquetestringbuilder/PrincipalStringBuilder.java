package paquetestringbuilder;

public class PrincipalStringBuilder {

	public static void main(String[] args) {
		// String -> no puede variar de tama�o
		// StringBuilder -> se puede expandir
		// metodos append, insert, replace, reverse
		
		StringBuilder texto = new StringBuilder("Una prueba");
		
		System.out.println("Texto "+texto);
		texto.append(" mas");
		System.out.println("A�adimos "+texto);
		texto.insert(2,"xxx");
		System.out.println("Insertamos "+texto);
		texto.reverse();
		System.out.println("Invertimos "+texto);
		
		System.out.println("En mayusculas "+texto.toString().toUpperCase());

	}

}
