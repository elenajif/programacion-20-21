package paquetestringbuilder;

public class PrincipalStringBuilder2 {

	public static void main(String[] args) {
		String s = "1234567890";
		System.out.println("Sin separar "+s);
		s=separarMiles(s);
		System.out.println("Separando "+s);
	}
	
	public static  String separarMiles(String s) {
		//creamos un StringBuilder a partir del String s
		StringBuilder aux = new StringBuilder(s);
		
		//le damos la vuelta
		aux.reverse();
		
		//variable que indica donde insertar el siguiente punto
		int posicion=3;
		
		//mientras no lleguemos al final del numero
		while (posicion<aux.length()) {
			//insertamos un punto en la posicion
			aux.insert(posicion, '.');
			//siguiente posicion donde insertar
			posicion+=4;
		}
		
		//le damos la vuelta
		aux.reverse();
		
		//devolvemos el stringBuilder
		return aux.toString();
	}

}
