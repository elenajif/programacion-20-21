package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero de 5 cifras");
		int numero=escaner.nextInt();
		
		System.out.println(numero/10000);
		System.out.println(numero/1000);
		System.out.println(numero/100);
		System.out.println(numero/10);
		System.out.println(numero);
		
		escaner.nextLine();
		
		System.out.println("Introduce un n�mero de 5 cifras");
		String cadena=escaner.nextLine();
		
		System.out.println(cadena.charAt(0));
		System.out.println(cadena.charAt(0)+""+cadena.charAt(1));
		System.out.println(cadena.charAt(0)+""+cadena.charAt(1)+""+cadena.charAt(2));
		System.out.println(cadena.charAt(0)+""+cadena.charAt(1)+""+cadena.charAt(2)+""+cadena.charAt(3));
		System.out.println(cadena);
		
		escaner.close();

	}

}
