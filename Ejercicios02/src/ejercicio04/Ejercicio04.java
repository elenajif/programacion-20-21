package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce el radio de la circunferencia");
		double radio=lector.nextDouble();
		
		double area=radio*radio*3.1415;
		// 3.1415 -> Math.PI
		double longitud=2*3.1415*radio;
		
		System.out.println("Longitud circunferencia "+longitud);
		System.out.println("Area circunferencia "+area);
		
		lector.close();
		
		
		

	}

}
