package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un n�mero");
		String cadena = input.nextLine();

		System.out.println(cadena.charAt(0));
		System.out.println(cadena.charAt(1));
		System.out.println(cadena.charAt(2));

		System.out.println("Introduce el n�mero por 2� vez");
		int numero = input.nextInt();

		System.out.println(numero / 100);
		System.out.println((numero / 10) % 10);
		System.out.println(numero % 10);

		input.close();

	}

}
