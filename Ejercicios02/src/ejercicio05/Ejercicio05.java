package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame una cadena");
		String cadena1=escaner.nextLine();
		
		System.out.println("Dame otra cadena");
		String cadena2=escaner.nextLine();
		
		System.out.println(cadena1.equals(cadena2)?
				"Las cadenas son iguales": "Las cadenas son distintas");
		
		System.out.println(cadena1.equalsIgnoreCase(cadena2)?
				"Las cadenas son iguales": "Las cadenas son distintas");
		
		escaner.close();

	}

}
