package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena=input.nextLine();
		
		int longitudCadena=cadena.length();
		
		System.out.println(cadena.substring(longitudCadena/2));
		
		input.close();

	}

}
