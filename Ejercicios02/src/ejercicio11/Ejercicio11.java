package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena=input.nextLine();
		
		boolean contieneVocales=cadena.contains("a") 
				|| cadena.contains("e") 
				|| cadena.contains("i") 
				|| cadena.contains("o") 
				|| cadena.contains("u");
		
		System.out.println(contieneVocales? "contiene vocales":"no contiene");
		
		boolean empiezaPorVocal =cadena.startsWith("a") 
				|| cadena.startsWith("e") 
				|| cadena.startsWith("i") 
				|| cadena.startsWith("o") 
				|| cadena.startsWith("u");
		
		System.out.println(empiezaPorVocal? "Empieza por vocal":"no empieza por vocal");
		
		boolean terminaPorVocal =cadena.endsWith("a") 
				|| cadena.endsWith("e") 
				|| cadena.endsWith("i") 
				|| cadena.endsWith("o") 
				|| cadena.endsWith("u");
		
		System.out.println(terminaPorVocal? "Termina por vocal":"no termina por vocal");
		
		input.close();

	}

}
