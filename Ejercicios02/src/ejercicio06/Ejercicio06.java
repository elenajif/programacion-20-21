package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce la longitud de un cateto");
		double cateto1=input.nextDouble();
		
		System.out.println("Introduce la longitud de otro cateto");
		double cateto2=input.nextDouble();
		
		double hipotenusa = Math.sqrt((cateto1*cateto1)+(cateto2*cateto2));
		
		System.out.println("La hipotenusa es "+hipotenusa);
		
		input.close();

	}

}
