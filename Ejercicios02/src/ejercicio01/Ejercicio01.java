package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		//Pido los datos al usuario
		System.out.println("Introduce un n�mero entero");
		//Leo datos desde el programa
		int numero=lector.nextInt();
		
		int doble =numero*2;
		
		System.out.println("El doble es "+doble);
		System.out.println("El triple es "+(3*numero));
		
		lector.close();

	}

}
