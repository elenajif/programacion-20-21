package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero de 5 cifras");
		int numero=escaner.nextInt();
		
		System.out.println(numero%10);
		System.out.println(numero%100);
		System.out.println(numero%1000);
		System.out.println(numero%10000);
		System.out.println(numero);
		
		escaner.close();
		
	}

}
