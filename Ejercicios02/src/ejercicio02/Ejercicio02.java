package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una serie de palabras separadas por un espacio");
		String cadena = input.nextLine();
		
		System.out.println("La cadena con espacios es");
		System.out.println(cadena);
		cadena=cadena.replaceAll(" ","");
		
		System.out.println("La cadena sin espacios es");
		System.out.println(cadena);
		
		input.close();

	}

}
