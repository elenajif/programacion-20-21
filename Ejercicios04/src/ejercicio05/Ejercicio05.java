package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un mes");
		String mes = input.nextLine();

		// convierto el mes a minuscula
		mes = mes.toLowerCase();

		switch (mes) {
		case "enero":
		case "marzo":
		case "mayo":
		case "julio":
		case "agosto":
		case "octubre":
		case "diciembre":
			System.out.println("Tiene 31 d�as");
			break;
		case "febrero":
			System.out.println("Tiene 28 d�as");
			break;
		case "abril":
		case "junio":
		case "septiembre":
		case "noviembre":
			System.out.println("Tiene 30 d�as");
			break;
		default:
			System.out.println("Opci�n no contemplada");
		}

		input.close();

	}

}
