package ejercicio01previo;

import java.util.Scanner;

public class Ejercicio01Previo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame una opci�n (num�ro)");
		int opcion = input.nextInt();

		switch (opcion) {
		case 1:
			System.out.println("Has elegido opcion 1");
			break;
		case 2:
			System.out.println("Has elegido opcion 2");
			break;
		case 3:
			System.out.println("Has elegido opcion 3");
			break;
		case 10:
			System.out.println("Has elegido opcion 10");
			break;
		default:
			System.out.println("No has pulsado ninguna de las opciones elegidas");
		}

		input.close();

	}

}
