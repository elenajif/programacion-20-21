package ejercicio01previo;

import java.util.Scanner;

public class Ejercicio01Previo2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un numero");
		int numero1=input.nextInt();
		System.out.println("Dame otro numero");
		int numero2=input.nextInt();
	
		System.out.println("1.- suma");
		System.out.println("2.- resta");
		System.out.println("3.- multiplica");
		System.out.println("4.- divide");
		System.out.println("Dame una opci�n");
		int opcion = input.nextInt();

		switch (opcion) {
		case 1:
			System.out.println("La suma es "+(numero1+numero2));
			break;
		case 2:
			System.out.println("La resta es "+(numero1-numero2));
			break;
		case 3:
			System.out.println("La multiplicacion es "+(numero1*numero2));
			break;
		case 4:
			System.out.println("La division es "+(numero1/numero2));
			break;
		default:
			System.out.println("No has pulsado ninguna de las opciones elegidas");
		}

		input.close();

	}

}
