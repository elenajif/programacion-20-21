package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce una letra");
		char letra=lector.nextLine().charAt(0);
		
		switch (letra) {
		case 'a':
			System.out.println("Es la vocal a");
			break;
		case 'e':
			System.out.println("Es la vocal e");
			break;
		case 'i':
			System.out.println("Es la vocal i");
			break;
		case 'o':
			System.out.println("Es la vocal o");
			break;
		case 'u':
			System.out.println("Es la vocal u");
			break;
		default:
			System.out.println("no es vocal "+letra);
			
		}
		
		System.out.println("Introduce una letra");
		int numero=(int)lector.nextLine().charAt(0);
		
		switch (numero) {
		case 97:
			System.out.println("Es la vocal a");
			break;
		case 101:
			System.out.println("Es la vocal e");
			break;
		case 105:
			System.out.println("Es la vocal i");
			break;
		case 111:
			System.out.println("Es la vocal o");
			break;
		case 117:
			System.out.println("Es la vocal u");
			break;
		default:
			System.out.println("no es vocal "+letra);
			
		}
		
		lector.close();
	}

}
