package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un float");
		float numero1 = input.nextFloat();

		System.out.println("Introduce otro float");
		float numero2 = input.nextFloat();

		// limpiar el buffer
		input.nextLine();

		System.out.println("Introduzca el operador (+, -, *, /, %");
		char operador = input.nextLine().charAt(0);

		switch (operador) {
		case '+':
			System.out.println(numero1 + numero2);
			break;
		case '-':
			System.out.println(numero1 - numero2);
			break;
		case '*':
			System.out.println(numero1 * numero2);
			break;
		case '/':
			System.out.println(numero1 / numero2);
			break;
		case '%':
			System.out.println(numero1 % numero2);
			break;
		default:
			System.out.println("Operador no encontrado");

		}

		input.close();

	}

}
