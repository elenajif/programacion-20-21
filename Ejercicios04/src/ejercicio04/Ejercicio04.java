package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una nota entre 0 y 10");
		int numero = input.nextInt();

		switch (numero) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			System.out.println("insuficiente");
			break;
		case 5:
			System.out.println("suficiente");
			break;
		case 6:
			System.out.println("bien");
			break;
		case 7:
		case 8:
			System.out.println("notable");
			break;
		case 9:
		case 10:
			System.out.println("sobresaliente");
			break;
		default:
			System.out.println("La opci�n introducida no es valida");
		}

		input.close();

	}

}
