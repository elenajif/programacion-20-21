package cuentas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorCuentas {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Cuenta> listaCuentas;
	private ArrayList<CuentaCorriente> listaCuentasCorrientes;
	private ArrayList<CuentaAhorroFija> listaCuentasAhorro;
	private ArrayList<CuentaPlanPensiones> listaCuentasPlanPensiones;

	public GestorCuentas() {
		listaClientes = new ArrayList<Cliente>();
		listaCuentas = new ArrayList<Cuenta>();
		listaCuentasCorrientes = new ArrayList<CuentaCorriente>();
		listaCuentasAhorro = new ArrayList<CuentaAhorroFija>();
		listaCuentasPlanPensiones = new ArrayList<CuentaPlanPensiones>();
	}

	public void altaCliente(String dni, String nombre, String fechaNacimiento) {
		Cliente nuevoCliente = new Cliente(dni, nombre);
		nuevoCliente.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
		listaClientes.add(nuevoCliente);
	}

	public void listarClientes() {
		for (Cliente cliente : listaClientes) {
			if (cliente != null) {
				System.out.println(cliente);
			}
		}
	}

	public Cliente buscarCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente != null && cliente.getDni().equals(dni)) {
				return cliente;
			}
		}
		return null;
	}

	public void altaCuenta(String numero, double saldo, double interes) {
		Cuenta nuevaCuenta = new Cuenta(numero, saldo, interes);
		listaCuentas.add(nuevaCuenta);
	}

	public void altaCuentaPlanPensiones(String numero, double saldo, double interes, double cotizacion) {
		CuentaPlanPensiones nuevaCuenta = new CuentaPlanPensiones(numero, saldo, interes, cotizacion);
		listaCuentasPlanPensiones.add(nuevaCuenta);
	}

	public void altaCuentaCorriente(String numero, double saldo, double interes) {
		CuentaCorriente nuevaCuenta = new CuentaCorriente(numero, saldo, interes);
		listaCuentasCorrientes.add(nuevaCuenta);
	}

	public void altaCuentaAhorroFija(String numero, double saldo, double interes) {
		CuentaAhorroFija nuevaCuenta = new CuentaAhorroFija(numero, saldo, interes);
		listaCuentasAhorro.add(nuevaCuenta);
	}

	public void listarCuentas() {
		for (Cuenta cuenta : listaCuentas) {
			if (cuenta != null) {
				System.out.println(cuenta);
			}
		}
	}

	public void listarCuentasCorrientes() {
		for (CuentaCorriente cuenta : listaCuentasCorrientes) {
			if (cuenta != null) {
				System.out.println(cuenta);
			}
		}
	}

	public void listarCuentasAhorro() {
		for (CuentaAhorroFija cuenta : listaCuentasAhorro) {
			if (cuenta != null) {
				System.out.println(cuenta);
			}
		}
	}

	public void listarCuentasPlanPensiones() {
		for (CuentaPlanPensiones cuenta : listaCuentasPlanPensiones) {
			if (cuenta != null) {
				System.out.println(cuenta);
			}
		}
	}

	public Cuenta buscarCuenta(String numero) {
		for (Cuenta cuenta : listaCuentas) {
			if (cuenta != null && cuenta.getNumero().equals(numero)) {
				return cuenta;
			}
		}
		return null;
	}

	public CuentaCorriente buscarCuentaCorriente(String numero) {
		for (CuentaCorriente cuenta : listaCuentasCorrientes) {
			if (cuenta != null && cuenta.getNumero().equals(numero)) {
				return cuenta;
			}
		}
		return null;
	}

	public CuentaAhorroFija buscarCuentaAhorroFija(String numero) {
		for (CuentaAhorroFija cuenta : listaCuentasAhorro) {
			if (cuenta != null && cuenta.getNumero().equals(numero)) {
				return cuenta;
			}
		}
		return null;
	}

	public CuentaPlanPensiones buscarCuentaPlanPensiones(String numero) {
		for (CuentaPlanPensiones cuenta : listaCuentasPlanPensiones) {
			if (cuenta != null && cuenta.getNumero().equals(numero)) {
				return cuenta;
			}
		}
		return null;
	}

	public void eliminarCuenta(String numero) {
		Iterator<Cuenta> iteradorCuentas = listaCuentas.iterator();
		while (iteradorCuentas.hasNext()) {
			Cuenta cuenta = iteradorCuentas.next();
			if (cuenta.getNumero().equals(numero)) {
				iteradorCuentas.remove();
			}
		}
	}

	public void asignarCuentaCliente(String dni, String numero) {
		Cliente cliente = buscarCliente(dni);
		Cuenta cuenta = buscarCuenta(numero);
		cuenta.setTitular(cliente);
	}

	public void asignarCuentaAhorroCliente(String dni, String numero) {
		Cliente cliente = buscarCliente(dni);
		CuentaAhorroFija cuenta = buscarCuentaAhorroFija(numero);
		cuenta.setTitular(cliente);
	}

	public void asignarCuentaPlanPensionesCliente(String dni, String numero) {
		Cliente cliente = buscarCliente(dni);
		CuentaPlanPensiones cuenta = buscarCuentaPlanPensiones(numero);
		cuenta.setTitular(cliente);
	}
	
	public void asignarCuentaCorrienteCliente(String dni, String numero) {
		Cliente cliente = buscarCliente(dni);
		CuentaCorriente cuenta = buscarCuentaCorriente(numero);
		cuenta.setTitular(cliente);
	}


	public void listarCuentasDeTitular(String dni) {
		for (Cuenta cuenta : listaCuentas) {
			if (cuenta.getTitular() != null && cuenta.getTitular().getDni().equals(dni)) {
				System.out.println(cuenta);
			}
		}
	}

	public void ingreso(Cuenta miCuenta, int dinero) {
		miCuenta.ingreso(dinero);
	}

	public void reintegro(CuentaCorriente miCuenta, int dinero) {
		miCuenta.reintegro(dinero);
	}

	public void ingresoMes(CuentaAhorroFija miCuenta) {
		miCuenta.ingresoMes();
	}

}
