package ejerciciobasedatos1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TrabajandoConDatos1 {

	// 1.- arrancado servidor de base de datos
	// 2.- ejecutado el .sql puesto que la base de datos y tablas deben estar
	// creadas
	// 3.- conectar jar de mysql en carpeta libs
	// 4.- conectado con mi proyecto (configure Build Path)
	// 5.- no es obligatorio, pero se recomienda que el .sql este copiado en la
	// carpeta del proyecto

	// Operaciones -> CRUD

	private Connection conexion = null;
	PreparedStatement sentencia = null;

	public void conectar() {
		try {
			String servidor = "jdbc:mysql://localhost:3307/";
			String bbdd = "videojuegos";
			String user = "root";
			String password = "";
			conexion = DriverManager.getConnection(servidor + bbdd, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void seleccionar() {
		try {
			String sentenciaSql = "SELECT * FROM videojuegos";
			sentencia = conexion.prepareStatement(sentenciaSql);
			ResultSet resultado = sentencia.executeQuery();
			// mostramos los datos
			while (resultado.next()) {
				System.out.println(resultado.getString(1) + ", " + resultado.getString(2) + ", "
						+ resultado.getString(3) + ", " + resultado.getString(4) + ", " + resultado.getFloat(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	// insert into videojuegos(nombre,plataforma, genero, precio)
	// values ("mario bross","xbox","plataformas",49);

	public void insertar(String nombre, String plataforma, String genero, float precio) {
		try {
			String sentenciaSql = "INSERT INTO videojuegos(nombre, plataforma, genero, precio)" + "VALUES (?,?,?,?)";
			PreparedStatement sentencia;
			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, nombre);
			sentencia.setString(2, plataforma);
			sentencia.setString(3, genero);
			sentencia.setFloat(4, precio);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	// update videojuegos
	// set plataforma="xbox", genero="plataformas", precio=45
	// where nombre="Mario Bross"
	public void actualizar(String nombre, String plataforma, String genero, float precio) {
		try {
			String sentenciaSql = "UPDATE videojuegos SET plataforma=?, genero=?,precio=? WHERE nombre=?";
			PreparedStatement sentencia;
			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, plataforma);
			sentencia.setString(2, genero);
			sentencia.setFloat(3, precio);
			sentencia.setString(4, nombre);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void eliminar(String nombre) {
		try {
			String sentenciaSql = "DELETE FROM videojuegos WHERE nombre=?";
			PreparedStatement sentencia;
			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, nombre);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void desconectar() {
		try {
			sentencia.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
