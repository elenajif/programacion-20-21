package ejerciciobasedatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TrabajandoConDatos {

	// 1.- arrancado servidor de base de datos
	// 2.- ejecutado el .sql puesto que la base de datos y tablas deben estar
	// creadas
	// 3.- conectar jar de mysql en carpeta libs
	// 4.- conectado con mi proyecto (configure Build Path)
	// 5.- no es obligatorio, pero se recomienda que el .sql este copiado en la
	// carpeta del proyecto

	// Operaciones -> CRUD

	private Connection conexion = null;
	PreparedStatement sentencia = null;

	public void conectar() throws SQLException {
		String servidor = "jdbc:mysql://localhost:3307/";
		String bbdd = "videojuegos";
		String user = "root";
		String password = "";
		conexion = DriverManager.getConnection(servidor + bbdd, user, password);
	}

	public void seleccionar() throws SQLException {
		String sentenciaSql = "SELECT * FROM videojuegos";
		sentencia = conexion.prepareStatement(sentenciaSql);
		ResultSet resultado = sentencia.executeQuery();
		// mostramos los datos
		while (resultado.next()) {
			System.out.println(resultado.getString(1) + ", " + resultado.getString(2) + ", " + resultado.getString(3)
					+ ", " + resultado.getString(4) + ", " + resultado.getFloat(5));
		}
	}

	// insert into videojuegos(nombre,plataforma, genero, precio)
	// values ("mario bross","xbox","plataformas",49);

	public void insertar(String nombre, String plataforma, String genero, float precio) throws SQLException {
		String sentenciaSql = "INSERT INTO videojuegos(nombre, plataforma, genero, precio)" + "VALUES (?,?,?,?)";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1, nombre);
		sentencia.setString(2, plataforma);
		sentencia.setString(3, genero);
		sentencia.setFloat(4, precio);
		sentencia.executeUpdate();
	}

	//update videojuegos
	//set plataforma="xbox", genero="plataformas", precio=45
	//where nombre="Mario Bross"
	public void actualizar(String nombre, String plataforma, String genero, float precio) throws SQLException {
		String sentenciaSql = "UPDATE videojuegos SET plataforma=?, genero=?,precio=? WHERE nombre=?";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1, plataforma);
		sentencia.setString(2, genero);
		sentencia.setFloat(3, precio);
		sentencia.setString(4, nombre);
		sentencia.executeUpdate();
	}

	public void eliminar(String nombre) throws SQLException {
		String sentenciaSql="DELETE FROM videojuegos WHERE nombre=?";
		PreparedStatement sentencia=conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1, nombre);
		sentencia.executeUpdate();
	}

	public void desconectar() throws SQLException {
		sentencia.close();
	}
}
