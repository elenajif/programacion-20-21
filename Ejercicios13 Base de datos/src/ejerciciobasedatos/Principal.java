package ejerciciobasedatos;

import java.sql.SQLException;
import java.util.Scanner;

public class Principal {
	public static void main (String[] args) throws SQLException {
		TrabajandoConDatos misDatos = new TrabajandoConDatos();
		Scanner in = new Scanner(System.in);
		System.out.println("Conectamos");
		misDatos.conectar();
		
		System.out.println("Mostramos datos");
		misDatos.seleccionar();
		
		System.out.println("Inserto dato");
		System.out.println("Dame el nombre del videojuego");
		String nombre=in.nextLine();
		System.out.println("Dame la plataforma del videojuego");
		String plataforma=in.nextLine();
		System.out.println("Dame el genero del videojuego");
		String genero=in.nextLine();
		System.out.println("Dame el precio del videojuego");
		float precio=in.nextFloat();
		misDatos.insertar(nombre, plataforma, genero, precio);
		misDatos.seleccionar();
		
		in.nextLine();
		
		System.out.println("Actualizamos datos");
		System.out.println("Dame el nombre del videojuego");
		nombre=in.nextLine();
		System.out.println("Dame la plataforma del videojuego");
		plataforma=in.nextLine();
		System.out.println("Dame el genero del videojuego");
		genero=in.nextLine();
		System.out.println("Dame el precio del videojuego");
		precio=in.nextFloat();
		misDatos.actualizar(nombre, plataforma, genero, precio);
		misDatos.seleccionar();
		
		in.nextLine();
		
		System.out.println("Eliminamos datos");
		System.out.println("Dame el nombre del videojuego");
		nombre=in.nextLine();
		misDatos.eliminar(nombre);
		misDatos.seleccionar();
		
		System.out.println("Desconectamos");
		misDatos.desconectar();
		
		in.close();
	}
}
