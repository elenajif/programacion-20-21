package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce un caracter");
		char caracter1=lector.nextLine().charAt(0);
		
		if (caracter1>='0' && caracter1<='9') {
			System.out.println("es una cifra");
		}else {
			System.out.println("no lo es");
		}
		
		lector.close();

	}

}
