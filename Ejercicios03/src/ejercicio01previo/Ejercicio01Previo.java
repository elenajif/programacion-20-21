package ejercicio01previo;

public class Ejercicio01Previo {

	public static void main(String[] args) {
		int numero=3;
		if (numero<25) {
			System.out.println("El n�mero es menor de 25");
		}
		
		int numero2=4;
		if (numero2>3 && numero2<7) {
			System.out.println("El n�mero es mayor de 3 y menor de 7");
		}
		
		int numero3=50;
		if (numero3>0 || numero3>120) {
			System.out.println("El n�mero o es mayor de 0 o es mayor de 120");
		}
		
		int numero4=15;
		if (numero4<3) {
			System.out.println("El n�mero es menor de 3");
		} else {
			System.out.println("El n�mero es mayor o igual a 3");
		}
		
		int numero5=20;
		if (numero5>3 && numero5<15) {
			System.out.println("El n�mero es mayor de 3 y menor de 15");
		} else {
			System.out.println("El n�mero no cumple las condiciones");
		}
		
		int numero6=46;
		if (numero6>49 || numero6>150) {
			System.out.println("El n�mero es mayor de 49 o mayor de 150");
		} else {
			System.out.println("No cumple las condiciones");
		}

	}

}
