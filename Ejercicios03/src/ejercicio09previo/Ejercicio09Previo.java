package ejercicio09previo;

import java.util.Scanner;

public class Ejercicio09Previo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame tu edad");
		int edad = input.nextInt();

		System.out.println("primer if");
		if (edad >= 18) {
			System.out.println("Tienes 18 o mas.");
		} else {
			System.out.println("Tienes menos de 18.");
		} 
		
		System.out.println("segundo if");		
		if (edad >= 18) {
			System.out.println("Tienes 18 o mas.");
		} else if (edad >= 15) {
			System.out.println("Tienes 15 a�os o mas pero menos de 18.");
		} else {
			System.out.println("Tienes menos de 15 a�os.");
		}
		
		System.out.println("tercer if");
		if (edad >= 18) {
			System.out.println("Tienes 18 o mas.");
		} else if (edad >= 15) {
			System.out.println("Tienes 15 a�os o mas pero menos de 18.");
		} else if (edad >= 10) {
			System.out.println("Tienes 10 a�os o mas pero menos de 15.");
		} else {
			System.out.println("Tienes menos de 10 a�os.");
		}

		input.close();
	}

}
