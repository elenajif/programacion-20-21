package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);

		System.out.println("Introduce una cadena");
		String cadena1 = escaner.nextLine();

		System.out.println("Introduce otra cadena");
		String cadena2 = escaner.nextLine();

		if (cadena1.equals(cadena2)) {
			System.out.println("mismos caracteres - coincidencia completa");
		} else {
			if (cadena1.equalsIgnoreCase(cadena2)) {
				System.out.println("mismos caracteres - aunque may�sculas y min�sculas");
			} else {
				System.out.println("no son iguales");
			}
		}

		escaner.close();

	}

}
