package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// pido dia, mes, a�o
		System.out.println("Introduce dia 1");
		int dia = input.nextInt();
		
		System.out.println("Introduce mes 1");
		int mes = input.nextInt();
		
		System.out.println("Introduce a�o 1");
		int anno = input.nextInt();
		
		// pido dia, mes, a�o
		System.out.println("Introduce dia 2");
		int dia2 = input.nextInt();
		
		System.out.println("Introduce mes 2");
		int mes2 = input.nextInt();
		
		System.out.println("Introduce a�o 2");
		int anno2 = input.nextInt();

		// paso las dos fechas a numero de dias
		int diasFecha1=dia+mes*30+anno*360;
		//15-2-2005
		//15+2*30+2005*360
		int diasFecha2=dia2+mes2*30+anno2*360;
		
		// calculo la diferencia de dias
		int diferencia =diasFecha1-diasFecha2;

		// si  una la primera fecha, es anterior a la segunda
		// saldra una cantidad negativa
		// invierto el signo
		if (diferencia<0) {
			diferencia=-diferencia;
		}
		
		System.out.println("La diferencia de d�as es "+diferencia);
		
		input.close();
		
	}
}
