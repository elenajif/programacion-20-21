package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int numero= input.nextInt();
		
		boolean condicion=numero%2==0;
		
		if(condicion) {
			System.out.println("El n�mero es par");
		} else {
			System.out.println("El n�mero es impar");
		}
		
		//if (numero%2==0)
		
		input.close();
	}

}
