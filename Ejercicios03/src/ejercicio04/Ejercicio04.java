package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Dame un caracter");
		char caracter1=lector.nextLine().charAt(0);
		
		System.out.println("Dame otro caracter");
		char caracter2=lector.nextLine().charAt(0);
		
		if (caracter1==caracter2) {
			System.out.println("son iguales");
		} else {
			System.out.println("no son iguales");
		}
		
		lector.close();

	}

}
