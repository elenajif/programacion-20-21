package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame un caracter");
		char letra = input.nextLine().charAt(0);
		
		if (letra>='A' && letra<='Z') {
			System.out.println("Es una letra may�scula");
		} else {
			System.out.println("Es una letra min�scula");
		}
		
		input.close();
		
		System.out.println((int)letra);

	}

}
