package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Dame un caracter");
		char caracter1=lector.nextLine().charAt(0);
		
		System.out.println("Dame otro caracter");
		char caracter2=lector.nextLine().charAt(0);
		
		boolean caracter1Minuscula=caracter1>='a' && caracter1<='z';
		boolean caracter2Minuscula=caracter2>='a' && caracter2<='z';
		
		if (caracter1Minuscula && caracter2Minuscula) {
			System.out.println("Son minuscula");
		} else {
			System.out.println("no lo son ");
		}
		
		// otra forma de hacerlo (todo en uno)
		if ((caracter1>='a' && caracter1<='z') && (caracter2>='a' && caracter2<='z')) {
			System.out.println("Son minuscula");
		} else {
			System.out.println("no lo son ");
		}
		
		
		
		lector.close();

	}

}
