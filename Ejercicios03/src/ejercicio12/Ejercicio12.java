package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);

		System.out.println("Introduce el n�mero de mes");
		int mes = escaner.nextInt();

		if (mes < 1 || mes > 12) {
			System.out.println("no existe el mes");
		} else {
			if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
				System.out.println("Tiene 31");
			} else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
				System.out.println("Tiene 30");
			} else {
				System.out.println("Tiene 28");
			}
		}

		escaner.close();

	}

}
