package ejerciciorepaso2;

public class Metodos {

	// metodo que recibe un vector y suma sus componentes
	public static void sumaVector(int[] vector) {
		int total = 0;
		for (int i = 0; i < vector.length; i++) {
			total = total + vector[i];
		}
		System.out.println("La suma de las componentes del vector es " + total);
	}

	// metodo que recibe un vector y lo muestra al rev�s
	public static void cadenaInversa(String[] vector) {
		for (int i = vector.length - 1; i >= 0; i--) {
			System.out.print(vector[i] + " ");
		}
		System.out.println("");
	}

}
