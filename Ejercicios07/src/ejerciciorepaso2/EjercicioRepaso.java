package ejerciciorepaso2;

import java.util.Scanner;

public class EjercicioRepaso {
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		
		int opcion;
		do {

			System.out.println("__________________________________________________");
			System.out.println("               Menu              ");
			System.out.println("1.- Sumar las componentes de un vector de enteros");
			System.out.println("2.- Mostrar un vector de cadenas a la inversa");
			System.out.println("3.- Salir");
			System.out.println("___________________________________________________");

			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				System.out.println("Sumamos componentes");
				Metodos.sumaVector(EjercicioRepaso.leerVector());
				break;
			case 2:
				System.out.println("Mostramos el vector de cadenas a la inversa");
				Metodos.cadenaInversa(EjercicioRepaso.leerVectorCadenas());
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("Opcion no seleccionada");
			}
		} while (opcion != 3);

		input.close();

	}
	
	// metodo que lee un vector de enteros
	public static int[] leerVector() {
		System.out.println("Pedimos vector de enteros");

		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();
		int[] vectorEnteros = new int[tam];

		for (int i = 0; i < vectorEnteros.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			vectorEnteros[i] = input.nextInt();
		}

		return vectorEnteros;
	}

	// metodo que lee un vector de cadenas
	public static String[] leerVectorCadenas() {
		System.out.println("Pedimos vector de cadenas");
		System.out.println("Dame el tama�o del vector");
		int tam1 = input.nextInt();

		input.nextLine();

		String[] vectorCadenas = new String[tam1];

		for (int i = 0; i < vectorCadenas.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			vectorCadenas[i] = input.nextLine();
		}
		return vectorCadenas;
	}

}
