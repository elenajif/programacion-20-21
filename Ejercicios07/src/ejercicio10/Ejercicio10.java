package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		char[] caracteres = new char[10];
		
		for(int i = 0; i < caracteres.length; i++){
			System.out.println("Introduce un caracter");
			caracteres[i] = input.nextLine().charAt(0);
		}
		
		//Muestro el contenido del array
		for(int i = 0; i < caracteres.length; i++){
			System.out.print(caracteres[i] + " ");
		}
		
		//Ordeno
		ordenar(caracteres);
		
		//En la clase Arrays tengo el metodo sort()
		//Recibe un array de una dimensi�n y lo ordena
		
		//Vuelvo a mostrar el contenido del array
		System.out.println();
		for(int i = 0; i < caracteres.length; i++){
			System.out.print(caracteres[i] + " ");
		}
		
		input.close();
	}

	private static void ordenar(char[] array) {
		
		//Cada vez que me situo en una celda, recorro
		//con el segundo bucle, als celdas siguientes
		//en busca del valor m�s peque�o, que ir� colocando
		//en la celda actual
		//as� sucesivamente a lo largo de todas las celdas
		for(int i = 0; i < array.length - 1; i++){
			for(int j = i + 1; j < array.length; j++){
				if(array[j] < array[i]){
					char aux = array[i];
					array[i] = array[j];
					array[j] = aux;
				}
				
			}
		}
		
		
	}

}
