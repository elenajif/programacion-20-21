package ejercicioprevio1;

import java.util.Scanner;

public class EjercicioPrevio7 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame el tama�o del vector");
		int tam=input.nextInt();
		
		System.out.println("Creamos el vector");
		int[] miVector= new int[tam];
		
		System.out.println("Rellenamos el vector");
		
		for (int i=0; i<miVector.length;i++) {
			System.out.println("Dame la componente "+i);
			miVector[i]=input.nextInt();
		}
		
		System.out.println("Sumo componentes del vector");
		MetodosPrevio1.sumaComponentes(miVector);
		input.close();

	}
	

}
