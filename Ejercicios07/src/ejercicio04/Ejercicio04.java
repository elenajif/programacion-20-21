package ejercicio04;

import java.util.Scanner;

import ejercicio03.Ejercicio03;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce tama�o del array");
		int tamano = input.nextInt();
		
		char[] letras = new char[tamano];
		
		for(int i = 0; i < letras.length; i++){
			
			letras[i] = (char) ((Math.random() * ('z' - 'a' + 1)) + 'a');
			System.out.print( letras[i] + " ");
		}
		System.out.println();
		
		letras = Ejercicio03.sustituyeVocales(letras);
		
		for(int i = 0; i < letras.length; i++){
			System.out.print(letras[i] + " ");
		}
		
		input.close();
	}

}
