package ejercicioprevio2;

import java.util.Scanner;

public class EjercicioPrevio2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// creamos una matriz de enteros
		int[][] matriz = new int[5][4];
		// pedimos los valores por teclado
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print("Dame la componente (" + i + "," + j + ")");
				matriz[i][j] = input.nextInt();
			}
		}
		
		//mostramos los datos
		for (int i=0;i<matriz.length;i++) {
			for (int j=0;j<matriz[i].length;j++) {
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println("");
		}

		input.close();
	}

}
