package ejercicioprevio3;

public class EjercicioPrevio3 {

	public static void main(String[] args) {
		int[][] matriz = { { 2, 4 }, { 5, 6 }, { 7, 2 }, { 5, 6 } };

		System.out.println("Muestro datos");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}

		System.out.println("Muestro indicando coordenadads");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print("("+i+","+j+")"+matriz[i][j] + " ");
			}
			System.out.println("");
		}

		System.out.println("Muestro indicando filas y columnas");
		System.out.println("   C     C ");
		for (int i = 0; i < matriz.length; i++) {
			System.out.print("F" + i + " ");
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(j + "  ");
				System.out.print(matriz[i][j] + "  ");
			}
			System.out.println("");
		}
	}

}
