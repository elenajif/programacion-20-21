package ejercicioprevio4;

import java.util.Arrays;

public class EjercicioPrevio4 {

	public static void main(String[] args) {
		// declaro un array
		int[] arrayEnteros = { 4, 5, 7, 3, 8, 2, 9, 1 };

		// imprimo el array
		System.out.println("Mostramos vector");
		System.out.println(Arrays.toString(arrayEnteros));
		// ordeno el array
		System.out.println("Ordenamos");
		Arrays.sort(arrayEnteros);
		// imprimo el array ordenado
		System.out.println("Mostramos vector ordenado");
		System.out.println(Arrays.toString(arrayEnteros));
		// hago una copia del array
		System.out.println("Copiamos el vector");
		int[] copia = Arrays.copyOf(arrayEnteros, arrayEnteros.length);
		// muestro la copia del array
		System.out.println("Mostramos la copia");
		System.out.println(Arrays.toString(copia));
		// comparamos dos vectores
		System.out.println("Comparamos vectores");
		if (Arrays.equals(arrayEnteros, copia))
			System.out.println("Son iguales");
		// crear un vector de caracteres de [10] y rellenarlo con *
		System.out.println("Creo un vector de caracteres");
		char[] vectorCaracteres = new char[10];
		System.out.println("Rellenamos con *");
		Arrays.fill(vectorCaracteres, '*');
		System.out.println("Imprimir vector caracteres");
		System.out.println(Arrays.toString(vectorCaracteres));
		// crear un vector de doubles de [5] y rellenarlo con aleatorios entre 3 y 5
		System.out.println("Creo un vector de doubles");
		double[] vectorDoubles = new double[5];
		System.out.println("Rellenamos con aleatorios");
		Arrays.fill(vectorDoubles, (Math.random() * 2) + 3);
		System.out.println("Imprimir vector doubles");
		System.out.println(Arrays.toString(vectorDoubles));

	}

}
