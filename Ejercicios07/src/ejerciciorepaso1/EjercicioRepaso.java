package ejerciciorepaso1;

import java.util.Scanner;

public class EjercicioRepaso {
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		
		int opcion;
		do {

			System.out.println("__________________________________________________");
			System.out.println("               Menu              ");
			System.out.println("1.- Sumar las componentes de un vector de enteros");
			System.out.println("2.- Mostrar un vector de cadenas a la inversa");
			System.out.println("3.- Salir");
			System.out.println("___________________________________________________");

			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				System.out.println("Sumamos componentes");
				Metodos.sumaVector(Metodos.leerVector());
				break;
			case 2:
				System.out.println("Mostramos el vector de cadenas a la inversa");
				Metodos.cadenaInversa(Metodos.leerVectorCadenas());
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("Opcion no seleccionada");
			}
		} while (opcion != 3);

		input.close();

	}

}
