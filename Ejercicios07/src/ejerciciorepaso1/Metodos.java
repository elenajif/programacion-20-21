package ejerciciorepaso1;

import java.util.Scanner;

public class Metodos {

	static Scanner input = new Scanner(System.in);

	// metodo que recibe un vector y suma sus componentes
	public static void sumaVector(int[] vector) {
		int total = 0;
		for (int i = 0; i < vector.length; i++) {
			total = total + vector[i];
		}
		System.out.println("La suma de las componentes del vector es " + total);
	}

	// metodo que recibe un vector y lo muestra al rev�s
	public static void cadenaInversa(String[] vector) {
		for (int i = vector.length - 1; i >= 0; i--) {
			System.out.print(vector[i] + " ");
		}
		System.out.println("");
	}

	// metodo que lee un vector de enteros
	public static int[] leerVector() {
		System.out.println("Pedimos vector de enteros");

		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();
		int[] vectorEnteros = new int[tam];

		for (int i = 0; i < vectorEnteros.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			vectorEnteros[i] = input.nextInt();
		}

		return vectorEnteros;
	}

	// metodo que lee un vector de cadenas
	public static String[] leerVectorCadenas() {
		System.out.println("Pedimos vector de cadenas");
		System.out.println("Dame el tama�o del vector");
		int tam1 = input.nextInt();

		input.nextLine();

		String[] vectorCadenas = new String[tam1];

		for (int i = 0; i < vectorCadenas.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			vectorCadenas[i] = input.nextLine();
		}
		return vectorCadenas;
	}
}
