package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		int[] enteros;		
		enteros=pide10Enteros();		
		for (int i=enteros.length-1;i>=0;i--) {
			System.out.print(enteros[i]+" ");
		}
	}
	
	static int[] pide10Enteros() {
		Scanner input = new Scanner(System.in);
		int[] array = new int[10];
		
		for (int i=0;i<array.length;i++) {
			System.out.println("Dame un numero");
			array[i]=input.nextInt();
		}
		
		input.close();
		return array;
	}
}
