package ejercicioprevio;

public class EjercicioPrevio3 {

	public static void main(String[] args) {
		int num1=5;
		int num2;
		num2=num1;
		System.out.println("num1 "+num1);
		System.out.println("num2 "+num2);
		
		int[] vectorEnteros= {1,2,3};
		int[] vectorEnteros1;
		vectorEnteros1=vectorEnteros;
		
		System.out.println("vectorEnteros");
		for (int i=0;i<vectorEnteros.length;i++) {
			System.out.println(vectorEnteros[i]);
		}

		System.out.println("vectorEnteros1");
		for (int i=0;i<vectorEnteros1.length;i++) {
			System.out.println(vectorEnteros1[i]);
		}

	}

}
