package ejercicioprevio;

public class EjercicioPrevio {

	public static void main(String[] args) {
		// vector -> datos del mismo tipo
		// vector -> tama�o
		// vector -> indice para poder acceder a sus "casillas" (componentes)
		// declaracion de un entero -> int x;
		// declaracion de un vector de enteros -> int[] x;
		// inicializar un entero -> x=3;
		// inicializar un vector -> x=new int[4]; -> 4 es el tama�o del vector
		// vectores son objetos por eso al crearse debe usarse new
		
		//creacion de un vector de enteros de tama�o 4
		int[] vectorEnteros = new int[4];
		
		//creacion de un vector de doubles de tama�o 3
		double[] vectorDoubles = new double[3];
		
		//creacion de un vector de String de tama�o 5
	    String[] vectorCadenas = new String[5];
	    
	    //creacion de un vector con un tama�o en una variable
	    int tam=20;
	    int[] vectorEnteros1=new int[tam];
	    
	    //crear un vector y darle valores al mismo tiempo
	    //los valores ya los conoces
	    // llaves -> { } -> cuando conoces los valores
	    int[] vectorNumeros= {3,4,5,6,7};
	    
	    String[] nombres= {"Maria","Carlos","Raquel"};
	    
	    //mostrar la componente de un vector
	    System.out.println(vectorNumeros[0]);
	    System.out.println(vectorNumeros[3]);
	    System.out.println(nombres[2]);
	    
	    //darle valor a una componente de un vector
	    vectorNumeros[0]=55;
	    System.out.println(vectorNumeros[0]);
	    
	    nombres[1]="Pepe";
	    System.out.println(nombres[1]);
	    
	    //mostrar todas las componentes de un vector usando un for
	    for (int i=0;i<vectorNumeros.length;i++) {
	    	System.out.println("La componente "+i+" es "+vectorNumeros[i]);
	    }
	    
	    for (int i=0;i<nombres.length;i++) {
	    	System.out.println("La componente "+i+" es "+nombres[i]);
	    }
	    

	}

}
