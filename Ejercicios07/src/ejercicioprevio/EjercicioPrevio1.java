package ejercicioprevio;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

public class EjercicioPrevio1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//declaracion del vector
		int[] vectorEnteros = new int[4];
		
		//lectura de datos del vector
		for (int i=0;i<vectorEnteros.length;i++) {
			System.out.println("Dame la componente "+i+" del vector");
			vectorEnteros[i]=input.nextInt();
		}
		
		//mostrar los datos del vector
		for (int i=0;i<vectorEnteros.length;i++) {
			System.out.println("La componente "+i+" del vector es "+vectorEnteros[i]);
		}
		
		input.close();

	}

}
