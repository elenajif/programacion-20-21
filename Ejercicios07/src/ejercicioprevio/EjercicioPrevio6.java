package ejercicioprevio;

import java.util.Scanner;

public class EjercicioPrevio6 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();

		System.out.println("Creamos el vector");
		double[] vector1 = new double[tam];

		EjercicioPrevio6.pedirDatos(vector1);
		EjercicioPrevio6.mostrarDatos(vector1);

		input.close();
	}

	static void pedirDatos(double[] vector1) {
		System.out.println("Pedimos los datos");
		for (int i = 0; i < vector1.length; i++) {
			System.out.println("Dame la componente del vector");
			vector1[i] = input.nextDouble();
		}
	}

	static void mostrarDatos(double[] vector1) {
		System.out.println("Mostramos los datos");
		for (int i = 0; i < vector1.length; i++) {
			System.out.println(vector1[i]);
		}
	}

}
