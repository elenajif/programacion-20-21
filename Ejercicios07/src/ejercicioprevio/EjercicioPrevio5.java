package ejercicioprevio;

import java.util.Scanner;

public class EjercicioPrevio5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame el tama�o del vector");
		int tam=input.nextInt();
		
		System.out.println("Creamos el vector");
		double[] vector1 = new double[tam];
		
		System.out.println("Pedimos los datos");
		for (int i=0;i<vector1.length;i++) {
			System.out.println("Dame la componente del vector");
			vector1[i]=input.nextDouble();
		}
		
		System.out.println("Mostramos los datos");
		for (int i=0;i<vector1.length;i++) {
			System.out.println(vector1[i]);
		}

		input.close();
	}

}
