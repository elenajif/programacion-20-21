package ejercicioprevio;

public class EjercicioPrevio8 {

	public static void main(String[] args) {
	    
	    String[] nombres= {"Maria de los Dolores","Carlos V","Raquel"};

	    //mostrar todas las componentes de un vector usando un for
	    for (int i=0;i<nombres.length;i++) {
	    	System.out.println("La componente "+i+" es "+nombres[i]);
	    }
	    
	    //muestro la primera componente
	    System.out.println("Componente 1 "+nombres[1]);
	    //con split corto por una caracter (el espacio)
	    // vector -> 0 Maria 1 de 2 los 3 Dolores
	    // vector[0] Maria
	    // vector[2] los
	    System.out.println("Split nombres componente 0 "+nombres[0].split(" ")[0]);
	    System.out.println("Split nombres componente 0 "+nombres[0].split(" ")[2]);
	    
	    System.out.println("Split nombres componente 1 "+nombres[1].split(" ")[0]);
	    System.out.println("Split nombres componente 1 "+nombres[1].split(" ")[1]);
	    
	    String saludo ="hola caracola";
	    System.out.println("Split cadena saludo "+saludo.split(" ")[0]);
	    System.out.println("Split cadena saludo "+saludo.split(" ")[1]);
	    
	    //delante del split lo que quiero cortar
	    //entre parentesis del split por lo que quiero cortar
	    //detras del parentesis del split la componente del vector que ha generado el split
	    //lo ha generado dividiendo la cadena en funcion de lo que he puesto entre comillas
	    
	    String cuentaBancaria ="2085-22-22254647894756456";
	    System.out.println("Split cadena cuenta bancaria "+cuentaBancaria.split("-")[0]);
	    System.out.println("Split cadena cuenta bancaria "+cuentaBancaria.split("-")[1]);
	    System.out.println("Split  cadena cuenta bancaria "+cuentaBancaria.split("-")[2]);
	    
	    
	    

	}

}
