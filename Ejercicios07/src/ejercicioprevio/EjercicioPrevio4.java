package ejercicioprevio;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

public class EjercicioPrevio4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//declaracion del vector
		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();
		String[] vectorString = new String[tam];
		input.nextLine();
		
		//lectura de datos del vector
		for (int i=0;i<vectorString.length;i++) {
			System.out.println("Dame la componente "+i+" del vector");
			vectorString[i]=input.nextLine();
		}
		
		//mostrar los datos del vector
		for (int i=0;i<vectorString.length;i++) {
			System.out.println("La componente "+i+" del vector es "+vectorString[i]);
		}
		
		input.close();

	}

}
