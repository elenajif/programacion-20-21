package ejercicioprevio;

import java.util.Scanner;

public class EjercicioPrevio2 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		// declaracion del vector
		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();
		String[] vectorString = new String[tam];
		input.nextLine();
		
		EjercicioPrevio2.pedirDatos(vectorString);
		EjercicioPrevio2.mostrarDatos(vectorString);

		input.close();

	}

	static void pedirDatos(String[] vectorString) {
		// lectura de datos del vector
		for (int i = 0; i < vectorString.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			vectorString[i] = input.nextLine();
		}
	}

	static void mostrarDatos(String[] vectorString) {
		// mostrar los datos del vector
		for (int i = 0; i < vectorString.length; i++) {
			System.out.println("La componente " + i + " del vector es " + vectorString[i]);
		}
	}

}
