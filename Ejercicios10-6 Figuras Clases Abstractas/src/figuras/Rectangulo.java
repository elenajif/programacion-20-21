package figuras;

public class Rectangulo extends Figura {
	private int largo;
	private int alto;

	public Rectangulo(int x, int y, int largo, int alto) {
		super(x, y);
		this.largo = largo;
		this.alto = alto;
	}

	@Override
	public float getArea() {
		return this.alto * this.largo;
	}

	@Override
	public void dibujar() {
		System.out.println("Dibujo un rectangulo");
	}

}
