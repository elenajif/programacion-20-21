package programa;

import java.util.ArrayList;

import figuras.Circulo;
import figuras.Cuadrado;
import figuras.Figura;
import figuras.Rectangulo;
import figuras.Triangulo;

public class EjercicioFiguras {

	public static void main(String[] args) {
		// creo figuras
		Triangulo triangulo = new Triangulo(0, 0, 5, 3);
		Cuadrado cuadrado = new Cuadrado(0, 0, 2);
		Rectangulo rectangulo = new Rectangulo(0, 0, 5, 3);
		Circulo circulo = new Circulo(0, 0, 3);

		// creamos un ArrayList de figuras
		ArrayList<Figura> listaFiguras = new ArrayList<Figura>();

		// a�adimos las figuras al ArrayList
		listaFiguras.add(triangulo);
		listaFiguras.add(cuadrado);
		listaFiguras.add(rectangulo);
		listaFiguras.add(circulo);

		// movemos a la coordenada (1,1)
		for (int i = 0; i < listaFiguras.size(); i++) {
			Figura fig = listaFiguras.get(i);
			fig.mover(i, i);
		}

		// mostramos las coordenadas
		for (int i = 0; i < listaFiguras.size(); i++) {
			Figura fig = listaFiguras.get(i);
			System.out.println("Origen de la figura (" + fig.getX() + "," + fig.getY() + ")");
		}

		// mostramos el area
		for (int i = 0; i < listaFiguras.size(); i++) {
			Figura fig = listaFiguras.get(i);
			System.out.println("Area de la figura " + fig.getArea());
		}

		// mostramos el metodo dibujar
		for (int i = 0; i < listaFiguras.size(); i++) {
			Figura fig = listaFiguras.get(i);
			System.out.println("Dibujamos ");
			fig.dibujar();
		}

	}

}
