package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Introduce la base");
		int base = input.nextInt();

		System.out.println("Introduce el exponente");
		int exponente = input.nextInt();

		int resultado = 1;

		// bucle con la operacion/es
		for (int i = 0; i < exponente; i++) {
			resultado = resultado * base;
		}

		System.out.println("El resultado es: " + resultado);

		input.close();
	}

}
