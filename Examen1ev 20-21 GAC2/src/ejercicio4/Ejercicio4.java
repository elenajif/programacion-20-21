package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String cadena;
		int contadorMayus = 0;
		int contadorCarac = 0;
		int num = 0;
		int numMayor = 0;
		int contadorLarga = 0;
		String cadenaLarga = "";
		int numMenor = 0;
		int contadorCorta = 0;
		String cadenaCorta = "";

		do {
			System.out.println("Dame una cadena");
			cadena = in.nextLine();
			if (!cadena.equalsIgnoreCase("fin")) {
				for (int i = 0; i < cadena.length(); i++) {
					char caracter = cadena.charAt(i);
					if (caracter >= 'A' && caracter <= 'Z') {
						contadorMayus++;
					}
				}
				if (cadena.length() > 5) {
					contadorCarac++;
				}
				num = cadena.length();
				if (contadorLarga == 0) {
					numMayor = num;
					cadenaLarga = cadena;
					contadorLarga++;
				}
				if (num > numMayor) {
					numMayor = num;
					cadenaLarga = cadena;
				}
				if (contadorCorta == 0) {
					numMenor = num;
					cadenaCorta = cadena;
					contadorCorta++;
				}
				if (num < numMenor) {
					numMenor = num;
					cadenaCorta = cadena;
				}
			}
		} while (!cadena.equalsIgnoreCase("fin"));

		System.out.println("La cantidad de may�sculas es: " + contadorMayus);
		System.out.println("La cantidad de cadenas de m�s de 5 es: " + contadorCarac);
		System.out.println("La cadena m�s larga es: " + cadenaLarga);
		System.out.println("La cadena m�s corta es: " + cadenaCorta);
		in.close();
	}
}
