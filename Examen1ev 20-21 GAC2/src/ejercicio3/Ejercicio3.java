package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int opcion;
		do {
			System.out.println("1- contar may�sculas");
			System.out.println("2- contrase�a correcta");
			System.out.println("3- salir");
			opcion = in.nextInt();
			switch (opcion) {
			case 1:
				int contador = 0;
				in.nextLine();
				System.out.println("Introduce una cadena");
				String cadena = in.nextLine();
				for (int i = 0; i < cadena.length(); i++) {
					if (cadena.charAt(i) >= 'A' && cadena.charAt(i) <= 'Z') {
						contador++;
					}
				}
				System.out.println("La cadena tiene " + contador + " may�sculas");
				break;

			case 2:
				in.nextLine();
				System.out.println("Introduce una contrase�a");
				String contrase�a = in.nextLine().toLowerCase();
				int contadorCaracteres = 0;
				if (contrase�a.contains("a") || contrase�a.contains("e") || contrase�a.contains("i")
						|| contrase�a.contains("o") || contrase�a.contains("u")) {
					for (int i = 0; i < contrase�a.length(); i++) {
						if (contrase�a.charAt(i) >= 'a' && contrase�a.charAt(i) <= 'z') {
						} else {
							contadorCaracteres++;
						}
					}
					if (contadorCaracteres == 0) {
						System.out.println("Has introducido una contrase�a correcta");
					} else {
						System.out.println("Has introducido una contrase�a inv�lida");
					}
				} else {
					System.out.println("Has introducido una contrase�a inv�lida");
				}
				break;

			case 3:
				System.out.println("Sales del programa");
				break;
			default:
				System.out.println("Introduce una opci�n contemplada en el men�");
			}
		} while (opcion != 3);

		in.close();
	}

}
