package clases;

import java.time.LocalDate;

public class Aula {
	private Alumno[] alumnos;

	// constructor
	public Aula(int maxAlumnos) {
		this.alumnos = new Alumno[maxAlumnos];
	}

	// dar de alta un alumno
	public void altaAlumno(String codAlumno, String nombre, String apellidos, String codAula, double nota) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] == null) {
				alumnos[i] = new Alumno(codAlumno);
				alumnos[i].setNombre(nombre);
				alumnos[i].setApellidos(apellidos);
				alumnos[i].setCodAula(codAula);
				alumnos[i].setNota(nota);
				alumnos[i].setFechaMatricula(LocalDate.now());
				break;
			}
		}
	}

	// buscar un Alumno
	public Alumno buscarAlumno(String codAlumno) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAlumno().equals(codAlumno)) {
					return alumnos[i];
				}
			}
		}
		return null;
	}

	// eliminar un Alumno
	public void eliminarAlumno(String codAlumno) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAlumno().equals(codAlumno)) {
					alumnos[i] = null;
				}
			}
		}
	}

	// mostrar alumnos
	public void listarAlumnos() {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				System.out.println(alumnos[i]);
			}
		}
	}

	// cambiar nombre alumno
	public void cambiarNombreAlumno(String codAlumno, String nombre2) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAlumno().equals(codAlumno)) {
					alumnos[i].setNombre(nombre2);
				}
			}
		}
	}

	// mostrar listado por aula
	public void listarAlumnosPorAula(String codAula) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAula().equals(codAula)) {
					System.out.println(alumnos[i]);
				}
			}
		}
	}

	// mostrar alumnos aprobados
	public void contarAlumnosAprobados() {
		int total = 0;
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getNota() >= 5) {
					total = total + 1;
				}
			}
		}
		System.out.println("Total aprobados " + total);
	}

	// mostrar el maximo, minimo, media
	public void notasAlumnos() {
		double total = 0.0;
		double media = 0.0;
		double max = alumnos[0].getNota();
		double min = alumnos[0].getNota();
		for (int i = 0; i < alumnos.length; i++) {
			System.out.println(alumnos[i]);
			if (alumnos[i] != null) {
				if (alumnos[i].getNota() < min) {
					min = alumnos[i].getNota();
				}
				if (alumnos[i].getNota() > max) {
					max = alumnos[i].getNota();
				}
				total = total + alumnos[i].getNota();
			}
		}
		media = total / alumnos.length;
		System.out.println("Media " + media);
		System.out.println("Maximo " + max);
		System.out.println("Minimo " + min);
	}

}
