package programa;


import clases.Aula;

public class Programa {

	public static void main(String[] args) {
		System.out.println("1.- Crear instancia de Aula con 4 alumnos");
		int maxAlumnos=4;
		Aula miAula=new Aula(maxAlumnos);
		System.out.println("Instancia creada");
		
		System.out.println("2.- Dar de alta 4 alumnos distintos");
		miAula.altaAlumno("Alumno1", "Pepe", "L�pez", "Aula1", 3.44);
		miAula.altaAlumno("Alumno2", "Raquel", "Garcia", "Aula1", 5.6);
		miAula.altaAlumno("Alumno3", "Fermin", "Torres", "Aula1", 6.7);
		miAula.altaAlumno("Alumno4", "Diego", "Garcia", "Aula2", 2.8);
		
		System.out.println("3.- Listar alumnos");
		miAula.listarAlumnos();
		
		System.out.println("4.- Buscar un alumno");
		System.out.println(miAula.buscarAlumno("Alumno2"));
		
		System.out.println("5.- Eliminamos un alumno diferente al anterior");
		System.out.println("Elimino Alumno3");
		miAula.eliminarAlumno("Alumno3");
		miAula.listarAlumnos();
		
		System.out.println("6.- Almacenamos un nuevo alumno");
		miAula.altaAlumno("Alumno5", "Andrea", "Rodriguez", "Aula2", 5.5);
		miAula.listarAlumnos();
		
		System.out.println("7.- Modificar nombre de un alumno");
		System.out.println("Modificamos Alumno1 de Raquel a Rocio");
		miAula.cambiarNombreAlumno("Alumno1", "Rocio");
		miAula.listarAlumnos();
		
		System.out.println("8.- Listar alumnos de un aula (Aula2)");
		miAula.listarAlumnosPorAula("Aula2");
		
		System.out.println("9.- Contar alumnos aprobados");
		miAula.contarAlumnosAprobados();
		
		System.out.println("10.- Mostrar datos alumnos");
		miAula.notasAlumnos();
	}

}
