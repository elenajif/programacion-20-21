package ejercicioprevio08;

public class Principal {

	public static void main(String[] args) {
		System.out.println("Creamos un viaje");
		//uso constructor para crear un viaje
		Viaje miViaje = new Viaje("Hawai","Espa�a",3000);
		//uso metodos getter para mostrar los datos
		System.out.println(miViaje.getDestino());
		System.out.println(miViaje.getPaisOrigen());
		System.out.println(miViaje.getPrecio());
		//uso metodos setter para cambiar los datos
		miViaje.setDestino("Australia");
		miViaje.setPaisOrigen("Hawai");
		miViaje.setPrecio(3500);
		//uso metodos getter para mostrar los datos
		System.out.println(miViaje.getDestino());
		System.out.println(miViaje.getPaisOrigen());
		System.out.println(miViaje.getPrecio());

	}

}
