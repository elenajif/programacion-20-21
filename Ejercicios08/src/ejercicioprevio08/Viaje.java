package ejercicioprevio08;

public class Viaje {
	
	//atributos
	String destino;
	String paisOrigen;
	double precio;
	
	//constructor
	public Viaje(String destino, String paisOrigen, double precio) {
		this.destino = destino;
		this.paisOrigen = paisOrigen;
		this.precio = precio;
	}

	//metodos setter y getter
	//por cada atributo genera un setNombreAtributo y un getNombreAtributo
	//los get sirven para mostrar valores de los atributos
	//los set sirven para cambiar valoers de los atributos
	
	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
	
}
