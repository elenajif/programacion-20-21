package ejercicioprevio17;

public class PrincipalCoche {

	public static void main(String[] args) {
		//crear un coche
		Coche miCoche = new Coche();
		
		//mostrar sus datos
		System.out.println(miCoche.toString());
		
		//cambiar con setter los datos
		miCoche.setMatricula("ZZ-2222");
		miCoche.setModelo("Zafira");
		miCoche.setColor("azul");
		
		//volver a mostrarlos
		System.out.println(miCoche.toString());

	}

}
