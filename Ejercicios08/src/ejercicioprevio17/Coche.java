package ejercicioprevio17;

public class Coche {
	//atributos matricula, modelo, color
	private String matricula;
	private String modelo;
	private String color;
	
	//constructor sin parámetros con valores por defecto
	public Coche() {
		this.matricula = "XX-1234";
		this.modelo = "Corsa";
		this.color = "rojo";
	}
	
	//metodos setter y getter
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	//metodo toString
	@Override
	public String toString() {
		return "Datos del Coche (matricula=" + matricula +
				", modelo=" + modelo + 
				", color=" + color + ")";
	}
	

	
}
