package ejercicioprevio15;

public class Coche {
	//atributos matricula, modelo, color
	String matricula;
	String modelo;
	String color;
	
	//constructor sin parámetros con valores por defecto
	public Coche() {
		this.matricula = "XX-1234";
		this.modelo = "Corsa";
		this.color = "rojo";
	}
	
	//metodos setter y getter
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	

	
}
