package ejercicioprevio13;

public class InfoFrutas {
	
	public void imprimirInfo(Fruta fr) {
		System.out.println("Clase Fruta");
		System.out.println("Su nombre es "+fr.nombre);
		System.out.println("Sus caracteristicas son "+fr.caracteristicas);
	}
	
	public void imprimirInfo(Manzana mn) { 
		System.out.println("Clase Manzana");    
		System.out.println("Su nombre es "+mn.nombre); 
		System.out.println("Sus caracteristicas son "+mn.caracteristicas);
	}
	
	public void imprimirInfo(Naranja nr) {
		System.out.println("Clase Naranja");
		System.out.println("Su nombre es "+nr.nombre);
		System.out.println("Sus caracteristicas son "+nr.caracteristicas);
	}
	
	public static void main(String[] args) {
		//creo objetos
		Fruta oFruta = new Fruta();
		Manzana oManzana = new Manzana();
		Naranja oNaranja = new Naranja();
		//para poder usar imprimirInfo necesito crear un objeto de esta clase
		InfoFrutas inf = new InfoFrutas();
		//muestro datos
		inf.imprimirInfo(oFruta);
		inf.imprimirInfo(oManzana);
		inf.imprimirInfo(oNaranja);	

	}

}
