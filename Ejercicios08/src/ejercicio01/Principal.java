package ejercicio01;

import java.util.Scanner;

public class Principal {

	static Scanner in = new Scanner(System.in);
	
	public static void main(String[] args) {
		Profesor profe1 = new Profesor();
		Profesor profe2 = new Profesor();
		Profesor profe3 = new Profesor();
		Profesor profe4 = new Profesor();
		rellenarProfesor(profe1);
		rellenarProfesor(profe2);
		rellenarProfesor(profe3);
		rellenarProfesor(profe4);
		System.out.println("Profe1");
		mostrarProfesor(profe1);
		System.out.println("Profe2");
		mostrarProfesor(profe2);
		System.out.println("Profe3");
		mostrarProfesor(profe3);
		System.out.println("Profe4");
		mostrarProfesor(profe4);
		in.close();
	}
	
	public static void rellenarProfesor(Profesor obProfesor) {
		System.out.println("Dime el nombre");
		String nombre=in.nextLine();
		System.out.println("Dime los apellidos");
		String apellidos=in.nextLine();
		System.out.println("Dime el ciclo que imparte");
		String ciclo=in.nextLine();
		
		obProfesor.setNombre(nombre);
		obProfesor.setApellidos(apellidos);
		obProfesor.setCiclo(ciclo);
	}
	
	public static void mostrarProfesor(Profesor obProfesor) {
		System.out.println("Su nombre es "+obProfesor.getNombre());
		System.out.println("Sus apellidos son "+obProfesor.getApellidos());
		System.out.println("El ciclo que imparte es "+obProfesor.getCiclo());
	}

}
