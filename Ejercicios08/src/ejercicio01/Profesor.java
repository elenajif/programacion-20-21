package ejercicio01;

public class Profesor {

	// atributos
	private String nombre;
	private String apellidos;
	private String ciclo;

	// constructores
	public Profesor() {
		this.nombre = "";
		this.apellidos = "";
		this.ciclo = "";
	}

	public Profesor(String nombre, String apellidos, String ciclo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.ciclo = ciclo;
	}

	// setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

}
