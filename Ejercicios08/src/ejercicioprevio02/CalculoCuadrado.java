package ejercicioprevio02;

public class CalculoCuadrado {

	public static void main(String[] args) {
		//cuando un m�todo es static
		//para llamarlo Clase.metodo
		//Math.sqrt(2)
		
		//si mis metodos fueran static
		//para llamarlo 
		//Mensaje.mostrarMensaje()
		//int x=Cuadrado.calculandoCuadrado(5)
		
		//si mis metodos no son static
		//necesito crear un objeto de esa clase
		//Clase nombreObjeto = new Constructor
		//Un constructor es un m�todo "especial" para crear objetos
		//si no est� programado, el que tiene que el mismo nombre que la clase es mi constructor
		//En la clase Mensaje no hay nada programado -> constructor por defecto ->  Mensaje()
		//En la clase Cuadrado no hay nada programado -> constructor por defecto -> Cuadrado()
		//Mensaje miMensaje = new Mensaje()
		//Cuadrado miCuadrado = new Cuadrado()
		
		System.out.println("Creo un objeto de la clase Mensaje");
		Mensaje miMensaje = new Mensaje();
		System.out.println("LLamo al metodo mostrarMensaje");
		miMensaje.mostrarMensaje();
		
		int numero=5;
		System.out.println("Creo un objeto de la clase Cuadrado");
		Cuadrado miCuadrado = new Cuadrado();
		System.out.println("LLamo al metodo calculandoCuadrado");
		System.out.println(miCuadrado.calculandoCuadrado(numero));

	}

}
