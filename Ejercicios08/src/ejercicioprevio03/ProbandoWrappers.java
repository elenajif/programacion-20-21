package ejercicioprevio03;

public class ProbandoWrappers {

	public static void main(String[] args) {
		System.out.println("Voy a crear un objeto de la clase Integer");
		//clase objeto = new constructor
		Integer miInteger1 = new Integer(5);

		//para llamar a metodos
		//objeto.metodo
		System.out.println("Muestro el valor del entero");
		System.out.println(miInteger1.intValue());
		
		System.out.println("Voy a crear otro objeto de la clase Integer");
		Integer miInteger2 = new Integer("34");
		System.out.println("Muestro el valor del entero");
		System.out.println(miInteger2.intValue());
		
		System.out.println("Voy a crear un objeto de la clase Double");
		Double miDouble1 = new Double(2.3);

		System.out.println("Muestro el valor del double");
		System.out.println(miDouble1.doubleValue());
		
		System.out.println("Voy a crear otro objeto de la clase Double");
		Double miDouble2 = new Double("2.55");
		
		System.out.println("Muestro el valor del double");
		System.out.println(miDouble2.doubleValue());
	}

}
