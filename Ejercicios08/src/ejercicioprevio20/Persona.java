package ejercicioprevio20;

public class Persona {
	String nombre;
	int edad;
	double altura;
	double peso;
	
	//constructores
	public Persona() {
		
	}
	
	public Persona(String nombre, int edad) {
		this.nombre=nombre;
		this.edad=edad;
	}
	
	public Persona(String nombre, int edad, double altura) {
		this(nombre, edad);
		this.altura=altura;
	}
	
	public Persona(String nombre, int edad, double altura, double peso) {
		this(nombre,edad, altura);
		this.peso=peso;
	}
	
	
	//setter y getter
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	//toString
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", altura=" + altura + ", peso=" + peso + "]";
	}
	
	

}
