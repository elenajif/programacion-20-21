package ejercicioprevio20;

public class Youtuber extends Persona {
	String canal;
	int seguidores;

	// constructores
	public Youtuber(String nombre, int edad, double altura, double peso, String canal, int seguidores) {
		super(nombre, edad, altura, peso);
		this.canal = canal;
		this.seguidores = seguidores;
	}

	// setter y getter
	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public int getSeguidores() {
		return seguidores;
	}

	public void setSeguidores(int seguidores) {
		this.seguidores = seguidores;
	}

	//toString
	@Override
	public String toString() {
		return "Youtuber [nombre=" + nombre + ", edad=" + edad
				+ ", altura=" + altura + ", peso=" + peso + " canal=" + canal + ", seguidores=" + seguidores +"]";
	}

}
