package ejercicioprevio20;

public class Principal {

	public static void main(String[] args) {
		//creamos personas
		System.out.println("Personas");
		Persona persona1 = new Persona();
		System.out.println(persona1.toString());
		Persona persona2 = new Persona("Pepe",22);
		System.out.println(persona2.toString());
		Persona persona3 = new Persona("Carlos", 45,1.75);
		System.out.println(persona3.toString());
		Persona persona4 = new Persona("Andrea",34,1.70,53);
		System.out.println(persona4.toString());
		//creamos youtuber
		System.out.println("Youtuber");
		Youtuber youtuber1 = new Youtuber("Rubius",31,1.75,65,"El Rubius",25000);
		System.out.println(youtuber1.toString());
		
	}

}
