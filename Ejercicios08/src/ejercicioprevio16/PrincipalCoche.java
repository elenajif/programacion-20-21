package ejercicioprevio16;

public class PrincipalCoche {

	public static void main(String[] args) {
		//crear un coche
		Coche miCoche = new Coche();
		
		//mostrar sus datos
		System.out.println("Matricula "+miCoche.getMatricula());
		System.out.println("Modelo "+miCoche.getModelo());
		System.out.println("Color "+miCoche.getColor());
		
		//cambiar con setter los datos
		miCoche.setMatricula("ZZ-2222");
		miCoche.setModelo("Zafira");
		miCoche.setColor("azul");
		
		//volver a mostrarlos
		System.out.println("Matricula "+miCoche.getMatricula());
		System.out.println("Modelo "+miCoche.getModelo());
		System.out.println("Color "+miCoche.getColor());
		

	}

}
