package ejercicioprevio07;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Creo una persona usando constructor sin parametros");
		Persona humano1 = new Persona();
		System.out.println(humano1.nombre);
		System.out.println(humano1.apellidos);
		System.out.println(humano1.edad);
		System.out.println(humano1.telefono);
		System.out.println(humano1.peso);

		System.out.println("Creo una persona usando constructor con parametros");
		Persona humano2 = new Persona("Maria","Lopez",34,"976111111",52.1);
		System.out.println(humano2.nombre);
		System.out.println(humano2.apellidos);
		System.out.println(humano2.edad);
		System.out.println(humano2.telefono);
		System.out.println(humano2.peso);
		
		System.out.println("Creo una persona usando constructor con parametros con datos por teclado");
		System.out.println("Dame el nombre");
		String nombre=input.nextLine();
		System.out.println("Dame los apellidos");
		String apellidos=input.nextLine();
		System.out.println("Dame la edad");
		int edad = input.nextInt();
		input.nextLine();
		System.out.println("Dame el telefono");
		String telefono = input.nextLine();
		System.out.println("Dame el peso");
		double peso = input.nextDouble();
		Persona humano3 = new Persona(nombre,apellidos,edad,telefono,peso);
		System.out.println(humano3.nombre);
		System.out.println(humano3.apellidos);
		System.out.println(humano3.edad);
		System.out.println(humano3.telefono);
		System.out.println(humano3.peso);
		
		
		input.close();

	}

}
