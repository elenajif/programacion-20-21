package ejercicioprevio07;

public class Persona {

	String nombre;
	String apellidos;
	int edad;
	String telefono;
	double peso;
	
	public Persona() {
		this.nombre = "Pepe";
		this.apellidos = "Martinez";
		this.edad = 22;
		this.telefono = "976212121";
		this.peso = 69.5;
	}
	
	public Persona(String nombre, String apellidos, int edad, String telefono, double peso) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.telefono = telefono;
		this.peso = peso;
	}
	

}
