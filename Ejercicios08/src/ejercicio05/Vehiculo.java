package ejercicio05;

public class Vehiculo {
	private String modelo;
	private String marca;
	private int autonomia;
	private float kilometraje;

	public Vehiculo() {
		
	}

	public Vehiculo(String modelo, String marca, int autonomia, float kilometraje) {
		this.modelo = modelo;
		this.marca = marca;
		this.autonomia = autonomia;
		this.kilometraje = kilometraje;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getAutonomia() {
		return autonomia;
	}

	public void setAutonomia(int autonomia) {
		this.autonomia = autonomia;
	}

	public float getKilometraje() {
		return kilometraje;
	}

	public void setKilometraje(float kilometraje) {
		this.kilometraje = kilometraje;
	}
	
	boolean esSeguro(float kilometros,int autonomia) {
		if (this.kilometraje<this.autonomia)  {
			return true;
		}
		else {
			return false;
		}
	}
}
