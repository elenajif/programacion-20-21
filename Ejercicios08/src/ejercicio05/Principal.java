package ejercicio05;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce modelo");
		String modelo = input.nextLine();
		
		System.out.println("Introduce marca");
		String marca = input.nextLine();
		
		System.out.println("Introduce la autonom�a"); 
		int autonomia = input.nextInt();
		
		System.out.println("Introduce la kilometraje"); 
		float kilometraje = input.nextFloat();
		
		Vehiculo vehiculo1 = new Vehiculo(modelo, marca, autonomia, kilometraje);
		
		System.out.println("Modelo: " + vehiculo1.getModelo());
		System.out.println("Marca: " + vehiculo1.getMarca());
		System.out.println("Autonom�a: " + vehiculo1.getAutonomia());
		System.out.println("Kilometrake: " + vehiculo1.getKilometraje());
		System.out.println("�Es seguro?: " + vehiculo1.esSeguro(vehiculo1.getKilometraje(),vehiculo1.getAutonomia()));
		input.close();
	}

}
