package ejercicioprevio05;

public class MisSumas {
	// declarar dos enteros
	int numInt1;
	int numInt2;

	// declarar dos doubles
	double numDouble1;
	double numDouble2;

	// constructor de la clase
	public MisSumas() {
		numInt1 = 3;
		numInt2 = 4;
		numDouble1 = 3.25;
		numDouble2 = 4.52;
	}

	// sobrecarga (metodo que se llaman igual pero reciben parametros distintos)

	// crear un metodo que reciba dos enteros y devuelva un entero sumado
	public int suma(int x, int y) {
		return x + y;
	}

	// crear un metodo que reciba dos doubles y devuelva un double sumado
	public double suma(double x, double y) {
		return x + y;
	}
}
