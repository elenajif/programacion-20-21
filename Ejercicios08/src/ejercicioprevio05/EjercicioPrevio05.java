package ejercicioprevio05;

public class EjercicioPrevio05 {

	public static void main(String[] args) {
		//creo un objeto de la clase MisSumas
		//clase objeto = new cnstructor
		System.out.println("Creo un objeto de la clase MisSumas");
		MisSumas sumita = new MisSumas();
		System.out.println("Valores iniciales del constructor");
		System.out.println("numInt1 "+sumita.numInt1);
		System.out.println("numInt2 "+sumita.numInt2);
		System.out.println("numDouble1 "+sumita.numDouble1);
		System.out.println("numDouble2 "+sumita.numDouble2);
		//para llamar a un metodo
		System.out.println("Uso el metodo suma con dos enteros");
		System.out.println(sumita.suma(5, 8));
		System.out.println("Uso el medodo suma con dos doubles");
		System.out.println(sumita.suma(6.6, 8.8));

	}

}
