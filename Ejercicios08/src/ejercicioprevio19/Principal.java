package ejercicioprevio19;

public class Principal {

	public static void main(String[] args) {
		System.out.println("Vehiculo");
		Vehiculo miVehiculo = new Vehiculo();
		//ruedas=0 velocidad=100.3 acelerar=100.3+15 frenar=115.3-10
		System.out.println("Ruedas "+miVehiculo.ruedas);
		System.out.println("Velocidad "+miVehiculo.velocidad);
		System.out.println("Acelerar "+miVehiculo.acelerar(15));
		System.out.println("Frenar "+miVehiculo.frenar(10));
		System.out.println("Coche");
		Coche miCoche = new Coche();
		//ruedas=4 velocidad=100.3 gasolina=400 acelerar=100.3+25 frenar=125.3-45 repostar=400+100
		//ruedas es un atributo sobreescrito
		System.out.println("Ruedas "+miCoche.ruedas);
		System.out.println("Velocidad "+miCoche.velocidad);
		System.out.println("Gasolina "+miCoche.gasolina);
		System.out.println("Acelerar "+miCoche.acelerar(25));
		System.out.println("Frenar "+miCoche.frenar(45));
		System.out.println("Repostar "+miCoche.repostar(100));
		
	}

}
