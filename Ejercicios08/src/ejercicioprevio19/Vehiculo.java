package ejercicioprevio19;

public class Vehiculo {
	int ruedas;
	double velocidad;
	
	public Vehiculo() {
		this.ruedas = 0;
		this.velocidad = 100.3;
	}
	
	public double acelerar(int km) {
		velocidad+=km;
		return velocidad;
	}
	
	public double frenar (int km) {
		velocidad-=km;
		return velocidad;
	}
	

}
