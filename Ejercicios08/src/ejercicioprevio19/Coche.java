package ejercicioprevio19;

public class Coche extends Vehiculo {
	int gasolina;

	public Coche() {
		this.ruedas=4;
		this.gasolina = 400;
	}
	
	public int repostar (int litros) {
		gasolina+=litros;
		return gasolina;
	}
	
	

}
