package ejercicioprevio18;

public class Persona {
	//atributos nombre, edad
	private String nombre;
	private int edad;
	
	//constructor sin parametros
	public Persona() {
		this.nombre = "Maria";
		this.edad = 24;
	}
	
	//constructor con parametros
	public Persona(String nombre, int edad) {
		this.nombre = nombre;
		this.edad = edad;
	}

	//setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	//metodo toString
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + "]";
	}


}
