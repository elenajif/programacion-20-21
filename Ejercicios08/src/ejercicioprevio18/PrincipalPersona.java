package ejercicioprevio18;

public class PrincipalPersona {

	public static void main(String[] args) {
		//crear un persona usando constructor sin parametros
		Persona persona1 = new Persona();
		//mostrar datos con toString
		System.out.println(persona1.toString());
		
	    //crear una persona usando constructor con parametros
		Persona persona2 = new Persona("David",15);
		//mostrar datos con toString
		System.out.println(persona2.toString());
		
		//cambiar datos con setter
		persona2.setEdad(16);
		//volver a mostrarlos
		System.out.println(persona2.toString());

	}

}
