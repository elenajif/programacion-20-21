package ejercicioprevio09;

public class NaveEspacial {

	public static void main(String[] args) {
		System.out.println("Llenamos la nave espacial");
		System.out.println("Creamos un alien por defecto");
		System.out.println("Usamos constructor sin parámetros");
		//clase objeto = new constructor
		Alienigena alien1 = new Alienigena();
		System.out.println("Mostramos sus datos usando directamente el atributo");
		System.out.println("nombre " +alien1.nombre);
		System.out.println("numero ojos "+alien1.numOjos);
		System.out.println("estatura "+alien1.estatura);
		System.out.println("color "+alien1.color);
		System.out.println("piernas "+alien1.piernas);
		System.out.println("Mostramos sus datos usando metodo getter");
		System.out.println("nombre " +alien1.getNombre());
		System.out.println("numero ojos "+alien1.getNumOjos());
		System.out.println("estatura "+alien1.getEstatura());
		System.out.println("color "+alien1.getColor());
		System.out.println("piernas "+alien1.getPiernas());

		System.out.println("Creamos un alien con nuestros datos usando directamente el atributo");
		System.out.println("Usamos constructor con parámetros");
		//clase objeto = new constructor
		Alienigena alien2 = new Alienigena("alien 2","azul",3,2.52,3);
		System.out.println("Mostramos sus datos");
		System.out.println("nombre " +alien2.nombre);
		System.out.println("numero ojos "+alien2.numOjos);
		System.out.println("estatura "+alien2.estatura);
		System.out.println("color "+alien2.color);
		System.out.println("piernas "+alien2.piernas);
		System.out.println("Mostramos sus datos usando metodo getter");
		System.out.println("nombre " +alien2.getNombre());
		System.out.println("numero ojos "+alien2.getNumOjos());
		System.out.println("estatura "+alien2.getEstatura());
		System.out.println("color "+alien2.getColor());
		System.out.println("piernas "+alien2.getPiernas());

		System.out.println("Creamos un alien privado por defecto");
		System.out.println("Usamos constructor sin parámetros");
		AlienigenaPrivado alien3 = new AlienigenaPrivado();
		System.out.println("no puedo usar directamente el atributo");
		System.out.println("Mostramos sus datos usando metodo getter");
		System.out.println("nombre " +alien3.getNombre());
		System.out.println("numero ojos "+alien3.getNumOjos());
		System.out.println("estatura "+alien3.getEstatura());
		System.out.println("color "+alien3.getColor());
		System.out.println("piernas "+alien3.getPiernas());

		System.out.println("Creamos un alien privado con nuestros datos usando directamente el atributo");
		System.out.println("Usamos constructor con parámetros");
		AlienigenaPrivado alien4 = new AlienigenaPrivado("alien 4","amarillo",1,1.52,1);
		System.out.println("no puedo usar directamente el atributo");
		System.out.println("Mostramos sus datos usando metodo getter");
		System.out.println("nombre " +alien4.getNombre());
		System.out.println("numero ojos "+alien4.getNumOjos());
		System.out.println("estatura "+alien4.getEstatura());
		System.out.println("color "+alien4.getColor());
		System.out.println("piernas "+alien4.getPiernas());
		
		System.out.println("Cambiamos los datos de un alien");
		alien4.setNombre("alien cambiado");
		alien4.setColor("rojo");
		System.out.println("Mostramos sus datos usando metodo getter");
		System.out.println("nombre " +alien4.getNombre());
		System.out.println("numero ojos "+alien4.getNumOjos());
		System.out.println("estatura "+alien4.getEstatura());
		System.out.println("color "+alien4.getColor());
		System.out.println("piernas "+alien4.getPiernas());


	}

}
