package ejercicioprevio14;

public class InfoFrutas {
	
	public static void imprimirInfo(Fruta fr) {
		System.out.println("Clase Fruta");
		System.out.println("Su nombre es "+fr.nombre);
		System.out.println("Sus caracteristicas son "+fr.caracteristicas);
	}
	
	public static void imprimirInfo(Manzana mn) { 
		System.out.println("Clase Manzana");    
		System.out.println("Su nombre es "+mn.nombre); 
		System.out.println("Sus caracteristicas son "+mn.caracteristicas);
	}
	
	public static void imprimirInfo(Naranja nr) {
		System.out.println("Clase Naranja");
		System.out.println("Su nombre es "+nr.nombre);
		System.out.println("Sus caracteristicas son "+nr.caracteristicas);
	}
	
	public static void main(String[] args) {
		//creo objetos
		Fruta oFruta = new Fruta();
		Manzana oManzana = new Manzana();
		Naranja oNaranja = new Naranja();
		//muestro datos
		InfoFrutas.imprimirInfo(oFruta);
		InfoFrutas.imprimirInfo(oManzana);
		InfoFrutas.imprimirInfo(oNaranja);	

	}

}
