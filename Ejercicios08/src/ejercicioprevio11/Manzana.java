package ejercicioprevio11;

public class Manzana extends Fruta {

	String nombre;
	String caracteristicas;
	
	public Manzana() {
		this.nombre = "Manzana";
		this.caracteristicas = "Piel suave y comestible";
	}
	
	public void imprimirInfo(Manzana mn) { 
		System.out.println("Clase Manzana");    
		System.out.println("Su nombre es "+mn.nombre); 
		System.out.println("Sus caracteristicas son "+mn.caracteristicas);
	}
}
