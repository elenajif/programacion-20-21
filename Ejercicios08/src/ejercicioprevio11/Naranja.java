package ejercicioprevio11;

public class Naranja extends Fruta{

	String nombre;
	String caracteristicas;
	
	public Naranja() {
		this.nombre = "Naranja";
		this.caracteristicas = "Piel rugosa y no comestible";
	}
	
	public void imprimirInfo(Naranja nr) {
		System.out.println("Clase Naranja");
		System.out.println("Su nombre es "+nr.nombre);
		System.out.println("Sus caracteristicas son "+nr.caracteristicas);
	}
}
