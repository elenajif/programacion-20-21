package ejercicioprevio11;

public class Fruta {
	String nombre;
	String caracteristicas;
	
	public Fruta() {
		this.nombre = "Fruta genérica";
		this.caracteristicas = "Características genéricas";
	}
	
	public void imprimirInfo(Fruta fr) {
		System.out.println("Clase Fruta");
		System.out.println("Su nombre es "+fr.nombre);
		System.out.println("Sus caracteristicas son "+fr.caracteristicas);
	}
	
	

}
