package ejercicioprevio11;

public class InfoFrutas {

	public static void main(String[] args) {
		System.out.println("Voy a crear un fruta");
		Fruta oFruta = new Fruta();
		System.out.println("Muestro sus datos");
		oFruta.imprimirInfo(oFruta);
		System.out.println("Voy a crear una manzana");
		Manzana oManzana = new Manzana();
		System.out.println("Muestro sus datos");
		oManzana.imprimirInfo(oManzana);
		System.out.println("Voy a crear una naranja");
		Naranja oNaranja = new Naranja();
		System.out.println("Muestro sus datos");
		oNaranja.imprimirInfo(oNaranja);

	}

}
