package ejercicio02;

public class Principal {

	public static void main(String[] args) {
		Alumno objAlumnos1 = new Alumno();
		objAlumnos1.rellenarAlumno();

		Alumno objAlumnos2 = new Alumno();
		objAlumnos2.rellenarAlumno();
		
		System.out.println("Datos del primer alumno");
		objAlumnos1.visualizarAlumno(objAlumnos1);

		System.out.println("El primer alumno promociona?");
		if (objAlumnos1.promociona(objAlumnos1.notaMedia)) {
			System.out.println("Si promociona");
		} else {
			System.out.println("No promociona");
		}

		System.out.println("Datos del segundo alumno");
		objAlumnos2.visualizarAlumno(objAlumnos2);

		System.out.println("El segundo alumno promociona?");
		if (objAlumnos2.promociona(objAlumnos2.notaMedia)) {
			System.out.println("Si promociona");
		} else {
			System.out.println("No promociona");
		}
		
		
	}

}
