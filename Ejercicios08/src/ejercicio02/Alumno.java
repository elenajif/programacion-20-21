package ejercicio02;

import java.util.Scanner;

public class Alumno {
	static Scanner in = new Scanner(System.in);
	
	//atributos
	String nombre;
	String apellidos;
	double notaMedia;

	//constructores
	public Alumno() {
		this.nombre = "";
		this.apellidos = "";
		this.notaMedia = 0.0;
	}

	public Alumno(String nombre, String apellidos, double notaMedia) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.notaMedia = notaMedia;
	}

	//setter y getter
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public double getNotaMedia() {
		return notaMedia;
	}
	public void setNotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}
	
	
	//metodo promociona
	public boolean promociona(double notaMedia) {
		if (notaMedia>=5) {
			return true;
		}
		return false;
	}
	
	//metodo rellenarAlumno
	public void rellenarAlumno() {
		System.out.println("Dime el nombre");
		nombre=in.nextLine();
		System.out.println("Dime los apellidos");
		apellidos=in.nextLine();
		System.out.println("Dame la nota media");
		notaMedia=in.nextDouble();
		in.nextLine();
	}
	
	//metodo visualizarAlumno
	public void visualizarAlumno(Alumno alumno) {
		System.out.println("El nombre es " + nombre);
		System.out.println("El apellido es " + apellidos);
		System.out.println("La nota media es " + notaMedia);
	}
	/*
	public void visualizarAlumno() {
		System.out.println("Su nombre es "+this.getNombre());
		System.out.println("Sus apellidos son "+this.getApellidos());
		System.out.println("El ciclo que imparte es "+this.getNotaMedia());
	}*/

}
