package ejercicioprevio10;

public class Naranja extends Fruta {
	String nombre;
	String caracteristicas;
	
	public Naranja() {
		this.nombre = "Naranja";
		this.caracteristicas = "Redonda, piel rugosa no comestible";
	}
	
	public Naranja(String nombre, String caracteristicas) {
		this.nombre = nombre;
		this.caracteristicas = caracteristicas;
	}
	

}
