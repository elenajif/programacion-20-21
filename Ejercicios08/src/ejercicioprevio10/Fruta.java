package ejercicioprevio10;

public class Fruta {
	String nombre;
	String caracteristicas;

	public Fruta() {
		this.nombre = "Fruta generica";
		this.caracteristicas = "caracteristicas genericas";
	}
	
	public Fruta(String nombre, String caracteristicas) {
		this.nombre = nombre;
		this.caracteristicas = caracteristicas;
	}
	

}
