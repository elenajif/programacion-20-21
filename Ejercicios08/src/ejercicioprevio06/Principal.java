package ejercicioprevio06;

public class Principal {

	public static void main(String[] args) {
		System.out.println("Creo un ordenador");
		Ordenador pc = new Ordenador();
		System.out.println(pc.tamMemoria);
		System.out.println(pc.tipoMemoria);
		System.out.println(pc.tamDiscoDuro);
		System.out.println(pc.modeloProcesador);
		System.out.println("Cambio tama�o memoria");
		pc.cambiarTamMemoria(222);
		System.out.println("Cambio tipo memoria");
		pc.cambiarTipoMemoria("nuevo tipo");
		System.out.println("Cambio tama�o disco duro");
		pc.cambiarTamDiscoDuro(333333);
		System.out.println("Cambio modelo procesador");
		pc.cambiarModeloProcesador("nuevo modelo");

	}

}
