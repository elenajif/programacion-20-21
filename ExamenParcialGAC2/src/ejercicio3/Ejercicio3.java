package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		String nombre;
		boolean aceptar;
		String nombrePrograma = "juan";
		aceptar = true;

		System.out.println("Introduce un usuario");
		nombre = entrada.nextLine();
		System.out.println("Aceptas las condiciones");
		aceptar = entrada.nextBoolean();

		if (nombre.equals(nombrePrograma) && aceptar == true) {
			System.out.println("Condiciones aceptadas, se�or " + nombre);
		} else {
			System.out.println("Las condiciones tienen que ser aceptadas");
		}

		entrada.close();

	}
}
