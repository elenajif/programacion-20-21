package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		System.out.print("Dame tu edad:");
		int edad = entrada.nextInt();
		if (edad >= 18 && edad<=85) {
			System.out.print("Tu edad es edad: "+ edad);
			System.out.println(" Puedes conducir");
		} else {
			System.out.print("Tu edad es edad: "+ edad);
			System.out.println(" No puedes conducir");
		}	
		
		entrada.nextLine();
		System.out.print("Has pasado el reconocimiento: (responder SI/NO)");
		String reconocimiento=entrada.nextLine();
		
		if (reconocimiento.equals("SI")){
			System.out.print("Eres apto para conducir");
		} else {
			System.out.print("Debes pasar el reconocimiento");
		}		
		

		entrada.close();
	}
}
