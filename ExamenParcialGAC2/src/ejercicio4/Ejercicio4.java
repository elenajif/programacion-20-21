package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("1.- Comprobar un numero");
		System.out.println("2.- Comprobar un caracter");
		System.out.println("3.- Salir");

		int opcion = input.nextInt();

		if (opcion >= 1 && opcion <= 3) {
			switch (opcion) {
			case 1:
				System.out.println("Introduce un n�mero entero");
				int entero = input.nextInt();
				if (entero < 0) {
					System.out.println("El numero es negativo");
				} else if (entero % 10 == 0) {
					System.out.println("Es m�ltiplo de 10");
				} else {
					System.out.println("No es m�ltiplo de 10");
				}
				break;
			case 2:
				input.nextLine();
				System.out.println("Introduce una cadena");
				String cadena = input.nextLine();
				if (cadena.length() != 1) {
					System.out.println("Has introducido m�s de un caracter");
				} else {
					int caracterAscii = (int) cadena.charAt(0);

					if (caracterAscii >= 48 && caracterAscii <= 57) {
						System.out.println("Es una cifra");
					} else if (caracterAscii >= 65 && caracterAscii <= 90
							|| caracterAscii >= 97 && caracterAscii <= 122) {
						System.out.println("Es una letra");
					} else {
						System.out.println("Es un simbolo");
					}
				}
			}
		} else {
			System.out.println("La opci�n elegida no es correcta, programa terminado");
		}

		input.close();

	}

}
