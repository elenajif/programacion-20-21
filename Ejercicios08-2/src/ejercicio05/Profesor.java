package ejercicio05;

public class Profesor {
	String nombre;
	String apellidos;
	int edad;
	String ciclo;

	public Profesor() {

	}

	public Profesor(String nombre, String apellidos, int edad, String ciclo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.ciclo = ciclo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	public String toString() {
		return "Nombre y apellidos " + nombre + " " + apellidos 
				+ "\nEdad " + edad 
				+ "\nCiclo "+ ciclo;
	}

	public void mostrarDatosProfesor(Profesor profesor) {
		System.out.println("Los datos del profesor son: ");
		System.out.println(profesor.toString());
	}

}
