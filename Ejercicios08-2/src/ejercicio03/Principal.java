package ejercicio03;

public class Principal {

	public static void main(String[] Args) {
		Profesor profesor1 = new Profesor("Juan", "Hern�ndez Garc�a", 33,"DAM");
		Profesor profesor2 = new Profesor("Jos�", "Herrando L�pez", 27,"DAW");
		Profesor profesor3 = new Profesor("Carlota", "Garc�a Garc�a", 28,"DAM");
		Profesor profesor4 = new Profesor("Raquel", "Su�rez Rodr�guez", 25,"DAM");
		
		System.out.println("Profesor 1");
		profesor1.mostrarDatosProfesor(profesor1);
		System.out.println("Profesor 2");
		profesor1.mostrarDatosProfesor(profesor2);
		System.out.println("Profesor 3");
		profesor1.mostrarDatosProfesor(profesor3);
		System.out.println("Profesor 4");
		profesor1.mostrarDatosProfesor(profesor4);
	}

}
