package programa;

import clases.Videoclub;

public class Programa {

	public static void main(String[] args) {

		Videoclub video1 = new Videoclub();

		System.out.println("Alta clientes, articulos y creamos alquileres");
		video1.altaCliente("Jesus");
		video1.altaCliente("Andres");
		video1.altaArticulo("1234", "Pelicula", 104);
		video1.altaArticulo("5678", "VideoJuego", "PC", true);
		video1.crearAlquilerCliente(1);
		video1.crearAlquilerCliente(2);
		video1.introducirArticuloAlquiler(1, "1234");
		video1.introducirArticuloAlquiler(2, "5678");
		
		System.out.println();
		System.out.println("Guardando datos....");
		video1.guardarDatos();
		System.out.println("Cargando datos....");
		video1.cargarDatos();

		System.out.println();
		System.out.println("Mostramos alquileres clientes");
		video1.mostrarAlquileresCliente(1);
		System.out.println("Listamos alquileres");
		video1.listarAlquileres();
		
	}

}
