package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Videoclub implements Serializable {

	private static final long serialVersionUID = -1429504306914757180L;
	public static final int DIAS_PRESTAMO = 15;

	private ArrayList<Articulos> articulos;
	private ArrayList<Clientes> clientes;
	private ArrayList<Alquileres> alquileres;

	public Videoclub() {
		articulos = new ArrayList<Articulos>();
		clientes = new ArrayList<Clientes>();
		alquileres = new ArrayList<Alquileres>();
	}

	// DAR ALTA CLIENTE
	public void altaCliente(String nombre) {
		clientes.add(new Clientes(clientes.size() + 1, nombre, LocalDate.now()));
	}

	// DAR ALTA ARTICULO PELICULA
	public void altaArticulo(String ISBN, String titulo, float duracion) {
		articulos.add(new Peliculas(ISBN, titulo, duracion));
		Collections.sort(articulos);
	}

	// DAR ALTA ARTICULO VIDEOJUEGO
	public void altaArticulo(String ISBN, String titulo, String plataforma, boolean online) {
		articulos.add(new Videojuegos(ISBN, titulo, plataforma, online));
		Collections.sort(articulos);
	}

	// CREAR ALQUILER PARA UN CLIENTE
	public void crearAlquilerCliente(int idCliente) {
		if (clienteExiste(idCliente)) {
			alquileres.add(new Alquileres(alquileres.size() + 1, LocalDate.now(), 
					LocalDate.now().plusDays(DIAS_PRESTAMO),devuelveCliente(idCliente)));
		} else {
			System.out.println("El cliente no existe");
		}
	}

	// INTRODUCIR ARTICULOS EN UN ALQUILER
	public void introducirArticuloAlquiler(int idAlquiler, String isbn) {
		if (alquilerExiste(idAlquiler)) {
			if (productoExiste(isbn) && !devuelveAlquiler(idAlquiler).comprobarArticulo(isbn)) {
				devuelveAlquiler(idAlquiler).listaArticulos.add(devuelveProducto(isbn));
			} else {
				System.out.println("El producto no existe");
			}
		} else {
			System.out.println("El alquiler no existe");
		}
	}

	// MOSTRAR ALQUILERES DE UN CLIENTE CONCRETO
	public void mostrarAlquileresCliente(int idCliente) {
		for (Alquileres alquileres2 : alquileres) {
			if (alquileres2.getCliente().equals(devuelveCliente(idCliente))) {
				System.out.println(alquileres2);
			}
		}
	}

	// LISTAR ALQUILERES
	public void listarAlquileres() {
		for (Alquileres alquileres2 : alquileres) {
			System.out.println(alquileres2);
		}
		System.out.println("Articulos:");
		for (Articulos articulo : articulos) {
			System.out.println(articulo);
		}
	}

	// GUARDAR DATOS
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(alquileres);
			escritor.writeObject(articulos);
			escritor.writeObject(clientes);
			escritor.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// CARGAR DATOS
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream(new File("src/datos.dat")));
			alquileres = (ArrayList<Alquileres>) escritor.readObject();
			articulos = (ArrayList<Articulos>) escritor.readObject();
			clientes = (ArrayList<Clientes>) escritor.readObject();
			escritor.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// COMPROBAR SI CLIENTE EXISTE
	public boolean clienteExiste(int idCliente) {
		for (Clientes cliente1 : clientes) {
			if (cliente1.getIdCliente() == idCliente) {
				return true;
			}
		}
		return false;
	}

	// DEVUELVE CLIENTE
	public Clientes devuelveCliente(int idCliente) {
		for (Clientes cliente1 : clientes) {
			if (cliente1.getIdCliente() == idCliente) {
				return cliente1;
			}
		}
		return null;
	}

	// COMPROBAR SI PRODUCTO EXISTE
	public boolean productoExiste(String isbn) {
		for (Articulos articulo : articulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return true;
			}
		}
		return false;
	}

	// COMPROBAR SI ALQUILER EXISTE
	public boolean alquilerExiste(int idAlquiler) {
		for (Alquileres alquiler : alquileres) {
			if (alquiler.getIdAlquiler() == idAlquiler) {
				return true;
			}
		}
		return false;
	}

	// DEVUELVE ALQUILER
	public Alquileres devuelveAlquiler(int idAlquiler) {
		for (Alquileres alquiler : alquileres) {
			if (alquiler.getIdAlquiler() == idAlquiler) {
				return alquiler;
			}
		}
		return null;
	}

	// DEVUELVE PRODUCTO
	public Articulos devuelveProducto(String isbn) {
		for (Articulos articulo : articulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return articulo;
			}
		}
		return null;
	}
}
