package clases;

import java.io.Serializable;

public class Peliculas extends Articulos implements Serializable {

	private static final long serialVersionUID = 8019482909214669952L;
	public final static double INCREMENTO_MINUTOS = 100;
	public final static double INCREMENTO_PRECIO = 0.10;

	private float duracion;

	public Peliculas(String isbn, String titulo, float duracion) {
		super(isbn, titulo);
		this.duracion = duracion;
	}

	public float getDuracion() {
		return duracion;
	}
	public void setDuracion(float duracion) {
		this.duracion = duracion;
	}

	@Override
	public float calcularPrecio() {
		float precioTotal = PRECIO_FIJO;
		if (duracion > INCREMENTO_MINUTOS) {
			precioTotal = (float) (precioTotal + (INCREMENTO_PRECIO * PRECIO_FIJO));
		}
		return precioTotal;

	}

	@Override
	public int compareTo(Articulos arg0) {
		return getIsbn().compareTo(arg0.getIsbn());
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "Duracion " + duracion;
	}

}
