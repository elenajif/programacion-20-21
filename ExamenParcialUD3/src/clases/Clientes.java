package clases;

import java.io.Serializable;
import java.time.LocalDate;

public class Clientes implements Serializable {

	private static final long serialVersionUID = -2224968161150362176L;

	private int idCliente;
	private String nombre;
	private LocalDate fechaAlta;

	public Clientes(int idCliente, String nombre, LocalDate fechaAlta) {
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setId_cliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFecha_Alta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return "IdCliente " + idCliente + "\n" + "Nombre " + nombre 
				+ "\n" + "FechaAlta " + fechaAlta;
	}

}
