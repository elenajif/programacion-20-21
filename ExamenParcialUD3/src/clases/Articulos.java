package clases;

import java.io.Serializable;

public abstract class Articulos implements Comparable<Articulos>, Serializable{
	
	private static final long serialVersionUID = -3675649796028274970L;
	
	protected static final int PRECIO_FIJO = 3;
	private String isbn;
	private String titulo;
	
	public abstract float calcularPrecio();
	
	public Articulos(String isbn, String titulo) {
		this.isbn = isbn;
		this.titulo = titulo;
	}
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	@Override
	public String toString() {
		return "ISBN " + isbn + "\n" + "Titulo " + titulo;
	}

	
}
