package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Alquileres implements Serializable {

	private static final long serialVersionUID = -6997039129930791416L;

	private int idAlquiler;
	private LocalDate fechaAlquiler;
	private LocalDate fechaDevolucion;
	private Clientes cliente;
	protected ArrayList<Articulos> listaArticulos;

	public Alquileres(int id_alquiler, LocalDate fechaAlquiler, LocalDate fechaDevolucion, Clientes cliente) {
		this.idAlquiler = id_alquiler;
		this.fechaAlquiler = fechaAlquiler;
		this.fechaDevolucion = fechaDevolucion;
		this.cliente = cliente;
		listaArticulos = new ArrayList<Articulos>();
	}

	public int getIdAlquiler() {
		return idAlquiler;
	}

	public void setIdAlquiler(int idAlquiler) {
		this.idAlquiler = idAlquiler;
	}

	public LocalDate getFechaAlquiler() {
		return fechaAlquiler;
	}

	public void setFechaAlquiler(LocalDate fechaAlquiler) {
		this.fechaAlquiler = fechaAlquiler;
	}

	public LocalDate getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(LocalDate fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public float calcularPrecioTotal() {
		float precioTotal = 0;

		for (Articulos articulos : listaArticulos) {
			precioTotal += articulos.calcularPrecio();
		}
		return precioTotal;
	}

	// COMPRUEBA QUE NO HAYA ARTICULOS CON ESE ISBN
	public boolean comprobarArticulo(String ISBN) {
		for (Articulos articulo : listaArticulos) {
			if (articulo.getIsbn().equals(ISBN)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "IdAlquiler " + idAlquiler + "\n" + "FechaAlquiler " + fechaAlquiler 
				+ "\n" + "FechaDevolucion "	+ fechaDevolucion 
				+ "\n" + "PrecioTotal " + calcularPrecioTotal() 
				+ "\n" + cliente + "\n\n";
	}

}
