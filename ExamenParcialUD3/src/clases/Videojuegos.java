package clases;

import java.io.Serializable;

public class Videojuegos extends Articulos implements Serializable {

	private static final long serialVersionUID = -4571311137859634594L;

	private String plataforma;
	private boolean online;

	public Videojuegos(String ISBN, String titulo, String plataforma, boolean online) {
		super(ISBN, titulo);
		this.plataforma = plataforma;
		this.online = online;
	}

	public String getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online) {
		this.online = online;
	}

	@Override
	public float calcularPrecio() {
		float precioTotal = PRECIO_FIJO;
		if (online) {
			precioTotal = (float) (precioTotal + (0.05 * PRECIO_FIJO));
		}
		return precioTotal;
	}

	@Override
	public int compareTo(Articulos arg0) {
		return getIsbn().compareTo(arg0.getIsbn());
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "Plataforma " + plataforma + "\n" + "Online " + online;
	}

}
