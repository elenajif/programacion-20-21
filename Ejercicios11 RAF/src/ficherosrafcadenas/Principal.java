package ficherosrafcadenas;

public class Principal {

	public static void main(String[] args) {
		ListaNombres unaLista = new ListaNombres("datoscadenas.txt");
		// Rellenar archivo
		System.out.println("Rellenar archivo");
		unaLista.rellenarArchivo();
		// Mostrar archivo
		System.out.println("Mostrar archivo");
		unaLista.visualizarArchivo();
		// Modificar archivo
		System.out.println("Modificar archivo");
		unaLista.modificarArchivo();
		// Mostrar archivo modificado
		System.out.println("Mostrar archivo modificado");
		unaLista.visualizarArchivo();
	}

}
