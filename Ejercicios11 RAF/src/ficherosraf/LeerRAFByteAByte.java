package ficherosraf;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class LeerRAFByteAByte {

	public static void main(String[] args) {
		try {
			// 1.- abrir el acceso al archivo
			RandomAccessFile f = new RandomAccessFile("datos1.txt", "rw");
			// 2.- recorrer el archivo caracter a caracter
			char letra;
			// final del fichero
			boolean finFichero = false;

			do {
				try {
					letra = (char) f.readByte();
					if (letra == 'b') {
						// 1.- desplazar el puntero hacia atr�s un byte
						f.seek(f.getFilePointer() - 1);
						// 2.- escribir la b
						f.writeByte('B');
					}
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					// 3.- cerrar el fichero
					f.close();
				}

			} while (finFichero == false);

		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
	}

}
