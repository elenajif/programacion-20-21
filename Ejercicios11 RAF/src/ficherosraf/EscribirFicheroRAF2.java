package ficherosraf;

import java.io.IOException;
import java.io.RandomAccessFile;

public class EscribirFicheroRAF2 {

	public static void main(String[] args) {
		try {
			// 1. Abrir el archivo en acceso RAF
			RandomAccessFile f = new RandomAccessFile("datos","rw");
			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());	
			// 3.- escribimos una cadena de texto
			f.writeBytes("Esto es un cadena de texto");
			// 4.- cerrar el fichero
			f.close();
			System.out.println("Fichero actualizado");
		} catch (IOException e) {
			System.out.println("Error de datos");
		} 
	}

}
