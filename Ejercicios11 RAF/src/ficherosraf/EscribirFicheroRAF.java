package ficherosraf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class EscribirFicheroRAF {

	//RAF
	//Random Access File
	//NO guarda texto plano, guarda registros
	//permite acceder a una posici�n determinada
	//me puedo desplazar por los registros
	//puede leer y escribir a la vez
	//modo
	// r -> read
	// w -> write
	//metodos escribir (writeInt, writeDouble, writeBytes, writeLong)
	//getFilePointer devuelve la posicion actual donde se va a realizar la operaci�n
	//seek coloca el fichero en una posici�n determinada
	// length tama�o archivo

	public static void main(String[] args) {
		try {
			// 1. Abrir el archivo en acceso RAF
			RandomAccessFile f = new RandomAccessFile("datos.txt","rw");
			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());	
			// 3.- escribimos una cadena de texto
			f.writeBytes("Esto es un texto");
			// 4.- cerrar el fichero
			f.close();
			System.out.println("Fichero actualizado");
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}

	}

}
