package ficherosraf;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class LeerYEscribirEnRAF {

	public static void main(String[] args) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// 1.- abrir el acceso al archivo
		try {
			RandomAccessFile f = new RandomAccessFile("datos2.txt", "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.println("Nombre ");
				String nombre = in.readLine();
				// grabar el nombre en el archivo
				f.writeUTF(nombre);
				System.out.println("�Deseas continuar? (si/no)");
				respuesta = in.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			// 3.- cerrar el archivo
			f.close();

			// leer el archivo
			// abrir el archivo
			RandomAccessFile f2 = new RandomAccessFile("datos2.txt", "rw");
			String nombre = "";
			boolean finFichero = false;

			do {
				try {
					nombre = f2.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f2.close();
				}
			} while (!finFichero);

		} catch (IOException e) {
			System.out.println("Error de entrada de datos");
		}

	}

}
