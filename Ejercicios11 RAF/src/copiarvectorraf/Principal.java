package copiarvectorraf;

public class Principal {

	public static void main(String[] args) {
		//crear el vector
		ListaProductos unaLista = new ListaProductos();
		//rellenar el vector
		System.out.println("Rellenamos vector");
		unaLista.rellenarLista();
		System.out.println("Visualizar vector");
		unaLista.visualizarLista();		
		//descargar el vector al archivo
		System.out.println("Copiamos informacion al archivo");
		unaLista.copiarListaArchivo("datos3");
		System.out.println("Mostando datos del archivo");
		unaLista.visualizarArchivo("datos3");
		//modificar precios
		System.out.println("Modificando precios");
		unaLista.modificarPrecioArchivo("datos3");
		System.out.println("Mostrando datos modificados");
		unaLista.visualizarArchivo("datos3");
	}

}
