package ejercicio09;

public class Ejercicio09 {

	public static void main(String[] args) {

		//Genero un numero decimal entre 0 y 100
		double aleatorio = Math.random() * 101;
		
		//Lo convierto a int
		int enteroAleatorio = (int)aleatorio;
		
		System.out.println(enteroAleatorio);
		
		//Despu�s muestro los numeros desde ese n� aleatorio
		// hasta el -100 de 7 en 7
		
		for( int i = enteroAleatorio ; i >= -100; i = i -7){
			System.out.println(i);
		}
		
	}	

}
