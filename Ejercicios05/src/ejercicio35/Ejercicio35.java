package ejercicio35;

import java.util.Scanner;

public class Ejercicio35 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un n�mero");
		int limite = input.nextInt();
		
		int contadorPrimos = 0;
		
		for(int i = 1; i <= limite; i++){
			
			//Uso el c�digo del ejercicio anterior 
			//para saber si el n�mero es primo
			int numero = i;
			
			boolean esPrimo = true;
			for(int j = 2; j < numero; j++){
				//Si encuentro alg�n divisor, no es primo
				if(numero % j == 0){
					esPrimo = false;
					break;
				}
			}
			if(esPrimo){
				contadorPrimos++;
			}
		}
		
		System.out.println("Hay tantos numeros primos: " + contadorPrimos);
		
		input.close();
	}

}
