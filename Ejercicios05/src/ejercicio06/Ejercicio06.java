package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un numero entero");
		int numero=input.nextInt();
		
		//a priori no se la cantidad de veces que se va a repetir el bucle
		
		int cantidadCifras=0;
		
		//mientras que el resultado de la division no sea cero
		while (numero!=0) {
			//divido entre 10
			numero=numero/10;
			//cada vez que divido,cuento un cifra
			cantidadCifras++;
		}
		System.out.println(cantidadCifras);
		
		input.close();

	}

}
