package ejercicio24;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce el ancho");
		int ancho=input.nextInt();
		
		System.out.println("Introduce el alto");
		int alto=input.nextInt();
		
		for (int i=0;i<alto;i++) {
			for (int j=0;j<ancho;j++) {
				System.out.print("*");
			}
			System.out.println();
		}
		
		input.close();
	}

}
