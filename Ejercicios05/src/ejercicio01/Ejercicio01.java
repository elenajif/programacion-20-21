package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {
	static final String MENSAJE_FINAL = "Fin del programa";

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un numero");
		int limite = input.nextInt();
		for (int i = 1; i < limite; i++) {
			System.out.println(i);
		}
		for (int i = 1; i < limite; i++) {
			System.out.print(i + " ");
		}
		System.out.println("");
		System.out.println(MENSAJE_FINAL);
		input.close();
	}
}
