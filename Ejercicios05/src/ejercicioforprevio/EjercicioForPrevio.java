package ejercicioforprevio;

import java.util.Scanner;

public class EjercicioForPrevio {

	public static void main(String[] args) {
		// bucle for
		// inicio fin incremento
		// int i=0 inicio
		// i<5 fin
		// i++ incremento
		// la variable que est� dentro del for
		// se suele llamar i, j, k
		// se declaran dentro del for
		System.out.println("bucle 1");
		for (int i = 0; i < 5; i++) {
			System.out.println(i);
		}
		System.out.println("bucle 2");
		for (int i = 3; i < 7; i++) {
			System.out.println(i);
		}
		System.out.println("bucle 3");
		for (int i = 10; i > 0; i--) {
			System.out.println(i);
		}
		System.out.println("bucle 4");
		// tabla del 2
		for (int i = 1; i <= 10; i++) {
			System.out.println(i + " por 2 es " + 2 * i);
		}
		System.out.println("bucle 5");
		// tabla del 5
		for (int i = 1; i <= 10; i++) {
			System.out.println(i + " por 5 es " + 5 * i);
		}
		// tabla del numero solicitado por teclado
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un numero");
		int numero = input.nextInt();
		for (int i = 1; i <= 10; i++) {
			System.out.println(i + " por "+numero+" es "+numero*i);
		}
		input.close();

	}

}
