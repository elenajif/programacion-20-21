package ejercicio20;

import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine();

		System.out.println("Introduce un caracter");
		char caracter = input.nextLine().charAt(0);

		int contadorCaracter = 0;

		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == caracter) {
				contadorCaracter++;
			}
		}

		System.out.println(
				"La cantidad de veces que aparece el caracter " + 
						caracter + " en la cadena " + contadorCaracter);
		input.close();
	}

}
