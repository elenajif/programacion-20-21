package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce el tama�o del deposito");
		float volumenTotal = input.nextFloat();

		//partimos de un deposito vacio
		float litrosActuales = 0;

		do {
			System.out.println("Introduce los litros");
			System.out.println("Positivo echo litros, negativo extraigo litros");
			// acumulador
			// variable=variable+a�adir -> 5 = 5+2 -> 7
			// variable=variable-restar -> 5 = 5-2 -> 3
			// en mi caso variable es litrosActuales
			// litrosActuales = litrosActuales+a�adir
			// litrosActuales = litrosActuales-restar
			
			//en una linea
			litrosActuales = litrosActuales + input.nextFloat();
			
			//en dos lineas
			//float litrosNuevos =input.nextFloat()
			// litrosActuales = litrosActuales+litrosNuevos
			
			if (litrosActuales<volumenTotal) {
				System.out.println("Faltan "+(volumenTotal-litrosActuales)+" litros para llenarlo");
			}

		} while (litrosActuales < volumenTotal);
		
		System.out.println("Se ha llenado el deposito");

		input.close();

	}

}
