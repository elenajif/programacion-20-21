package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero mayor");
		int mayor = input.nextInt();

		System.out.println("Introduce un numero menor");
		int menor = input.nextInt();

		if (menor > mayor) {
			System.out.println("Datos incorrectos");
		} else {
			// voy a ir decrementando del numero grande al peque�o
			for (int i = mayor; i >= menor; i--) {
				System.out.println("estoy en el bucle");
				System.out.println(i);
			}
		}

		input.close();

	}

}
