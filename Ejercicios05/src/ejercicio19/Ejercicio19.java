package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		boolean hayMultiplos = false;

		for (int i = 0; i < 5; i++) {
			System.out.println("Dame un numero");
			int numero = input.nextInt();

			// compruebo si es multiplo de 3
			if (numero % 3 == 0) {
				hayMultiplos = true;
			}
		}

		// comprueba si ha habido multiplos
		if (hayMultiplos) {
			System.out.println("Hay multiplos");
		} else {
			System.out.println("No hay multiplos");
		}

		input.close();
	}

}
