package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce el valor de inicio");
		int inicio = input.nextInt();
		System.out.println("Introduce el valor de fin");
		int fin = input.nextInt();
		System.out.println("Introduce el salto");
		int salto = input.nextInt();

		if (salto <= 0) {
			System.out.println("El salto debe ser positivo");
		} else {
			// sabemos que el salto es positivo
			// ascendente o descendente
			if (inicio <= fin) {
				// ascendente
				for (int i = inicio; i <= fin; i = i + salto) {
					System.out.println(i + " ");
				}
			} else {
				// descendente
				for (int i = inicio; i >= fin; i -= salto) {
					System.out.println(i + " ");
				}
			}
		}

		input.close();

	}

}
