package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una contrasena");
		String password = input.nextLine();
		
		for(int i = 0; i < password.length(); i++){
			//Muestro cada caracter de la cadena 
			//y le concateno un espacio
			System.out.println(password.charAt(i) + " ");
			
		}
		
		
		input.close();
	}

}
