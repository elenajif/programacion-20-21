package ejercicio30;

import java.util.Scanner;

public class Ejercicio30 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero positivo");
		int numero= input.nextInt();
		
		System.out.println("Sus divisores son");
		
		for (int i=1;i<=numero;i++) {
			if (numero%i==0) {
				System.out.print(i+" ");
			}
		}
		
		input.close();
	}

}
