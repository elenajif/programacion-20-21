package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int numeroLeido;
		int contadorNumerosLeidos = 0;
		int sumaTotal = 0;

		do {
			System.out.println("Introduce numeros positivos o negativos (0 para terminar)");
			numeroLeido = input.nextInt();

			//contador
			contadorNumerosLeidos++;
			//acumulador
			sumaTotal = sumaTotal + numeroLeido;

		} while (numeroLeido != 0);
		
		System.out.println("Los numeros leidos son "+(contadorNumerosLeidos-1));
		System.out.println("La suma total es "+sumaTotal);

		input.close();

	}

}
