package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	static final String MENSAJE_FIN="Fin del programa";
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un numero mayor a 1");
		int numero=input.nextInt();
		int contador=numero;
		
		do {
			System.out.println(contador);
			contador--;
		} while (contador>=1) ;
		
		int contador1=numero;
		do {
			System.out.print(contador1);
			contador1--;			
		} while(contador1>=1);
		
		// puedo usar \n para hacer un salto de linea
		System.out.println("\n"+MENSAJE_FIN);
		input.close();

	}

}
