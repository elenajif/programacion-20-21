package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce la cantidad de sueldos a pedir");
		int cantidadSueldos=input.nextInt();
		
		float mayor=0.0F;
		//si es double no lleva F
		
		for (int i=0;i<cantidadSueldos;i++) {
			System.out.println("Introduce el sueldo ("+(i+1)+")");
			float sueldo=input.nextFloat();
			
			//si es el primer sueldo, ese es el mayor
			if (i==0) {
				mayor=sueldo;
			} else if (sueldo > mayor) {
				mayor = sueldo;
			}
		}
		
		System.out.println("El sueldo mayor es "+mayor);

		input.close();

	}

}
