package ejercicio34;

import java.util.Scanner;

public class Ejercicio34 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();
		
		//Para calcular si es primo 
		//compruebo si encuentro alg�n divisor
		
		//Supongo que es primo
		boolean esPrimo = true;
		for(int i = 2; i < numero; i++){
			//Si encuentro alg�n divisor, no es primo
			if(numero % i == 0){
				esPrimo = false;
				break;
			}
		}
		
		if(esPrimo){
			System.out.println("El numero es primo");
		}else{
			System.out.println("El numero no es primo");
		}
		
		input.close();
	}

}
