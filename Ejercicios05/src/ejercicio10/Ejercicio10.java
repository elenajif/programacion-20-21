package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double resultado=0;
		
		for (int i=0;i<10;i++) {
			System.out.println("Introduce un numero decimal");
			double numero=input.nextDouble();
			
			//almaceno el resultado acumulando el numero leido
			//acumulador
			resultado=resultado+numero;
		}
		
		System.out.println("La suma es "+resultado); 
		input.close();

	}

}
