package ejercicio28;

import java.util.Scanner;

public class Ejercicio28 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int numeroLeido;
		int contadorNumerosLeidos = 0;
		int mayor=0;
		int menor=999999999;
		
		do{
			System.out.println("Introduce numeros(termina con el 0)");
			numeroLeido = input.nextInt();
			contadorNumerosLeidos++;
			if (numeroLeido!=0 && numeroLeido>mayor) {
				mayor=numeroLeido;
			}
			if (numeroLeido!=0 && numeroLeido<menor) {
				menor=numeroLeido;
			}
	
		}while(numeroLeido != 0);
		
		System.out.println("La cantidad de numeros leidos es: " + contadorNumerosLeidos);
		System.out.println("El mayor es: " + mayor);
		System.out.println("El menor es: " + menor);
		
		input.close();
	}
}
