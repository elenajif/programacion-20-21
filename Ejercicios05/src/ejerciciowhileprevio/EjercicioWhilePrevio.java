package ejerciciowhileprevio;

public class EjercicioWhilePrevio {

	public static void main(String[] args) {
		int cantidad = 5;
		System.out.println("Bucle1");
		System.out.println("Cantidad inicial "+cantidad);
		System.out.println("Cantidad en el while");
		while (cantidad > 2) {
			cantidad = cantidad - 1;
			System.out.println(cantidad);
		}
		cantidad = 15;
		System.out.println("Bucle2");
		System.out.println("Cantidad inicial "+cantidad);
		System.out.println("Cantidad en el while");
		while (cantidad < 20) {
			cantidad=cantidad+1;
			System.out.println(cantidad);
		}
		cantidad = 25;
		System.out.println("Bucle3");
		System.out.println("Cantidad inicial "+cantidad);
		System.out.println("Cantidad en el while");
		while (cantidad < 50) {
			cantidad=cantidad+5;
			System.out.println(cantidad);
		}
		cantidad = 7;
		System.out.println("Bucle4");
		System.out.println("Cantidad inicial "+cantidad);
		System.out.println("Cantidad en el while");
		while (cantidad < 50) {
			cantidad=cantidad*2;
			System.out.println(cantidad);
		}
		
	}

}
