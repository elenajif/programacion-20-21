package ejercicio25;

import java.util.Scanner;

public class Ejercicio25 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		char respuesta;
		do {
			System.out.println("Introduce los grados centigrados");
			double centigrados=input.nextDouble();
			input.nextLine();
			System.out.println("Kelvin "+(centigrados+273));
			System.out.println("¿quieres repetir (s/n)?");
			respuesta = input.nextLine().charAt(0);
			
		} while (respuesta=='S'||respuesta=='s');
		
		input.close();
	}

}
