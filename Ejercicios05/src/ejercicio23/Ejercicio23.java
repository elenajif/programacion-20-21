package ejercicio23;

import java.util.Scanner;

public class Ejercicio23 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double sumaNotas=0;
		double nota;
		int cantidadNotas=0;
		//para calcular la media, necesito contar la cantidad de notas
		// y sumar el total de las notas
		//realizaré una división, siempre que la cantidad de notas no sea 0
		
		do {
			System.out.println("Introduce nota del alumno");
			nota = input.nextDouble();
			
			if (nota>=0) {
				sumaNotas=sumaNotas+nota;
				cantidadNotas++;
			}
		} while(nota>=0);
		
		if (cantidadNotas!=0) {
			System.out.println("La media es "+(sumaNotas/cantidadNotas));
		}
		
		input.close();

	}

}
