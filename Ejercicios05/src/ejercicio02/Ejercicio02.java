package ejercicio02;

public class Ejercicio02 {
	static final String MENSAJE_FIN = "Fin del programa";

	public static void main(String[] args) {
		int contador = 100;
		
		System.out.println(contador);
		while (contador>0) {
			contador--;
			//contador=contador-1
			System.out.println(contador);
		}
		contador = 100;
		System.out.print(contador+" ");
		while (contador>0) {
			contador--;
			//contador=contador-1
			System.out.print(contador+" ");
		}

		System.out.println();
		System.out.println(MENSAJE_FIN);
	}

}
