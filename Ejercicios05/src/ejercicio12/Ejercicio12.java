package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int numero;
		do {
			System.out.println("1.- Opcion 1");
			System.out.println("2.- Opcion 2");
			System.out.println("3.- Salir");
			System.out.println("Introduce una opci�n");
			numero = input.nextInt();

			switch (numero) {
			case 1:
				System.out.println("opcion 1 seleccionada");
				break;
			case 2:
				System.out.println("opcion 2 seleccionada");
				break;
			case 3:
				System.out.println("Salir");
				break;
			default:
				System.out.println("Opci�n incorrecta");
				break;
			}

		} while (numero != 3);
		input.close();

	}

}
