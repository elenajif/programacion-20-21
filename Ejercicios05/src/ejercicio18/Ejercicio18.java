package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el tama�o del lado");
		int lado= input.nextInt();
		//primer for pinta hacia abajo, eje Y, vertical
		//segundo for pinta hacia la derecha, eje X, horizontal
		for (int i=0;i<lado;i++) {
			for (int j=0;j<lado;j++) {
				System.out.print("*");
			}
			System.out.println("");
		}

		input.close();

	}

}
