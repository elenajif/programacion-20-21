package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame la base");
		int base = input.nextInt();

		System.out.println("Dame el exponente");
		int exponente = input.nextInt();

		int resultado = 1;
		
		//bucle que muestra la potencia
		//3^5=3*3*3*3*5 -> repito la multiplicacion 5 veces (exponente)
		//resultado es un acumulador que multiplica (se inicializa a 1)
		for (int i=0;i<exponente;i++) {
			resultado=resultado*base;
		}
		System.out.println("El resultado es "+resultado);
		input.close();
	}

}
