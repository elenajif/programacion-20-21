package ejercicio08;

public class Ejercicio08 {

	public static void main(String[] args) {
		//una forma con int
		for (int i=65;i<91;i++) {
			System.out.println((char)i);
			if (i=='N') {
				System.out.println("�");
			}
		}
		
		//otra forma con char
		for (char i='A';i<='Z';i++) {
			System.out.println(i);
			if (i=='N') {
				System.out.println("�");
			}
		}
	}

}
