package ejercicio31;

import java.util.Scanner;

public class Ejercicio31 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un numero");
		int numero=input.nextInt();
		
		//para calcular si un numero es perfecto, debo ir sumando sus divisores
		int sumaDivisores=0;
		
		for (int i=1;i<numero;i++) {
			if (numero%i==0) {
				//es divisor, lo guardo y lo sumo
				sumaDivisores=sumaDivisores+i;
			}
		}
		
		//si la suma de los divisores es igual al numero, es perfecto
		if (sumaDivisores==numero) {
			System.out.println("Es perfecto");
		} else {
			System.out.println("No es perfecto");
		}
		
		input.close();
	}

}
