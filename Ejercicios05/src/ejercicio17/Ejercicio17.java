package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		boolean hayNegativos = false;

		for (int i = 0; i < 10; i++) {
			System.out.println("Dame un numero");
			int numero = input.nextInt();
			// revisar si el numero es negativo
			if (numero < 0) {
				hayNegativos = true;
			}
		}

		// comprobar si ha habido y mostrar un mensaje
		if (hayNegativos) {
			System.out.println("Hay negativos");
		} else {
			System.out.println("No hay negativos");
		}

		input.close();

	}

}
