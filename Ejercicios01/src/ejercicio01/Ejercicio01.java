package ejercicio01;

public class Ejercicio01 {

	public static void main(String[] args) {
		// declaro 3 variables e inicializo
		int entero=-24;
		double decimal=56.3;
		char caracter='Y';
		
		//mostramos
		System.out.println(entero);
		System.out.println(decimal);
		System.out.println(caracter);
		
		//sumo entero del double
		System.out.println(entero+decimal);
		//resto el entero del double
		System.out.println(entero-decimal);
		//muestro el valor numerico del caracter
		System.out.println((int)caracter);

	}

}
