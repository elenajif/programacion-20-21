package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {
		int varA=6;
		int varB=-45;
		int varC=500;
		int varD=-1234;
		
		System.out.println("Muestro las 4 variables");
		System.out.println("varA "+varA);
		System.out.println("varB "+varB);
		System.out.println("varC "+varC);
		System.out.println("varD "+varD);

		int aux=varA;
		varA=varB;
		varB=varC;
		varC=varD;
		varD=aux;
		
		System.out.println("Muestro las 4 variables");
		System.out.println("varA "+varA);
		System.out.println("varB "+varB);
		System.out.println("varC "+varC);
		System.out.println("varD "+varD);
		
		
	}

}
