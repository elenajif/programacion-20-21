package ejercicio14previo;

public class Ejercicio14Previo {

	public static void main(String[] args) {
		String cadenaEntero = "123";
		System.out.println(cadenaEntero);
		// cadenaEntero contiene un String
		int entero = Integer.parseInt(cadenaEntero);
		// entero contiene un int
		System.out.println(entero);
		// una vez tengo el entero, ya puedo hacer operaciones
		System.out.println(entero*25);
		// si quiero guardar un boolean, parseBoolean 
		boolean valor = Boolean.parseBoolean("true");
		System.out.println(valor);
		// si quiero guardar un double, parseDouble
		double decimal = Double.parseDouble("23.45");
		System.out.println(decimal);

	}

}
