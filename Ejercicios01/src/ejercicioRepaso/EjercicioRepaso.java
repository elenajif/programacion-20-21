package ejercicioRepaso;

import java.util.Scanner;

public class EjercicioRepaso {

	public static void main(String[] args) {
		// declarar una variable llamada num1 de tipo byte
		byte num1;
		// asignarle un valor
		num1 = 33;

		// Crear una variable num�rica entera llamada num2 y asignarle un valor
		int num2 = 56;
		// Crear una variable num�rica con decimales llamada num3 y asignarle un valor
		double num3 = 25.3;

		// Realizar las operaciones + - * / % con num2 y num3
		System.out.println("Suma " + num2 + num3);
		System.out.println("Resta " + (num3 - 10));
		System.out.println("Multiplicacion " + (num2 * 7));
		System.out.println("Divisi�n " + num2 * num3);
		System.out.println("Resto " + num2 % 2);

		// Crear dos variables enteras entero1, entero2 y realizar la divisi�n entera
		int entero1 = 3;
		int entero2 = 4;
		int divide = entero1 / entero2;
		System.out.println("Division entera " + divide);
		// Crear dos variables enteras entero1, entero2 y realizar la divisi�n decimal
		double divide1 = (double) entero1 / (double) entero2;
		System.out.println("Division decimal " + divide1);

		// incrementar en uno la variable entero1
		entero1++;
		System.out.println("entero1 incrementado " + entero1);

		// asignar a entero2 el valor que ya ten�a m�s el valor de entero1
		// usar notacion corta
		// forma larga -> entero2 = entero2 + entero1
		System.out.println("entero2 antes de asignar "+entero2);
		entero2 += entero1;
		System.out.println("entero2 despu�s de asignar "+entero2);

		// crear un String llamado cadena1 usando instanciaci�n corta
		String cadena1= "Estamos teniendo un gran d�a para repasar";
		// mostrar su longitud
		System.out.println("Longitud "+cadena1.length());
		// mostrar el cuarto caracter
		System.out.println("Mostramos cuarto caracter "+cadena1.charAt(3));

		// crear otro String llamado cadena2 usando instanciaci�n larga
		// compararlo con el primer String
		String cadena2 = new String("Hola Caracola");
		int resultado = cadena1.compareTo(cadena2);
		System.out.println("Comparando cadenas "+resultado);

		// usar el operador condicional para mostrar en un mensaje si entero1 es mayor
		// o igual que entero2 o menor
		entero1=7;
		entero2=16;
		System.out.println(entero1>entero2?"entero1 es mayor que entero2":
						                       (entero1==entero2?"son iguales":
						            	            "entero1 es menor que entero2"));

		// utilizar el operador && para crear una condici�n que muestre true con entero1
		// y entero2
		System.out.println("condicicion true -> "+((entero1<entero2)&&(entero1==7)));

		// utilizar el operador || para crear una condici�n que muestre false con
		// entero1 y entero2
		System.out.println("condicicion false -> "+((entero1>entero2)||(entero1==5)));

		// pedir un dato por pantalla usando la clase Scanner y mostrar su valor por
		// pantalla
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un n�mero");
		int numerito=input.nextInt();
		System.out.println("El n�mero introducido es "+numerito);

	}

}
