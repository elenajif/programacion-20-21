package ejercicio17previo;

public class Ejercicio17Previo {

	public static void main(String[] args) {
		String cadena1 = "Esto es una cadena";
		String cadena2 = "Hola Caracola";
		String cadena3 = "Adios";
		String cadena4 = "Esto es otra cadena";
		String cadena5 = "Adios";
		System.out.println("Cadena1 " + cadena1);
		System.out.println("Longitud cadena1 " + cadena1.length());
		System.out.println("Cadena2 " + cadena2);
		System.out.println("Longitud cadena2 " + cadena2.length());
		System.out.println("Cadena3 " + cadena3);
		System.out.println("Longitud cadena3 " + cadena3.length());
		System.out.println("Cadena4 " + cadena4);
		System.out.println("Longitud cadena4 " + cadena4.length());
		System.out.println("Cadena5 " + cadena5);
		System.out.println("Longitud cadena5 " + cadena5.length());

		// resultado =cadena1.compareTo(cadena2)
		// compara alfabéticamente
		// resultado>0 -> cadena1>cadena2
		// resultado<0 -> cadena1<cadena2
		// resultado==0 -> iguales
		int resultado1 = cadena1.compareTo(cadena2);
		System.out.println("cadena1 comparado con cadena2 " + resultado1);
		int resultado2 = cadena1.compareTo(cadena3);
		System.out.println("cadena1 comparado con cadena3 " + resultado2);
		int resultado3 = cadena2.compareTo(cadena3);
		System.out.println("cadena2 comparado con cadena3 " + resultado3);
		int resultado4 = cadena1.compareTo(cadena4);
		System.out.println("cadena1 comparado con cadena4 " + resultado4);
		int resultado5 = cadena3.compareTo(cadena5);
		System.out.println("cadena3 comparado con cadena5 " + resultado5);

	}

}
