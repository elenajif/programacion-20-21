package ejercicio12plus;

public class Ejercicio12Plus {

	public static void main(String[] args) {
		// crear un entero y asignar un valor
		int numero =3;
		// crear un double y asignarle el entero
		double miDouble = numero;
		System.out.println(numero);
		System.out.println(miDouble);
		// crear otro double y asignar un valor
		double miDouble2=8.25;
		System.out.println(miDouble2);
		// crear un entero y asignarle el double (corregir error) -> castear
		int numero1=(int)miDouble2;
		System.out.println(numero1);
		// crear dos enteros y dividir sin decimales
		int numeroA=3;
		int numeroB=5;
		System.out.println(numeroA/numeroB);
		// crear dos enteros y dividir con decimales
		System.out.println((double)(numeroA/numeroB)); //incorrecto
		System.out.println((double)numeroA/(double)numeroB); //correcto
		//mostrar el entero de un char
		char miLetra= 'B';
		System.out.println(miLetra);
		System.out.println((int)miLetra);
		

	}

}
