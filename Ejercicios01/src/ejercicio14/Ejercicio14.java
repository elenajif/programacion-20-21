package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {
		String textoEntero="2345";
		String textoEnteroLargo="212154564561478";
		String textoDecimal="1234.5647";
		String textoByte="123";
		
		int entero=Integer.parseInt(textoEntero);
		System.out.println(entero);
		long enteroLargo=Long.parseLong(textoEnteroLargo);
		System.out.println(enteroLargo);
		double decimal=Double.parseDouble(textoDecimal);
		System.out.println(decimal);
		byte numByte=Byte.parseByte(textoByte);
		System.out.println(numByte);		

	}

}
