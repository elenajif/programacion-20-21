package ejercicio11;

public class Ejercicio11 {

	public static void main(String[] args) {
		long maximo = 9223372036854775807L;
		long minimo = -9223372036854775808L;
		System.out.println(maximo);
		System.out.println(minimo);
		
		short short1 = (short) maximo;
		short short2 = (short) minimo;
		
		System.out.println(short1);
		System.out.println(short2);
		
	}

}
