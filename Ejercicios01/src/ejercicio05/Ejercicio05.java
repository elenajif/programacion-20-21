package ejercicio05;

public class Ejercicio05 {

	public static void main(String[] args) {
		int numeroA = 5;
		int numeroB = 3;
		int numeroC = -12;

		boolean expresion;
		
		expresion=numeroA>3;
		System.out.println("a.- numeroA>3 "+expresion);
		
		expresion=numeroA>numeroC;
		System.out.println("b.- numeroA>numeroC "+expresion);
		
		expresion=numeroA<numeroC;
		System.out.println("c.- numeroA<numeroC "+expresion);
		
		expresion=numeroB<numeroC;
		System.out.println("d.- numeroB<numeroC "+expresion);
		
		expresion=numeroB!=numeroC;
		System.out.println("e.- numeroB!=numeroC "+expresion);
		
		expresion=numeroA==3;
		System.out.println("f.- numeroA==3 "+expresion);
		
		expresion=numeroA*numeroB==15;
		System.out.println("g.- numeroA*numeroB==15 "+expresion);
		
		expresion=numeroC/numeroB==-4;
		System.out.println("h.- numeroC/numeroB==-4 "+expresion);
		
		expresion=numeroC/numeroB<numeroA;
		System.out.println("i.- numeroC/numeroB<numeroA "+expresion);
		
		expresion=numeroC/numeroB<-10;
		System.out.println("j.- numeroC/numeroB<-10 "+expresion);
		
		expresion=numeroA%numeroB==2;
		System.out.println("k.- numeroA%numeroB==2 "+expresion);
		
		expresion=numeroA+numeroB+numeroC==5;
		System.out.println("l.- numeroA+numeroB+numeroC==5 "+expresion);
		
		expresion=(numeroA+numeroB==8)&&(numeroA-numeroB==2);
		System.out.println("m.- (numeroA+numeroB==8)&&(numeroA-numeroB==2) "+expresion);
		
		expresion=(numeroA+numeroB==8)||(numeroA-numeroB==6);
		System.out.println("n.- (numeroA+numeroB==8)||(numeroA-numeroB==6) "+expresion);
		
		expresion=numeroA>3&&numeroB>3&&numeroC<3;
		System.out.println("o.-numeroA>3&&numeroB>3&&numeroC<3 "+expresion);
		
		expresion=numeroA>3&&numeroB>=3&&numeroC<-3;
		System.out.println("p.- numeroA>3&&numeroB>=3&&numeroC<-3 "+expresion);
		
		
		
		
		
	}

}
