package ejercicio13previo;

public class Ejercicio13Previo {

	public static void main(String[] args) {
		int x=2;
		double y=4.25;
		// declaracion
		// int x;
		// asignacion
		// x=1;
		//Instanciación rápida
		//de momento usaremos esta
		// clase String pertenece al paquete java.lang
		// el paquete java.lang no necesita import
		String cadena="hola";
		String cadena1 = "primer programa";
		//Instanciación lenta
		//de momento no lo necesitamos
		String cadena2 = new String("otro programa");

		//Método para mostrar la longitud con un int
		System.out.println( cadena.length() ); //Muestra el int 4
		System.out.println( cadena1.length() ); //Muestra el int 15
		System.out.println( cadena2.length() ); //Muestra el int 13
		 
		//Método para mostrar el caracter 4 de la cadena (empieza en 0)
		System.out.println( cadena.charAt(3) ); //Muestra el caracter 'a'
		System.out.println( cadena1.charAt(3) ); //Muestra el caracter 'm'
		System.out.println( cadena2.charAt(3) ); //Muestra el caracter 'o'
	}

}
