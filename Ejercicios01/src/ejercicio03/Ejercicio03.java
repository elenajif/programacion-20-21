package ejercicio03;

public class Ejercicio03 {

	public static void main(String[] args) {
		//declaro
		int num;
		//asigno
		num=11;
		//int num=11; declaro y asigno
		System.out.println("valor inicial "+num);
		//incrementar 77
		num=num+77;
		//otra forma 
		//num+=77;
		System.out.println("incrementando 77 "+num);
		//decrementar 3
		num=num-3;
		//otra forma
		//num-=3;
		System.out.println("decrementando 3 "+num);
		//duplicar valor
		num*=2;
		//otra forma
		// num=num*2;
		System.out.println("duplicando su valor "+num);
		
		
	}

}
