package ejercicio18previo;

import java.util.Scanner;

public class Ejercicio18Previo {

	public static void main(String[] args) {
		// recordatorio
		// instanciacion corta de String
		// String cadena = "hola";
		// instanciacion larga de String
		// String cadena = new String("hola");

		// Scanner no tiene instanciacion corta
		// creamos un scanner
		Scanner input = new Scanner(System.in);
		// mostramos una frase que me indica que tengo que introducir
		System.out.println("Dame un numero entero");
		// leo el dato entero
		int entero = input.nextInt();
		//muestro el dato introducido
		System.out.println("El numero entero introducido es "+entero);
		//mostramos una frase que me indica que tengo que introducir
		System.out.println("Dame un numero con decimales (separador ,)");
		//leo el numero decimal
		double decimal=input.nextDouble();
		//muestro el dato introducido
		System.out.println("El numero decimal introducido es "+decimal);
		//cierro el scanner
		input.close();
	}

}
