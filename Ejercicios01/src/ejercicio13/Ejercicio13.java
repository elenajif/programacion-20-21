package ejercicio13;

public class Ejercicio13 {

	public static void main(String[] args) {
		String cadena="esto es una cadena";
		System.out.println(cadena);
		
		System.out.println("La longitud de la cadena es ");
		System.out.println(cadena.length());
		
		// otra forma
		System.out.println("La longitud de la cadena es "+cadena.length());
	}

}
