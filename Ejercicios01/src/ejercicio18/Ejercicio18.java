package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// byte
		System.out.println("Introduce un byte");
		byte numByte=input.nextByte();
		System.out.println("El n�mero introducido es "+numByte);
		// short
		System.out.println("Introduce un short");
		short numShort=input.nextShort();
		System.out.println("El n�mero introducido es "+numShort);
		// int
		System.out.println("Introduce un int");
		int numInt=input.nextInt();
		System.out.println("El n�mero introducido es "+numInt);
		// long
		System.out.println("Introduce un long");
		long numLong=input.nextLong();
		System.out.println("El n�mero introducido es "+numLong);
		// float
		System.out.println("Introduce un float");
		float numFloat=input.nextFloat();
		System.out.println("El n�mero introducido es "+numFloat);
		// double
		System.out.println("Introduce un double");
		double numDouble=input.nextDouble();
		System.out.println("El n�mero introducido es "+numDouble);
		// antes de leer un String (input.next)
		// se recomienda limpiar el buffer
		input.nextLine();
		// char .... hay que pensar
		System.out.println("Introduce un caracter");
		char caracter;
		caracter=input.next().charAt(0);
		System.out.println("El caracter introducido es "+caracter);
		input.close();
	}

}
