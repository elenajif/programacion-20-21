package ejercicio16;

public class Ejercicio16 {

	public static void main(String[] args) {
		// operador condicional
		// condicion ? valor true : valor false
		// hay que usar length y parse
		String cadena = "123415456";
		System.out.println(cadena.length() >= 5 ? Integer.parseInt(cadena) : "Tiene menos de 5 cifras");
		String cadena1 = "123";
		System.out.println(cadena1.length() >= 5 ? Integer.parseInt(cadena1) : "Tiene menos de 5 cifras");
		
		//creo un entero y muestro la variable
		String cadena2 = "12344556664";
		int numero=Integer.parseInt(cadena2);
		System.out.println(cadena2.length() >= 5 ? numero : "Tiene menos de 5 cifras");

	}

}
