package ejercicio15;

public class Ejercicio15 {

	static final String MENSAJE_INICIO= "Inicio del programa";
	static final String MENSAJE_FIN="Fin del programa";
	
	public static void main(String[] args) {
		System.out.println(MENSAJE_INICIO);
		String cadena="Esto es un mensaje largo que tiene m�s de 20 caracteres";
		String subcadena=cadena.substring(11, 23);
		System.out.println(subcadena);		
		System.out.println(MENSAJE_FIN);

	}

}
