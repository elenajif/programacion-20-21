package ejercicio10;

public class Ejercicio10 {

	public static void main(String[] args) {
		int variableA=35;
		System.out.println(variableA%5==0?"es multiplo de 5":"no lo es");
		System.out.println(variableA%10==0?"es multiplo de 10":"no lo es");
		System.out.println(variableA>=100?(variableA==100?"es igual a 100":
												"es mayor de 100"):"Es menor a 100");

	}

}
