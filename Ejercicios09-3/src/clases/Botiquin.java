package clases;

import java.util.ArrayList;
import java.util.Scanner;

public class Botiquin {
	Scanner scan = new Scanner(System.in);

	private String nombreBotiquin;
	private String ubicacion;
	private ArrayList<Producto> productos;

	public Botiquin() {
		this.nombreBotiquin = "";
		this.ubicacion = "";
		this.productos = new ArrayList<Producto>();
	}

	public Botiquin(String nombreBotiquin, String ubicacion) {
		this.nombreBotiquin = nombreBotiquin;
		this.ubicacion = ubicacion;
		this.productos = new ArrayList<Producto>();
	}

	public String getNombreBotiquin() {
		return nombreBotiquin;
	}

	public void setNombreBotiquin(String nombreBotiquin) {
		this.nombreBotiquin = nombreBotiquin;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public void rellenar() {
		System.out.println("Introduce el nombre del botiquin");
		this.nombreBotiquin = scan.nextLine();
		System.out.println("Introduce la ubicacion del botiquin");
		this.ubicacion = scan.nextLine();
		boolean continuar = true;
		do {
			System.out.println("Introduce el nombre del producto (* para salir)");
			String nombre = scan.nextLine();
			if (nombre.equals("*")) {
				continuar = false;
			} else {
				Producto producto = new Producto();
				producto.setNombreProducto(nombre);
				producto.rellenar();
				this.productos.add(producto);
			}

		} while (continuar);
	}

	public void visualizar() {
		System.out.println("Nombre botiquin " + this.nombreBotiquin);
		System.out.println("Ubicacion " + this.ubicacion);
		for (Producto producto : productos) {
			producto.visualizar();
		}

	}

	public void modificar() {
		System.out.println("�Que producto deseas modificar?");
		boolean esta = false;
		String nombre = scan.nextLine();
		for (Producto producto : productos) {
			if (producto.getNombreProducto().equalsIgnoreCase(nombre)) {
				System.out.println("Que nombre quieres poner a " + producto.getNombreProducto());
				String nuevo = scan.nextLine();
				producto.setNombreProducto(nuevo);
				System.out.println("El nombre se ha cambiado a " + producto.getNombreProducto());
				esta = true;
			}
		}
		if (!esta) {
			System.out.println("No se ha encontrado el producto");
		}
	}

	public double mediaPrecio() {
		int media = 0;
		for (Producto producto : productos) {
			media += producto.getPrecio();
		}
		return (media / productos.size());
	}

	public double totalPrecio() {
		int totalPrecio=0;
		for (Producto producto:productos) {
			totalPrecio+=producto.getPrecio();
		}
		return totalPrecio;
	}
}
