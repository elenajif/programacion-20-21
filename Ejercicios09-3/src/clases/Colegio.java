package clases;

import java.util.ArrayList;
import java.util.Scanner;

public class Colegio {
	Scanner scan = new Scanner(System.in);
	
	private String nombre;
	private ArrayList<Botiquin> botiquines;
	
	public Colegio() {
		this.nombre = "";
		this.botiquines = new ArrayList<Botiquin>();
	}

	public Colegio(String nombre) {
		this.nombre = nombre;
		this.botiquines = new ArrayList<Botiquin>();
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

	
	public void rellenar() {
		System.out.println("Dame el nombre del colegio");
		this.nombre=scan.nextLine();
		
		String respuesta="";
		do {
			Botiquin botiquin = new Botiquin();
			botiquin.rellenar();
			this.botiquines.add(botiquin);
			
			System.out.println("�Desea introducir otro botiquin? (si/no)");
			respuesta=scan.nextLine();
			
		} while (respuesta.toLowerCase().trim().equalsIgnoreCase("si"));
		
		
	}
	
	public void visualizar() {
		System.out.println("Colegio "+this.nombre);
		
		for (Botiquin botiquin: botiquines) {
			botiquin.visualizar();
			System.out.println("Media botiquin "+botiquin.mediaPrecio());
		}
		System.out.println("Media Colegio "+mediaBotiquin());
	}
	
	public void modificar() {
		System.out.println("�Qu� botiquin queremos modificar? (indica nombre)");
		boolean esta=false;
		String nombre=scan.nextLine();
		for (Botiquin botiquin: botiquines) {
			if (botiquin.getNombreBotiquin().equalsIgnoreCase(nombre)) {
				botiquin.modificar();
				esta=true;
			}
		}
		if (!esta)  {
			System.out.println("No se ha encontrado el botiquin");
		}
		
	}
	
	public void ordenar() {
		for (int i=0;i<botiquines.size()-1;i++) {
			for (int j=i+1;j<botiquines.size();j++) {
				if (botiquines.get(i).getNombreBotiquin().compareTo(botiquines.get(j).getNombreBotiquin())<0) {
						Botiquin temp =botiquines.get(i);
						botiquines.set(i, botiquines.get(j));
						botiquines.set(j, temp);
				}
			}
		}
		System.out.println("Lista ordenada");
	}
	
	public double mediaBotiquin() {
		int media=0;
		for (Botiquin botiquin: botiquines) {
			media+=botiquin.totalPrecio();
		}
		return (media/botiquines.size());
	}
	
	
	
	

}
