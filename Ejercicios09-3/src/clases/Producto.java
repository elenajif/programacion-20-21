package clases;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Producto {

	Scanner scan = new Scanner(System.in);

	private String nombreProducto;
	private int precio;

	public Producto() {
		this.nombreProducto = "";
		this.precio = 0;
	}

	public Producto(String nombreProducto, int precio) {
		this.nombreProducto = nombreProducto;
		this.precio = precio;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public boolean rellenar() {
		boolean error = false;
		do {
			try {
				System.out.println("Introduce el precio del producto");
				this.precio = scan.nextInt();
				error = false;
			} catch (InputMismatchException e) {
				System.out.println("Caracter introducido no valido");
				error = true;
				scan.nextLine();
			} catch (Exception e) {
				System.out.println("Algo ha fallado");
				error = true;
				scan.nextLine();
			}

		} while (error);
		return false;

	}
	
	public void visualizar() {
		System.out.println("Producto "+this.nombreProducto);
		System.out.println("Precio "+this.precio);
	}

}
