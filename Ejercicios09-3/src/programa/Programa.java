package programa;

import clases.Colegio;

public class Programa {

	public static void main(String[] args) {
		Colegio colegio1 = new Colegio();
		
		colegio1.rellenar();
		colegio1.visualizar();
		System.out.println();
		colegio1.modificar();
		System.out.println();
		colegio1.ordenar();
		colegio1.visualizar();

	}

}
