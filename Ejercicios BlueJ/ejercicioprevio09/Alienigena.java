 

public class Alienigena {
	//atributos
	String nombre;
	String color;
	int numOjos;
	double estatura;
	int piernas;
	

	//constructor sin parámetros
	//this -> hace referencia a "esta" clase
	public Alienigena() {
		this.nombre = "alienigena 1";
		this.color = "verde";
		this.numOjos = 2;
		this.estatura = 1.45;
		this.piernas = 2;
	}
	
	//constructor con parámetros
	public Alienigena(String nombre, String color, int numOjos, double estatura, int piernas) {
		this.nombre = nombre;
		this.color = color;
		this.numOjos = numOjos;
		this.estatura = estatura;
		this.piernas = piernas;
	}
	
	//metodos setter y getter
	//permiten cambiar valores y mostrar valores de los atributos
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getNumOjos() {
		return numOjos;
	}

	public void setNumOjos(int numOjos) {
		this.numOjos = numOjos;
	}

	public double getEstatura() {
		return estatura;
	}

	public void setEstatura(double estatura) {
		this.estatura = estatura;
	}

	public int getPiernas() {
		return piernas;
	}

	public void setPiernas(int piernas) {
		this.piernas = piernas;
	}
	
	
	
}
