public class InfoFrutas {

	//metodo void que recibe una Naranja
	//si recibo un String -> String cadena
	//si recibo una Naranja -> Naranja nr
	//para acceder a la informacion de la naranja -> nr.nombre nr.caracteristicas
	public void imprimirInfo(Naranja nr) {
		System.out.println("Fruta ");
		System.out.println("Su nombre es "+nr.nombre);
		System.out.println("Sus caracteristicas son "+nr.caracteristicas);
	}
	
	//si recibo una Manzana -> Manzana mn
	public void imprimirInfo(Manzana mn) {
		System.out.println("Fruta ");
		System.out.println("Su nombres es "+mn.nombre);
		System.out.println("Sus caracteristicas son "+mn.caracteristicas);
	}
	
	public static void main(String[] args) {
		System.out.println("Creo una naranja");
		//este objeto oNaranja me va a permitir crear una Naranja
		Naranja oNaranja = new Naranja();
		//este objeto inf me va a permitir usar el metodo imprimirInfo
		InfoFrutas inf = new InfoFrutas();
		//con inf accedo al metodo imprimirInfo que recibe una Naranja (en mi caso oNaranja)
		inf.imprimirInfo(oNaranja);
		System.out.println("Creo una manzana");
		Manzana oManzana = new Manzana();
		inf.imprimirInfo(oManzana);
	}
}
