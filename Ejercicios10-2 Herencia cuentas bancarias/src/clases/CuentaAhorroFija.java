package clases;

public class CuentaAhorroFija extends Cuenta {
	
	public CuentaAhorroFija() {
		super();
		this.interes=2.6;
	}
	
	public CuentaAhorroFija(String numero, double saldo, double interes) {
		super(numero,saldo);
		this.interes=interes;
	}

	@Override
	public String toString() {
		return "CuentaAhorroFija [numero=" + numero + ", saldo=" + saldo 
				+ ", interes=" + interes + ", titular="
				+ titular + "]";
	}
	
	//metodos
	public double ingresoMes() {
		saldo+=100;
		return saldo;
	}
	

}
