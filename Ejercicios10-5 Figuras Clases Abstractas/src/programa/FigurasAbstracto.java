package programa;

import figuras.Circulo;
import figuras.Triangulo;

public class FigurasAbstracto {

	public static void main(String[] args) {
		Triangulo triangulo = new Triangulo(20, 12);
		Circulo circulo = new Circulo(3);

		System.out.println("Nombre " + circulo.dimeMiNombre());
		System.out.println("Area Circulo " + circulo.area());
		System.out.println("Nombre " + triangulo.dimeMiNombre());
		System.out.println("Area Triangulo " + triangulo.area());
	}

}
