package figuras;

public  class Circulo extends FiguraGeometrica{

    private double radio;

    public Circulo(double radio) {
        nombre = "Circulo";
        this.radio = radio;
    }
    
	@Override
    public double area() {
        return Math.PI*Math.pow(radio, 2);
    }

    @Override
    public double perimetro() {
        return 2*Math.PI*radio*radio;
    }


    
}
