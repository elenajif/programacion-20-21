package figuras;

public abstract class FiguraGeometrica {
    
    String nombre;
    
 	public FiguraGeometrica(){   
    	this.nombre="";
    }
    
    public FiguraGeometrica(String nombre){
        this.nombre = nombre;
    }

    public abstract double area();
    
    public abstract double perimetro();
    
    public String dimeMiNombre(){
        return nombre;
    }
    
}
