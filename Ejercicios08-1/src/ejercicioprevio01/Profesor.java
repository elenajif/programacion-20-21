package ejercicioprevio01;

import java.time.LocalDate;

/**
 * @author DAW
 * Clase en al que se guardan informaci�n de los profesores
 */
public class Profesor {
	private Alumno[] alumnos;
	
	/**
	 * constructor de un nuevo Profesor
	 * @param maxAlumnos indica cual es la cantidad m�xima de alumnos para este profesor
	 */
	public Profesor(int maxAlumnos) {
		this.alumnos=new Alumno[maxAlumnos];
	}
	
	/**
	 * metodo para dar de alta un nuevo alumno 
	 * @param codAlumno codigo que tendra el alumno en el aula
	 * @param nombre nombre del alumno
	 * @param apellidos apellidos del alumno
	 */
	
	public void altaAlumno(String codAlumno, String nombre, String apellidos) {
		for (int i=0;i<alumnos.length;i++) {
			if (alumnos[i]==null) {
				alumnos[i]= new Alumno(codAlumno);
				alumnos[i].setNombre(nombre);
				alumnos[i].setApellidos(apellidos);
				alumnos[i].setFechaMatricula(LocalDate.now());
				break;
			}
		}
	}
	
	/**
	 * metodo que busca un determinado alumno dado un codAlumno
	 * @param codAlumno codigo del alumno que quiero buscar
	 * @return objeto Alumno
	 */
	public Alumno buscarAlumno(String codAlumno) {
		for (int i=0;i<alumnos.length;i++) {
			if (alumnos[i]!=null) {
				if (alumnos[i].getCodAlumno().equals(codAlumno)) {
					return alumnos[i];
				}
			}
		}
		return null;
	}
}
