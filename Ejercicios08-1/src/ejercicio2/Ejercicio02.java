package ejercicio2;

import java.util.Scanner;

import ejercicios081.Vehiculo;

public class Ejercicio02 {
 
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.println("Datos 1� Vehiculo");
		System.out.println("Introduce marca");
		String marca = input.nextLine();
		
		System.out.println("Introduce tipo");
		String tipo = input.nextLine();
		
		System.out.println("Introduce el consumo"); 
		float consumo = input.nextFloat();
		
		System.out.println("Introduce el numero de ruedas");
		int numRuedas = input.nextInt();
		input.nextLine();
		
		Vehiculo vehiculo1 = new Vehiculo(tipo, marca, consumo, numRuedas);
		
		System.out.println("Datos 2� Vehiculo");
		System.out.println("Introduce marca");
		marca = input.nextLine();
		
		System.out.println("Introduce tipo");
		tipo = input.nextLine();
				
		Vehiculo vehiculo2 = new Vehiculo(tipo, marca);
		
		Vehiculo vehiculo3 = new Vehiculo();
		
		System.out.println("Datos 3� Vehiculo");
		System.out.println("Introduce marca");
		marca = input.nextLine();
		vehiculo3.setMarca(marca);
		
		System.out.println("Introduce tipo");
		tipo = input.nextLine();
		vehiculo3.setTipo(tipo);
		
		System.out.println("Introduce el consumo"); 
		consumo = input.nextFloat();
		vehiculo3.setConsumo(consumo);
		
		System.out.println("Introduce el numero de ruedas");
		numRuedas = input.nextInt();
		vehiculo3.setNumRuedas(numRuedas);
		
		input.close();
		
		mostrarDatosVehiculo(vehiculo1);
		mostrarDatosVehiculo(vehiculo2);
		mostrarDatosVehiculo(vehiculo3);
		
		System.out.println("Cambio los km del vehiculo1");
		vehiculo1.trucarCuentaKm(57000);
		
		System.out.println("Estimo el combustible consumido");
		float estimacion = vehiculo1.combustibleConsumido();
		System.out.println(estimacion);
		
		System.out.println("Reseteo el cuentakm");
		vehiculo1.trucarCuentaKm();
		
		System.out.println("Calculo el consumo para un viaje de 40km");
		estimacion = vehiculo1.combustibleConsumido(40);
		System.out.println(estimacion);
	}
	
	public static void mostrarDatosVehiculo(Vehiculo vehiculo){
		
		System.out.println("\nTipo: " + vehiculo.getTipo());
		System.out.println("Marca: " + vehiculo.getMarca());
		System.out.println("Consumo: " + vehiculo.getConsumo());
		System.out.println("Numero de Ruedas: " + vehiculo.getNumRuedas());
		System.out.println("Funciona: " + vehiculo.isFunciona());
		System.out.println("Km: " + vehiculo.getKmTotales());
		System.out.println();
		
	}

}
