package ejercicios084;

import java.util.Scanner;

import ejercicio2.Ejercicio02;
import ejercicios081.Vehiculo;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Vehiculo[] listaVehiculos = new Vehiculo[3];
		
		for(int i = 0; i < listaVehiculos.length; i++){
			System.out.println("Introduce tipo");
			String tipo = input.nextLine();
			
			System.out.println("Introduce marca");
			String marca = input.nextLine();
			
			System.out.println("Introduce el consumo");
			float consumo = input.nextFloat();
			
			System.out.println("Introduce numero ruedas");
			int numRuedas = input.nextInt();
			input.nextLine();
			
			listaVehiculos[i] = new Vehiculo(tipo, marca, consumo, numRuedas);
			
			System.out.println("Cantidad: " + listaVehiculos[0].getVehiculosCreados());
		}
		
		input.close();
		
		for(int i = 0; i < listaVehiculos.length; i++){
			Ejercicio02.mostrarDatosVehiculo(listaVehiculos[i]);
			System.out.println(listaVehiculos[i].getVehiculosCreados());
		}
		
	}
}
