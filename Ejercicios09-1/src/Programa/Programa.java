package Programa;

import clases.GestorTrabajos;

public class Programa {

	public static void main(String[] args) {
		System.out.println("1.- Crear instancia gestor trabajos");
		GestorTrabajos gestor = new GestorTrabajos();
		
		System.out.println("2.- Alta de tres responsables con dni repetido");
		gestor.altaResponsable("54321", "responsable1");
		gestor.altaResponsable("12345", "responsable2");
		gestor.altaResponsable("12345", "responsable3");
		
		System.out.println("3.- Listar responsables");
		gestor.listarResponsables();
		
		System.out.println("4.- Buscar un responsable que exista");
		System.out.println(gestor.buscarResponsable("12345"));
		System.out.println("4.- Buscar un responsable que no exista");
		System.out.println(gestor.buscarResponsable("123456"));
		
		System.out.println("5.- Dar de alta 3 trabajos");
		gestor.altaTrabajo("trabajo1", "cliente1", 10.0, "2019-04-02");
		gestor.altaTrabajo("trabajo2", "cliente2", 20.0, "2019-05-02");
		gestor.altaTrabajo("trabajo3", "cliente3", 30.0, "2020-04-02");
		
		System.out.println("6.- Asignar el mismo responsable a dos trabajos");
		gestor.asignarResponsable("12345", "trabajo1");
		gestor.asignarResponsable("12345", "trabajo2");
		
		System.out.println("7.- Listar trabajos del responsable anterior");
		gestor.listarTrabajosDeResponsable("12345");
		
		System.out.println("8.- Listar los trabajos del 2019");
		gestor.listarTrabajoAnno(2019);
		
		System.out.println("9.- Eliminar un trabajo que tenga asignado un responsable");
		System.out.println("Elimino trabajo2");
		gestor.eliminarTrabajo("trabajo2");
		
		System.out.println("10.- Listar los trabajos de ese responsable");
		gestor.listarTrabajosDeResponsable("12345");
		
		System.out.println("11.- ere y listar responsables");
		gestor.ere();
		gestor.listarResponsables();
		
		
	}

}
