package clasesejercicio0102;

public class SubClase  extends SuperClase {
	private int cantidad;
	
	public SubClase (String nombre, int cantidad) {
		super(nombre);
		this.cantidad=cantidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return super.toString() +"cantidad=" + cantidad ;
	}

}
