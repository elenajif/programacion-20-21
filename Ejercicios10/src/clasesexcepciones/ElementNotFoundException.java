package clasesexcepciones;

//creamos nuestra propia clase de excepcion
public class ElementNotFoundException extends Exception {

	//a�adimos el serial
	private static final long serialVersionUID = 1L;

	public ElementNotFoundException(String matricula) {
		super("No existe vehiculo con esta matricula "+matricula);
	}
}
