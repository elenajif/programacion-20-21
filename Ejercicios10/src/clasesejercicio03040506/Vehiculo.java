package clasesejercicio03040506;

public class Vehiculo {
	protected static int cantidadInstancias;
	
	protected String matricula;
	protected String marca;
	protected int plazas;
	
	public Vehiculo(String matricula, String marca, int plazas) {
		this.matricula = matricula;
		this.marca = marca;
		this.plazas = plazas;
		
		Vehiculo.cantidadInstancias++;
	}
	
	public static int getCantidadInstanciasStatic() {
		return cantidadInstancias;
	}
	public int getCantidadInstancias() {
		return cantidadInstancias;
	}
	public static void setCantidadInstancias(int cantidadInstancias) {
		Vehiculo.cantidadInstancias = cantidadInstancias;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public int getPlazas() {
		return plazas;
	}
	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}

	@Override
	public String toString() {
		return "Vehiculo [matricula=" + matricula + ", marca=" + marca + ", plazas=" + plazas + "]";
	}
	
	

}
