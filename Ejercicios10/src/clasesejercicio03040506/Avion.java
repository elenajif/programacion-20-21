package clasesejercicio03040506;

public class Avion extends Vehiculo{
	private int numMisiles;
	
	public Avion(String matricula, String marca, int plazas, int numMisiles) {
		super(matricula, marca, plazas);
		this.numMisiles=numMisiles;
	}

	public int getNumMisiles() {
		return numMisiles;
	}

	public void setNumMisiles(int numMisiles) {
		this.numMisiles = numMisiles;
	}

	@Override
	public String toString() {
		return super.toString()+"Avion [numMisiles=" + numMisiles + "]";
	}

}
