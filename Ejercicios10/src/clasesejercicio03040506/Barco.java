package clasesejercicio03040506;

public class Barco  extends Vehiculo{
	private String nombreCapitan;
	private boolean tieneVela;
	
	public Barco(String matricula, String marca, int plazas, String nombreCapitan, boolean tieneVela) {
		super(matricula, marca, plazas);
		this.nombreCapitan=nombreCapitan;
		this.tieneVela=tieneVela;
	}
	
	public String getNombreCapitan() {
		return nombreCapitan;
	}
	public void setNombreCapitan(String nombreCapitan) {
		this.nombreCapitan = nombreCapitan;
	}
	public boolean isTieneVela() {
		return tieneVela;
	}
	public void setTieneVela(boolean tieneVela) {
		this.tieneVela = tieneVela;
	}
	@Override
	public String toString() {
		return "Barco [nombreCapitan=" + nombreCapitan + ", tieneVela=" + tieneVela 
				+ ", matricula=" + matricula
				+ ", marca=" + marca + ", plazas=" + plazas + "]";
	}
	
	
}
