package ejercicios;

import java.util.ArrayList;

import clasesejercicio03040506.Avion;
import clasesejercicio03040506.Barco;
import clasesejercicio03040506.Coche;
import clasesejercicio03040506.Vehiculo;
import clasesexcepciones.ElementNotFoundException;
import clasesexcepciones.ElementNotFoundRuntimeException;

public class Ejercicio08 {

	public static void main(String[] args) throws ElementNotFoundRuntimeException {
		// sobreescribir clases Exception

		// creamos ArrayList
		ArrayList<Vehiculo> vehiculos = new ArrayList<>();
		// a�adimos distintos tipo de objetos
		vehiculos.add(new Coche("FDC-123", "Audi", 5, 123.5));
		vehiculos.add(new Coche("GDW-314", "Opel", 2, 45));
		vehiculos.add(new Avion("ESP-987", "Boeing", 2, 2));
		vehiculos.add(new Avion("FRA-314", "Airbus", 120, 0));
		vehiculos.add(new Barco("12345", "Zodiac", 7, "Willy", false));
		vehiculos.add(new Barco("12121", "Legend", 3, "Francisco", true));
		vehiculos.add(new Vehiculo("GFTRR", "Honda", 2));
		vehiculos.add(new Vehiculo("JUYGR", "Yamaha", 5));

		// listamos de 1� forma
		System.out.println("Listamos con el toString");
		for (Vehiculo vehiculo : vehiculos) {
			System.out.println(vehiculo);
		}

		// listamos de la segunda forma
		// instaceof -> comparar de que clase es
		System.out.println();
		System.out.println("Mostramos metodos concretos de cada clase");
		for (Vehiculo vehiculo : vehiculos) {
			if (vehiculo instanceof Coche) {
				Coche miCoche = (Coche) vehiculo;
				System.out.println(miCoche.getMatricula());
				System.out.println(miCoche.getMarca());
				System.out.println(miCoche.getPlazas());
				System.out.println(miCoche.getKm());
			} else if (vehiculo instanceof Barco) {
				Barco miBarco = (Barco) vehiculo;
				System.out.println(miBarco.getMatricula());
				System.out.println(miBarco.getMarca());
				System.out.println(miBarco.getPlazas());
				System.out.println(miBarco.getNombreCapitan());
				System.out.println(miBarco.isTieneVela());
			} else if (vehiculo instanceof Avion) {
				Avion miAvion = (Avion) vehiculo;
				System.out.println(miAvion.getMatricula());
				System.out.println(miAvion.getMarca());
				System.out.println(miAvion.getPlazas());
				System.out.println(miAvion.getNumMisiles());
			} else {
				System.out.println(vehiculo.getMatricula());
				System.out.println(vehiculo.getMarca());
				System.out.println(vehiculo.getPlazas());
			}
		}

		System.out.println();
		System.out.println("Buscamos un vehiculo");
		Vehiculo vehiculo1 = buscarVehiculoV2("fgdfgdfgdfgdf34", vehiculos);
		System.out.println(vehiculo1);

		try {
			System.out.println("Buscamos otro vehiculo");
			System.out.println("Controlar la excepci�n");
			Vehiculo vehiculo2 = buscarVehiculo("fgdfgdfgdfgdf34", vehiculos);
			System.out.println(vehiculo2);
		} catch (ElementNotFoundException e) {
			e.printStackTrace();
		}
	}

	static Vehiculo buscarVehiculo(String matricula, ArrayList<Vehiculo> lista) throws ElementNotFoundException {
		//con throws estamos lanzando la excepcion 
		//nosotros decidimos donde controlarla (en este caso en el main)
		for (Vehiculo vehiculo : lista) {
			if (vehiculo.getMatricula().equals(matricula)) {
				return vehiculo;
			}
		}
		//con esta linea estamos llamando a la creacion de un objeto de Exception nuevo
		throw new ElementNotFoundException(matricula);
	}
	
	static Vehiculo buscarVehiculoV2(String matricula, ArrayList<Vehiculo> lista) throws ElementNotFoundRuntimeException {
		//con throws estamos lanzando la excepcion 
		//nosotros decidimos donde controlarla (no la vamos a controlar)
		for (Vehiculo vehiculo : lista) {
			if (vehiculo.getMatricula().equals(matricula)) {
				return vehiculo;
			}
		}
		//con esta linea estamos llamando a la creacion de un objeto de Exception nuevo
		throw new ElementNotFoundRuntimeException(matricula);
	}

}
