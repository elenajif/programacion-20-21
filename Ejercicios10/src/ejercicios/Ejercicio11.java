package ejercicios;

import clasesejercicio11.ListaPersonas;
import clasesejercicio11.Persona;

public class Ejercicio11 {

	public static void main(String[] args) {
		// crear una listaPersonas
		ListaPersonas miLista = new ListaPersonas();
		
		//a�adir
		miLista.add(null);
		miLista.add(1,new Persona("Fernando","1234"));
		miLista.add(1,new Persona("Fran","1234"));
		miLista.add(1,new Persona(null, null));
		miLista.add(1,new Persona("Silvia","2354"));
				
		//recorrer con un foreach
		for (Persona persona: miLista) {
			System.out.println(persona);
		}
		
		//mostrar
		System.out.println("");
		System.out.println("toString del ArrayList");
		System.out.println(miLista);

	}

}
