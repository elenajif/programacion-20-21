package ejercicios;

import clasesejercicio0102.SubClase;
import clasesejercicio0102.SuperClase;

public class Ejercicio01 {

	public static void main(String[] args) {
		SuperClase padre = new SuperClase("Pepe");
		SubClase hijo = new SubClase("Maria",2);
		
		System.out.println(padre);
		System.out.println(hijo);
		
	}

}
