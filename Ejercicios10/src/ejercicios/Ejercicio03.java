package ejercicios;

import clasesejercicio03040506.Avion;
import clasesejercicio03040506.Barco;
import clasesejercicio03040506.Coche;

public class Ejercicio03 {
	public static void main(String[] args) {
		Barco barco = new Barco("1234","Zodiac",7,"Juanjo",false);
		System.out.println("BARCO");
		System.out.println(barco);
		System.out.println(barco.getNombreCapitan());
		System.out.println(barco.isTieneVela());
		
		Avion avion = new Avion("1235F","Boeing",2,2);
		System.out.println("AVION");
		System.out.println(avion);
		System.out.println(avion.getNumMisiles());
		
		Coche coche = new Coche("FDS-1234","Peugeot",5,120.7);
		System.out.println("COCHE");
		System.out.println(coche);
		System.out.println(coche.getKm());

	}

}
