package ejercicios;

import clasesejercicio0709.Director;
import clasesejercicio0709.Empleado;
import clasesejercicio0709.Oficial;
import clasesejercicio0709.Operario;
import clasesejercicio0709.Tecnico;

public class Ejercicio07 {

	public static void main(String[] args) {
		Empleado empleado = new Empleado("Raquel");
		Operario operario = new Operario("Juan");
		Director director = new Director("Alberto");
		Oficial oficial = new Oficial("Jose");
		Tecnico tecnico = new Tecnico("Laura");
		
		System.out.println(empleado);
		System.out.println(operario);
		System.out.println(director);
		System.out.println(oficial);
		System.out.println(tecnico);

	}

}
