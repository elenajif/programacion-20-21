package ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

import clasesejercicio0709.Director;
import clasesejercicio0709.Empleado;
import clasesejercicio0709.Oficial;
import clasesejercicio0709.Operario;
import clasesejercicio0709.Tecnico;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Empleado> lista = new ArrayList<Empleado>();
		int opcion;

		do {
			System.out.println("Selecciona el tipo de empleado");
			System.out.println("1.- Empleado");
			System.out.println("2.- Directivo");
			System.out.println("3.- Operario");
			System.out.println("4.- Tecnico");
			System.out.println("5.- Oficial");
			System.out.println("6.- Salir");

			opcion = input.nextInt();
			input.nextLine();
			String nombre = null;
			if (opcion != 6) {
				System.out.println("Introduce nombre");
				nombre = input.nextLine();
			}

			switch (opcion) {

			case 1:
				lista.add(new Empleado(nombre));
				break;
			case 2:
				lista.add(new Director(nombre));
				break;
			case 3:
				lista.add(new Operario(nombre));
				break;
			case 4:
				lista.add(new Tecnico(nombre));
				break;
			case 5:
				lista.add(new Oficial(nombre));
				break;
			case 6:
				System.out.println("Salimos");
				break;
			default:
				System.out.println("Opci�n no contemplada");
			}

		} while (opcion != 6);
		
		System.out.println("Listamos");
		for (Empleado empleado: lista) {
			System.out.println(empleado);
		}

		input.close();

	}

}
