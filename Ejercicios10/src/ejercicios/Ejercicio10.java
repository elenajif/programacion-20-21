package ejercicios;

import java.time.LocalTime;
import java.util.ArrayList;

import clasesejercicio03040506.Avion;
import clasesejercicio03040506.Coche;
import clasesejercicio0709.Empleado;


public class Ejercicio10 {

	public static void main(String[] args) {
		ArrayList<Object> lista = new ArrayList<>();
		
		System.out.println("A�ado un String");
		lista.add("fdsfsdf");
		System.out.println("A�ado un LocalTime");
		lista.add(LocalTime.now());
		System.out.println("A�ado un coche");
		Coche miCoche = new Coche("DFG-1111","Peugeot",5,120.7);
		lista.add(miCoche);
		System.out.println("A�ado un avi�n");
		Avion miAvion = new Avion("1231G","Boeing",2,2);
		lista.add(miAvion);
		System.out.println("A�ado un empleado");
		Empleado miEmpleado = new Empleado("Raquel");
		lista.add(miEmpleado);
		System.out.println("A�ado un booleano");
		lista.add(false);
		
		System.out.println("");
		System.out.println("Recorro la lista y  muestro");
		for (Object objeto: lista) {
			System.out.println(objeto);
		}
		
		System.out.println("");
		System.out.println("Matricula del coche "+miCoche.getMatricula());
		System.out.println("Marca del avion "+miAvion.getMarca());
		System.out.println("Nombre del empleado "+miEmpleado.getNombre());
		

	}

}
