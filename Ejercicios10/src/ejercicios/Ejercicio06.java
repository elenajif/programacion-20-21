package ejercicios;

import clasesejercicio03040506.Avion;
import clasesejercicio03040506.Barco;
import clasesejercicio03040506.Coche;
import clasesejercicio03040506.Vehiculo;

public class Ejercicio06 {

	public static void main(String[] args) {
		System.out.println("Creamos barco y mostramos datos");
		Barco barco = new Barco("1234","Zodiac",7,"Juanjo",false);
		System.out.println("BARCO");
		System.out.println(barco);
		
		System.out.println("Accedemos a cantidad instancias static partiendo de Vehiculo");
		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println("Accedemos a cantidad instancias no static partiendo de objeto barco");
		System.out.println(barco.getCantidadInstancias());
		
		System.out.println("Creamos avion y mostramos datos");
		Avion avion = new Avion("1235F","Boeing",2,2);
		System.out.println("AVION");
		System.out.println(avion);
		
		System.out.println("Accedemos a cantidad instancias static partiendo de Vehiculo");
		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println("Accedemos a cantidad instancias no static partiendo de objeto avion");
		System.out.println(avion.getCantidadInstancias());
		
		System.out.println("Creamos coche y mostramos datos");
		Coche coche = new Coche("FDS-1234","Peugeot",5,120.7);
		System.out.println("COCHE");
		System.out.println(coche);
		
		System.out.println("Accedemos a cantidad instancias static partiendo de Vehiculo");
		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println("Accedemos a cantidad instancias no static partiendo de objeto coche");
		System.out.println(coche.getCantidadInstancias());
		
		System.out.println("Creamos vehiculo y mostramos datos");
		Vehiculo vehiculo = new Vehiculo("2346","honda",3);
		System.out.println("VEHICULO");
		System.out.println(vehiculo);

		System.out.println("Accedemos a cantidad instancias static partiendo de Vehiculo");
		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println("Accedemos a cantidad instancias no static partiendo de objeto vehiculo");
		System.out.println(vehiculo.getCantidadInstancias());
	}

}
