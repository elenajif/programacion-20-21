package clases;

import java.time.LocalDate;

//clase que actua de almacen de pedidos (albaranes)

public class TallerVehiculos {

	// atributo vector de albaranes
	Albaran[] albaranes;

	// constructor
	public TallerVehiculos(int maxAlbaranes) {
		this.albaranes = new Albaran[maxAlbaranes];
	}

	// metodo para dar de alta un albaran
	public void altaAlbaran(String codAlbaran, double precio, String codVehiculo) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] == null) {
				albaranes[i] = new Albaran(codAlbaran);
				albaranes[i].setPrecio(precio);
				albaranes[i].setCodVehiculo(codVehiculo);
				albaranes[i].setFecha(LocalDate.now());
				break;
			}
		}
	}

	// metodo buscarAlbaran
	public Albaran buscarAlbaran(String codAlbaran) {
		for (int i=0;i<albaranes.length;i++) {
			if (albaranes[i]!=null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					return albaranes[i];
				}
			}
		}
		return null;
	}

	// metodo eliminarAlbaran
	public void eliminarAlbaran(String codAlbaran) {
		for (int i=0;i<albaranes.length;i++) {
			if (albaranes[i]!=null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					albaranes[i]=null;
				}
			}
		}
	}

	// metodo listarAlbaranes
	public void listarAlbaranes() {
			for (int i=0;i<albaranes.length;i++) {
				if (albaranes[i]!=null) {
					System.out.println(albaranes[i]);
				}
			}
			
		}
	
	//metodo cambiar vehiculo de un albaran determinado
	public void cambiarAlbaran(String codAlbaran, String codVehiculo2) {
		for (int i=0;i<albaranes.length;i++) {
			if (albaranes[i]!=null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					albaranes[i].setCodVehiculo(codVehiculo2);
				}
			}
		}
	}
	
	//metodo listar albaranes por codVehiculo
	public void listarAlbaranesPorVehiculo(String codVehiculo) {
		for (int i=0;i<albaranes.length;i++) {
			if (albaranes[i]!=null) {
				if (albaranes[i].getCodVehiculo().equals(codVehiculo)) {
					System.out.println(albaranes[i]);
				}
			}
		}
	}

}
