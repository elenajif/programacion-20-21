package programa;

import clases.TallerVehiculos;

public class Programa {

	public static void main(String[] args) {
		System.out.println("1.- Voy a crear una instancia de Vehiculo llamada taller con 4 albaranes");
		//declaro maxVehiculos
		int maxVehiculos=4;
		TallerVehiculos taller = new TallerVehiculos(maxVehiculos);
		System.out.println("Instancia creada");
		
		System.out.println("2.- Doy de alta cuatro albaranes");
		System.out.println("Doy de alta A1-A2-A3-A4");
		taller.altaAlbaran("A1", 20, "V1");
		taller.altaAlbaran("A2", 23.10, "V1");
		taller.altaAlbaran("A3", 25.95, "V1");
		taller.altaAlbaran("A4", 16.99, "V2");
		
		System.out.println("3.- Listar albaranes");
		taller.listarAlbaranes();
		
		System.out.println("4.- Buscar un albar�n por su c�digo y mostrar datos por pantalla");
		System.out.println("Buscamos albar�n A2");
		System.out.println(taller.buscarAlbaran("A2"));
		
		System.out.println("5.- Eliminar un albar�n diferente al anterior");
		System.out.println("Eliminamos el albar�n A3");
		taller.eliminarAlbaran("A3");
		taller.listarAlbaranes();
		
		System.out.println("Almacenar un nuevo albar�n");
		System.out.println("Almacenamos A5");
		taller.altaAlbaran("A5", 27.33, "V3");
		taller.listarAlbaranes();
		
		System.out.println("7.- Modificar el codVehiculo de un albar�n");
		System.out.println("Vamos a modificar el albaran A1 de V1 a V2");
		taller.cambiarAlbaran("A1", "V2");
		taller.listarAlbaranes();
		
		System.out.println("8.- Lista solo los albaranes de un vehiculo");
		System.out.println("Vamos a listar los albaranes del vehiculo V2");
		taller.listarAlbaranesPorVehiculo("V2");

	}

}
