package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String[] frases = Metodos01.rellenarVector();
		int opcion;

		do {
			System.out.println("1 – Cantidad palabras ");
			System.out.println("2 – Invertir vector ");
			System.out.println("3.- Salir");

			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				Metodos01.cantidadPalabras(frases);
				break;
			case 2:
				Metodos01.invertirVector(frases);
				break;
			case 3:
				System.out.println("Programa finalizado");
				System.exit(0);
				break;
			default:
				System.out.println("Error de introducción de datos");
			}
		} while (opcion != 4);
		input.close();

	}
}