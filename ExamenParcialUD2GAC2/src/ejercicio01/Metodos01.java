package ejercicio01;

import java.util.Scanner;

public class Metodos01 {
	static Scanner input = new Scanner(System.in);

	public static String[] rellenarVector() {
		System.out.println("Rellenar vector: ");
		String frases[] = new String[5];
		for (int i = 0; i < frases.length; i++) {
			System.out.println("Introduce la frase " + i);
			frases[i] = input.nextLine();
		}
		return frases;
	}

	public static void cantidadPalabras(String[] frases) {
		System.out.println("Cantidad palabras: ");
		for (int i = 0; i < frases.length; i++) {
			System.out.println("Frase " + i);
			System.out.println(frases[i]);
			System.out.println("La cantidad de palabras de la frase " + i + " es " + frases[i].split(" ").length);
		}

	}

	public static void invertirVector(String[] frases) {
		System.out.println("Invertir vector: ");
		String[] frasesInvertido = new String[frases.length];

		for (int i = (frases.length - 1), j = 0; i >= 0; i--, j++) {
			frasesInvertido[j] = frases[i];
			System.out.println(frasesInvertido[j]);
		}
	}

}
