package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		double[] vector = Metodos02.rellenarVector();
		int opcion;

		do {
			System.out.println("1 � Visualizar temperaturas m�s altas");
			System.out.println("2 � Ordenar vector mayor a menor");
			System.out.println("3 � Visualizar m�nimo impar  ");
			System.out.println("4.- Salir");

			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				Metodos02.visualizarTemperaturasAltas(vector);
				break;
			case 2:
				Metodos02.ordenarVector(vector);
				break;
			case 3:
				Metodos02.visualizarMinimoImpar(vector);
				break;
			case 4:
				System.out.println("Programa finalizado");
				System.exit(0);
				break;
			default:
				System.out.println("Error de introducci�n de datos");
			}
		} while (opcion != 4);
		input.close();

	}
}