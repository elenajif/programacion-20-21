package ejercicio02;

import java.util.Scanner;

public class Metodos02 {
	static Scanner input = new Scanner(System.in);

	public static double[] rellenarVector() {
		System.out.println("Rellenar vector: ");
		double vector[] = new double[5];
		for (int i = 0; i < vector.length; i++) {
			System.out.println("Introduce una temperatura");
			vector[i] = input.nextDouble();
		}
		return vector;
	}

	public static void visualizarTemperaturasAltas(double[] vector) {
		System.out.println("Visualizar temperaturas altas: ");
		for (int i = 0; i < vector.length; i++) {
			if (vector[i]>= 35.0) {
				System.out.print(vector[i]+" ");
			}
		}
		System.out.println("");
	}

	public static void ordenarVector(double[] vector) {

		System.out.println("Vector sin ordenar ");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector[i] + " ");
		}
		System.out.println("");

		for (int i = 0; i < vector.length - 1; i++) {
			for (int j = i + 1; j < vector.length; j++) {
				if (vector[j] > vector[i]) {
					double aux = vector[i];
					vector[i] = vector[j];
					vector[j] = aux;
				}
			}
		}

		System.out.println("Vector ordenado");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector[i] + " ");
		}
		System.out.println("");

	}

	public static void visualizarMinimoImpar(double[] vector) {
		System.out.println("M�nimo impar: ");
		int vector1[] = new int[10];
		for (int i = 0; i < vector.length; i++) {
			vector1[i] = (int) Math.round(vector[i]);
		}
		System.out.println("Muestro vector enteros ");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector1[i] + " ");
		}
		System.out.println(" ");
		int minimo = vector1[0];
		int posicion = 0;
		for (int i = 0; i < vector1.length; i++) {
			if (vector1[i] % 2 != 0) {
				if (vector1[i] < minimo) {
					minimo = vector1[i];
					posicion = i;
				}
			}
		}
		System.out.println("El m�nimo impar es: " + minimo);
		System.out.println("La posici�n es: " + posicion);
	}

}
