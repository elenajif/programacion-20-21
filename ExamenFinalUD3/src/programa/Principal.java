package programa;

import java.sql.SQLException;

import clases.Colegio;

public class Principal {

	public static void main(String[] args) {

		Colegio montessori = new Colegio();
		System.out.println("Creamos profesores, tutores y cursos");
		montessori.altaTutor("0004", "Elena", "DAM", 2);
		System.out.println("Tutor creado");
		montessori.altaProfesor("0005", "Elena", "1972-01-01", "Informatica");
		System.out.println("Profesor creado");
		System.out.println("Creamos cursos");
		montessori.altaCurso("C1", "1 DAM", "0004");
		System.out.println("Curso creado");
		System.out.println("");
		System.out.println("Mostramos profesores");
		montessori.listarProfesores();
		System.out.println("Mostramos tutores");
		montessori.listarTutores();	

		System.out.println("");
		System.out.println("Conectamos BBDD y guardamos tutores DAM");
		montessori.conectarBBDD();
		montessori.guardarTutoresBBDD("DAM");
		System.out.println("Tutores guardados en BBDD");
		System.out.println("Cargamos profesores informatica");
		try {
			montessori.cargarProfesoresBBDD("Informática");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
