package clases;

import java.time.LocalDate;

public class Profesor extends Personal{

	private static final long serialVersionUID = 3915527913264256570L;
	private LocalDate fechaNacimiento;
	private String especialidad;
	
	public Profesor(String dni, String nombre, String fechaNacimiento, String especialidad) {
		super(dni, nombre);
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
		this.especialidad = especialidad;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	@Override
	public int compareTo(Personal o) {
		return getDni().compareTo(o.getDni());
	}

	@Override
	public String toString() {
		return super.toString() + fechaNacimiento + especialidad;
	}
	
	

}
