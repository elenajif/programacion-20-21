package clases;

import java.io.Serializable;
import java.util.ArrayList;

public class Curso implements Comparable<Curso>, Serializable{


	private static final long serialVersionUID = 3852925526538813766L;
	private String codigo;
	private String nombre;
	private Tutor tutor;
	private ArrayList<Profesor> listaProfesores;
	
	public Curso(String codigo, String nombre, Tutor tutor) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.tutor = tutor;
		this.listaProfesores = new ArrayList<Profesor>();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Tutor getTutor() {
		return tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

	public ArrayList<Profesor> getListaProfesores() {
		return listaProfesores;
	}

	public void setListaProfesores(ArrayList<Profesor> listaProfesores) {
		this.listaProfesores = listaProfesores;
	}

	@Override
	public int compareTo(Curso o) {
		return getCodigo().compareTo(o.getCodigo());
	}
	
	
	
}
