package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class Colegio {

	private ArrayList<Curso> listaCursos;
	private ArrayList<Personal> listaPersonal;
	private Connection conexion;

	public Colegio() {
		this.listaCursos = new ArrayList<Curso>();
		this.listaPersonal = new ArrayList<Personal>();
	}

	public ArrayList<Curso> getListaCursos() {
		return listaCursos;
	}

	public void setListaCursos(ArrayList<Curso> listaCursos) {
		this.listaCursos = listaCursos;
	}

	public ArrayList<Personal> getListaPersonal() {
		return listaPersonal;
	}

	public void setListaPersonal(ArrayList<Personal> listaPersonal) {
		this.listaPersonal = listaPersonal;
	}

	// ALTA TUTOR
	public void altaTutor(String dni, String nombre, String ciclo, int curso) {
		listaPersonal.add(new Tutor(dni, nombre, ciclo, Integer.valueOf(curso)));
		Collections.sort(listaPersonal);
	}

	// ALTA PROFESOR
	public void altaProfesor(String dni, String nombre, String fechaNacimiento, String especialidad) {
		listaPersonal.add(new Profesor(dni, nombre, fechaNacimiento, especialidad));
		Collections.sort(listaPersonal);
	}

	// ALTA CURSO
	public void altaCurso(String codigo, String nombre, String dni) {
		if (compruebaCurso(codigo) == false && compruebaTutor(dni)) {
			listaCursos.add(new Curso(codigo, nombre, devuelveTutor(dni)));
			Collections.sort(listaCursos);
		} else {
			System.out.println("Ha ocurrido un error");
		}
	}

	// REGISTAR CURSOS EN COLEGIO
	public void registrarProfesoresCurso(String codigoCurso, String dniProfesor) {
		if (compruebaCurso(codigoCurso) && compruebaProfesor(dniProfesor)) {
			devuelveCurso(codigoCurso).getListaProfesores().add(devuelveProfesor(dniProfesor));
		} else {
			System.out.println("Ha ocurrido un error");
		}
	}

	// LISTAR TUTORES
	public void listarTutores() {
		for (Personal personal : listaPersonal) {
			if (personal instanceof Tutor) {
				System.out.println(personal);
			}
		}
	}

	// LISTAR PROFESORES
	public void listarProfesores() {
		for (Personal personal : listaPersonal) {
			if (personal instanceof Profesor) {
				System.out.println(personal);
			}
		}
	}

	// GUARDAR DATOS
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));

			escritor.writeObject(listaCursos);
			escritor.writeObject(listaPersonal);

			escritor.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// CARGAR DATOS
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream cargar = new ObjectInputStream(new FileInputStream(new File("src/datos.dat")));

			listaCursos = (ArrayList<Curso>) cargar.readObject();
			listaPersonal = (ArrayList<Personal>) cargar.readObject();

			cargar.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// CONECTAR BASE DE DATOS
	public void conectarBBDD() {
		String servidor = "jdbc:mysql://localhost:3306/colegio3ev";
		try {
			conexion = DriverManager.getConnection(servidor, "root", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// GUARDAR TUTORES
	public void guardarTutoresBBDD(String ciclo) {
		String query = "INSERT INTO tutores(dni, nombre, ciclo,curso) VALUES(?, ?, ?, ?)";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);

			for (Personal personal : listaPersonal) {
				if (personal instanceof Tutor && ((Tutor) personal).getCiclo().equals(ciclo)) {
					sentencia.setString(1, personal.getDni());
					sentencia.setString(2, personal.getNombre());
					sentencia.setString(3, ((Tutor) personal).getCiclo());
					sentencia.setInt(4, ((Tutor) personal).getCurso());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// CARGAR PROFESORES
	public void cargarProfesoresBBDD(String especialidad) throws SQLException {
		String query = "SELECT * FROM profesores";
		PreparedStatement sentencia = conexion.prepareStatement(query);
		ResultSet resultado = sentencia.executeQuery();

		while (resultado.next()) {
			if (resultado.getString(5).equals(especialidad)) {
				listaPersonal.add(new Profesor(resultado.getString(2), resultado.getString(3), resultado.getString(4),
						resultado.getString(5)));
			}
		}
	}

	// COMPROBAR CURSO
	public boolean compruebaCurso(String codigo) {
		boolean existe = false;
		for (Curso al : listaCursos) {
			if (al.getCodigo().equals(codigo)) {
				existe = true;
			}
		}
		return existe;
	}

	// COMPROBAR TUTOR
	public boolean compruebaTutor(String dni) {
		boolean existe = false;
		for (Personal personal : listaPersonal) {
			if (personal instanceof Tutor) {
				if (personal.getDni().equals(dni)) {
					existe = true;
				}
			}
		}
		return existe;
	}

	// COMPROBAR PROFESOR
	public boolean compruebaProfesor(String dni) {
		boolean existe = false;
		for (Personal personal : listaPersonal) {
			if (personal instanceof Profesor) {
				if (personal.getDni().equals(dni)) {
					existe = true;
				}
			}
		}
		return existe;
	}

	// DEVUELVE TUTOR
	public Tutor devuelveTutor(String dni) {
		for (Personal personal : listaPersonal) {
			if (personal instanceof Tutor) {
				if (personal.getDni().equals(dni)) {
					return (Tutor) personal;
				}
			}
		}
		return null;
	}

	// DEVUELVE PROFESOR
	public Profesor devuelveProfesor(String dni) {
		for (Personal personal : listaPersonal) {
			if (personal instanceof Profesor) {
				if (personal.getDni().equals(dni)) {
					return (Profesor) personal;
				}
			}
		}
		return null;
	}

	// DEVUELVE CURSO
	public Curso devuelveCurso(String codigo) {
		for (Curso al : listaCursos) {
			if (al.getCodigo().equals(codigo)) {
				return al;
			}
		}
		return null;
	}
}
