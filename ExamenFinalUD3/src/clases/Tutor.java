package clases;

public class Tutor extends Personal {


	private static final long serialVersionUID = -3287096738226583558L;
	private String ciclo;
	private int curso;
	
	public Tutor(String dni, String nombre, String ciclo, int curso) {
		super(dni, nombre);
		this.ciclo = ciclo;
		this.curso = curso;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	public int getCurso() {
		return curso;
	}

	public void setCurso(int curso) {
		this.curso = curso;
	}

	@Override
	public int compareTo(Personal arg0) {
		return getDni().compareTo(arg0.getDni());
	}

	@Override
	public String toString() {
		return super.toString() + ciclo + curso;
	}

	
	
}
