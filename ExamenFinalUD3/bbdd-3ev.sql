CREATE DATABASE colegio3ev;
USE colegio3ev;

CREATE TABLE tutores(
id INT AUTO_INCREMENT PRIMARY KEY,
dni VARCHAR(100),
nombre VARCHAR(100),
ciclo VARCHAR(100),
curso int
);

CREATE TABLE profesores(
id INT AUTO_INCREMENT PRIMARY KEY,
dni VARCHAR(100),
nombre VARCHAR(100),
fecha_nacimiento DATE,
especialidad VARCHAR(100)
);

INSERT INTO tutores (dni, nombre, ciclo, curso)
VALUES ("0001", "Ana Belen", "DAW", 1),("0002", "Vicente", "DAW", 2),("0003", "Monica", "DAM", 1);

INSERTO INTO profesores(dni, nombre, fecha_nacimiento, especialidad)
VALUES ("0006", "Monica", "1979-09-09","Informática"), ("0007", "Pedro", "1975-05-05","AFI");
