package menuficheros;

import java.io.IOException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) throws IOException {
		int op;
		Scanner in = new Scanner(System.in);
		//1.- pido el nombre del archivo
		System.out.println("Introduce el nombre del archivo");
		String nombreArchivo=in.nextLine();
		//2.- creare un archivo usando OperacionesConArchivos
		OperacionesConArchivos miArchivo= new OperacionesConArchivos(nombreArchivo);
		
		do {
			//llamo al menu y devuelvo un entero
			op=menu();
			switch(op) {
			case 1:
				miArchivo.crearArchivo();
				break;
			case 2: 
				miArchivo.visualizarArchivo();
				break;
			case 3:
				miArchivo.convertirMayuscMinus(1);
				break;
			case 4:
				miArchivo.convertirMayuscMinus(2);
			case 5:
				int cantidadLineas=miArchivo.numeroLineas();
				System.out.println("Numero de lineas "+cantidadLineas);
				break;
			case 6:
				miArchivo.buscarPalabra();
				break;
			}
		} while  (op<7);

	}

	public static int menu() {
		Scanner in = new Scanner(System.in);
		int opcion = 0;
		boolean error = false;
		do {
			try {
				System.out.println("MENU");
				System.out.println("1.- Crear archivo desde teclado (* para salir)");
				System.out.println("2.- Visualizar archivo");
				System.out.println("3.- Convertir a may�sculas");
				System.out.println("4.- Convertir a min�sculas");
				System.out.println("5.- N�mero de lineas");
				System.out.println("6.- Buscar palabra en fichero");
				System.out.println("7.- Salir");
				System.out.println(" ");
				System.out.println("Elegir opcion");
				opcion = in.nextInt();
				if (opcion < 1 || opcion > 7) {
					System.out.println("Error, el valor debe estar entre 1 y 7");
					error = true;
				} else {
					error = false;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error, debes introducir un n�mero");
				in.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Error de acceso a la informaci�n del teclado");
				System.out.println("Comprueba tu conexi�n al teclado");
				System.exit(0);
			}
		} while (error);
		return opcion;
	}

}
