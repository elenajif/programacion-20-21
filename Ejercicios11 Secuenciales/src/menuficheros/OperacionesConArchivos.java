package menuficheros;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class OperacionesConArchivos {

	// campo archivo
	private String archivo;

	// constructor con archivo
	public OperacionesConArchivos(String archivo) {
		this.archivo = archivo;
	}

	public void crearArchivo() throws IOException {
		System.out.println("CREAR ARCHIVO");
		// 1.- abro para lectura y entrada de teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String linea;
		// 2.- abro para escritura con el buffer de lectura
		PrintWriter fuente = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Creaci�n de un archivo \n" + "Introducir lineas (* para acabar)");
		// 3.- recorro linea a linea hasta *
		linea = in.readLine();
		while (!linea.equalsIgnoreCase("*")) {
			fuente.println(linea);
			linea = in.readLine();
		}
		System.out.println("archivo creado");
		// 4.- cerrar el archivo
		fuente.close();

	}

	public void visualizarArchivo() throws IOException {
		String linea;
		System.out.println("VISUALIZAR ARCHIVO " + this.archivo);
		// 1.- abro para lectura
		BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
		// 2.- recorro linea a linea hasta null
		linea = fuente.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = fuente.readLine();
		}
		// 3.- cierro el archivo
		fuente.close();
	}

	public void convertirMayuscMinus(int opcionElegida) {
		String linea;
		System.out.println("CONVERTIR MAYUSCULAS/MINUSCULAS");
		if (opcionElegida == 1) {
			System.out.println("May�sculas");
		} else {
			System.out.println("Min�sculas");
		}
		// declaro ArrayList
		ArrayList<String> v = new ArrayList<String>();

		try {
			// 1.- conecto en modo lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- guardo las lineas en un vector
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			// 3.- cerrar archivo
			fuente.close();
			// 4.- leo del vector y lo paso al archivo a mayus/minus
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {
				if (opcionElegida == 1) {
					archivo.println(v.get(i).toUpperCase());
				} else {
					archivo.println(v.get(i).toLowerCase());
				}
			}
			// 5.- cierro el archivo
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	public int numeroLineas() {
		System.out.println("CONTAR LINEAS");
		String linea;
		int contadorLineas = 0;

		try {
			// 1.- abro para lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- recorro linea a linea hasta null y cuento
			linea = fuente.readLine();
			while (linea != null) {
				contadorLineas++;
				linea = fuente.readLine();
			}
			// 3.- cierro el archivo
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
		return contadorLineas;

	}

	public void buscarPalabra() {
		// pedimos la ruta y pedimos la palabra
		String valorABuscar;
		String linea;
		Scanner in = new Scanner(System.in);
		System.out.println("BUSCAR PALABRA EN EL VECTOR");
		System.out.println("Introduce la palabra a buscar");
		valorABuscar = in.nextLine();
		// declaramos el vector
		Vector<String> v = new Vector<String>();

		try {
			// 1.- abro para lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- leemos linea a linea y metemos las palabras en un vector
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
						System.out.println(palabra);
					} 
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			// 3.- buscamos la palabra en el vector
			int cp = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					cp++;
					System.out.println("valor encontrado -> " +valorABuscar);
				}
			}
			if (cp == 0) {
				System.out.println("La palabra no se encuentra en el archivo");
			} else {
				System.out.println("La palabra se encuentra en el archivo " + cp + " veces");
			}
			// 4.- cerramos el archivo
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
}
