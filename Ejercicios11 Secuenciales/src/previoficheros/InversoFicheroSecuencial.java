package previoficheros;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class InversoFicheroSecuencial {
	
	public static final String archivo="datos.txt";
	public static final String archivo2="inverso.txt";

	public static void main(String[] args) throws IOException {
		// 1.- Abrir el archivo para leer y vamos a crear un vector ArrayList
		BufferedReader origen = new BufferedReader(new FileReader(archivo));
		ArrayList<String> vector = new ArrayList<String>();
		
		// 2.- Leo el archivo y escribo en el vector
		String linea = "";
		linea=origen.readLine();
		while (linea !=null) {
			vector.add(linea);
			linea=origen.readLine();
		}
		
		// 3.- Cierro el archivo
		origen.close();
		
		// 4.- Abro el segundo archivo para escribir
		PrintWriter destino = new PrintWriter(new FileWriter(archivo2,false));
		
		// 5.- Leo el vector del reves y voy escribiendo
		for (int i=vector.size()-1;i>=0;i--) {
			destino.println(vector.get(i));
		}
		
		// 6.- Cierro el archivo
		destino.close();
		
		// 7.- Visualizo primer archivo
		System.out.println("***** ARCHIVO ORIGEN *******");
		visualizarArchivo("datos.txt");
		
		System.out.println("***** ARCHIVO DESTINO *******");
		visualizarArchivo("inverso.txt");

	}
	
	private static void visualizarArchivo(String nombreArchivo) {
		// 1 abro para lectura
		try {
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));

			// 2 recorro linea a linea hasta null
			String linea="";
			linea = origen.readLine();
			while (linea!=null) {
				System.out.println(linea);
				linea=origen.readLine();
			}

			// 3 cerrar el archivo
			origen.close();

		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Archivo innacesible");
			System.exit(0);
		}

	}

}
