package previoficheros;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EscrituraFicheroSecuencial2 {

	// cualquier flujo de informaci�n en java, necesita un stream (flujo)
	// flujo -> conexi�n entre el programa y el dispositivo de entrada o salida
	// tipos flujos -> caracteres (texto) y bytes (binarios)
	// clases caracteres -> Reader y Writer
	// clases bytes -> InputStream y OutputStream
	// tipos archivos -> texto (no enriquecido) y binarios (enriquecido)
	// archivos de texto -> texto plano visible con notepad, wordpad
	// archivos binarios -> formato enriquecido, audio
	// acceso -> secuencial y aleatorio
	// modo de acceso -> lectura (BufferedReader) escritura (PrintWriter)

	// "c:\\users\\datos.txt" -> \\
	// caracteres escapa de salto de linea -> \n

	// el proceso de lectura secuencial finalizara cuando llegue al final del
	// fichero
	// fichero (EOF)
	// esto ocurre cuando la variable (nombre) es null

	public static String archivo = "datos.txt";

	public static void main(String[] args) {

		try {
			// 1 abrir el fichero para escribir
			PrintWriter f = new PrintWriter(new FileWriter(archivo, true));
			// true -> al final
			// false -> al principio y escribe encima
			Scanner in = new Scanner(System.in);
			String linea = "";

			// 2 escribir en el archivo
			// leer linea de teclado
			// mientras (la linea no sea fin)
			//     escribir esa linea en el archivo
			//     leer linea de teclado
			System.out.println("Introduce el texto que quieres guardar");
			System.out.println("Escribiendo en un archivo, FIN para acabar");
			linea = in.nextLine();
			while (!linea.equalsIgnoreCase("fin")) {
				System.out.println();
				f.println(linea);
				linea=in.nextLine();
			}

			// 3 cerrar el archivo
			f.close();
			in.close();

		} catch (IOException e) {
			System.out.println("Error");
		}

	}

}
