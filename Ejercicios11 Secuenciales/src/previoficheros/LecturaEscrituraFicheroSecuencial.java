package previoficheros;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class LecturaEscrituraFicheroSecuencial {

	public static void main(String[] args) throws IOException {
		// 1 abrir el archivo para leer y el archivo para escribir
		// abro para lectura -> datos.txt
		BufferedReader origen = new BufferedReader(new FileReader("datos.txt"));
		// abro para escritura -> datosMayusculas.txt
		PrintWriter destino = new PrintWriter(new FileWriter("datosMayusculas.txt"));

		// 2 leo del primero y escribo en mayusculas en el segundo
		String linea = "";
		linea = origen.readLine();
		while (linea != null) {
			destino.println(linea.toUpperCase());
			linea = origen.readLine();
		}

		// 3 cerrar los archivos
		origen.close();
		destino.close();

		// 4 visualizar el archivo origen
		System.out.println("***** ARCHIVO ORIGEN *******");
		visualizarArchivo("datos.txt");

		// 5 visualizar el archivo destino
		System.out.println("***** ARCHIVO DESTINO *******");
		visualizarArchivo("datosMayusculas.txt");
	}

	private static void visualizarArchivo(String nombreArchivo) {
		// 1 abro para lectura
		try {
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));

			// 2 recorro linea a linea hasta null
			String linea="";
			linea = origen.readLine();
			while (linea!=null) {
				System.out.println(linea);
				linea=origen.readLine();
			}

			// 3 cerrar el archivo
			origen.close();

		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Archivo innacesible");
			System.exit(0);
		}

	}

}
