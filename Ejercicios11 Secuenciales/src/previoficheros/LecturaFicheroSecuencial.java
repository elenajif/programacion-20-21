package previoficheros;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LecturaFicheroSecuencial {

	// cualquier flujo de informaci�n en java, necesita un stream (flujo)
	// flujo -> conexi�n entre el programa y el dispositivo de entrada o salida
	// tipos flujos -> caracteres (texto) y bytes (binarios)
	// clases caracteres -> Reader y Writer
	// clases bytes -> InputStream y OutputStream
	// tipos archivos -> texto (no enriquecido) y binarios (enriquecido)
	// archivos de texto -> texto plano visible con notepad, wordpad
	// archivos binarios -> formato enriquecido, audio
	// acceso -> secuencial y aleatorio
	// modo de acceso -> lectura (BufferedReader) escritura (PrintWriter)

	// "c:\\users\\datos.txt" -> \\
	// caracteres escapa de salto de linea -> \n

	// el proceso de lectura secuencial finalizara cuando llegue al final del
	// fichero
	// fichero (EOF)
	// esto ocurre cuando la variable (nombre) es null

	public static void main(String[] args) {
		try {
			// 1 crear el puntero al archivo -> objeto (BufferedReader y FileReader)
			BufferedReader f = new BufferedReader(new FileReader("datos.txt"));
			// 2 recorrer el archivo linea a linea (comprobar con !=null)
			String nombre = "";
			nombre = f.readLine();
			while (nombre != null) {
				System.out.println(nombre);
				nombre = f.readLine();
			}
			// 3 cerrar el archivo
			f.close();

		} catch (FileNotFoundException e) {
			System.out.println("Error, el archivo no existe");
		} catch (IOException e) {
			System.out.println("Error, el archivo no es accesible");
		}
	}

}
