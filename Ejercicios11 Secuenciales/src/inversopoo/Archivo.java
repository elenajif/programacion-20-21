package inversopoo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Archivo {
	// atributos nombreArchivo
	String nombreArchivo;

	// constructor sin parametros
	public Archivo() {
		this.nombreArchivo = "";
	}

	// constructor con parametros
	public Archivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	// metodo static visualizarArchivo(nombreArchivo)
	public static void visualizarArchivo(String nombreArchivo) {
		try {
			// 1.- abro para lectura
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));

			// 2.- recorro linea a linea hasta null
			String linea="";
			linea=origen.readLine();
			while (linea!=null) {
				System.out.println(linea);
				linea=origen.readLine();
			}
			
			// 3.- cierro archivo
			origen.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Archivo inaccesible");
			System.exit(0);
		}

	}

	// metodo void invertirArchivo()
	public void crearArchivoInvertido() throws IOException {
		//1.- Abrir el archivo para leer y crear el vector ArrayList
		BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));
		ArrayList<String> vector = new ArrayList<String>();
		
		//2.- leo el archivo y escribo en el vector
		String linea="";
		linea=origen.readLine();
		while (linea!=null) {
			vector.add(linea);
			linea=origen.readLine();
		}
		
		//3.- Cerrar el archivo
		origen.close();
		
		// 4.- Abro el segundo archivo para escribir
		PrintWriter destino = new PrintWriter(new FileWriter("nombreInvertido.txt",false));
		
		// 5.- Leo el vector del reves y voy escribiendo
		for (int i=vector.size()-1;i>=0;i--) {
			destino.println(vector.get(i));
		}
		
		// 6.- Cierro el archivo
		destino.close();
			}
}
