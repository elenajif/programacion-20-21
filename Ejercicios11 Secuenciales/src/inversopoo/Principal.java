package inversopoo;

import java.io.IOException;

public class Principal {

	public static void main(String[] args) throws IOException {
		// declaro el nombre del archivo datos.txt
		String nombreArchivo="datos.txt";
		
		//crear un objeto de la clase Archivo
		Archivo unArchivo = new Archivo(nombreArchivo);
		
		//crear el archivo invertido
		unArchivo.crearArchivoInvertido();
		
		//visualizar archivo origen
		System.out.println("**** MOSTRANDO ARCHIVO ORIGEN ******");
		Archivo.visualizarArchivo(nombreArchivo);
		
		//visualizar archivo invertido (le pasamos directamente el nombre del archivo)
		//nombreInvertido.txt
		System.out.println("**** MOSTRANDO ARCHIVO DESTINO ******");
		Archivo.visualizarArchivo("nombreInvertido.txt");

	}

}
