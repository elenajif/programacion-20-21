package ejercicioscontrolexcepciones;

public class Ejercicio1SinExcepciones {

	public static void main(String[] args) {
		// casi todas las excepciones heredan de Exception
		// checked obligado controlarlas
		// ClassNotFoundException IOExcepcion IterruptedException...
		// unchecked no son de obligado cumplimiento
		// StringIndexOfBoundException ArrayIndexOfBoundException
		// NumberFormatException NullPointerException...

		int i;
		int valor;
		i = 3;
		valor = i / 0;
		System.out.println(valor);
	}

}
