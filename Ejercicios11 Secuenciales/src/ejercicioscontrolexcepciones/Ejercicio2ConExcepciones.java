package ejercicioscontrolexcepciones;

public class Ejercicio2ConExcepciones {

	public static void main(String[] args) {
		String cadenas[] = { "Cadena1", "Cadena2", "Cadena3", "Cadena4" };
		try {
			for (int i = 0; i <= 4; i++) {
				System.out.println(cadenas[i]);
			}

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Error: Fuera del indice del array");
		} finally {
			System.out.println("Esto se imprime siempre");
		}

	}

}
