package ejercicioscontrolexcepciones;

public class Ejercicio6ConTrazaExcepciones {
	public static int numerador = 10;
	public static Integer denominador = null;
	public static float division;

	public static void main(String[] args) {
		metodo1();
	}

	public static void metodo1() {
		try {
			division = numerador / denominador;
			System.out.println(division);			
		} catch (NullPointerException e) {
			division=1;
			System.out.println("Traza de errores");
			e.printStackTrace();
		}
	}

}
