package ejercicioscontrolexcepciones;

import java.util.Scanner;

public class Ejercicio5SinExcepciones {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un numero");
		double x=input.nextDouble();
		System.out.println("Raiz cuadrada de "+x+" = "+Math.sqrt(x));	
		input.close();

	}

}
