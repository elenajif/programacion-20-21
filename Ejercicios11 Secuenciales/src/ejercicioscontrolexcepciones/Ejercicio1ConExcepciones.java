package ejercicioscontrolexcepciones;

public class Ejercicio1ConExcepciones {

	public static void main(String[] args) {
		// casi todas las excepciones heredan de Exception
		// checked obligado controlarlas
		// ClassNotFoundException IOExcepcion IterruptedException...
		// unchecked no son de obligado cumplimiento
		// StringIndexOfBoundException ArrayIndexOfBoundException
		// NumberFormatException NullPointerException...

		try {
			int i;
			int valor;
			i = 3;
			valor = i / 0;
			System.out.println(valor);
		} catch (ArithmeticException e) {
			System.out.println(e.toString());
		} finally {
			// sirve para cerrar recursos
			System.out.println("Esto se imprime siempre");
		}
	}

}
