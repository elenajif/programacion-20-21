package ejercicioscontrolexcepciones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio7ThrowExcepciones {

	public static void main(String[] args) {
		lectura();
	}

	public static void lectura() {
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Dame una frase");
		try {
			String frase = br2.readLine();
			System.out.println("La frase es " + frase);			
		} catch (IOException e) {
			System.err.println("Se produjo un error de E/S");
		}
	}

}
