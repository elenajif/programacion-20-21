package ejercicioscontrolexcepciones;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ejercicio5ConExcepciones {

	public static void main(String[] args) {
		try {
			Scanner input = new Scanner(System.in);
			System.out.println("Dame un numero");
			double x=input.nextDouble();
			System.out.println("Raiz cuadrada de "+x+" = "+Math.sqrt(x));	
			input.close();
			
		} catch (InputMismatchException e) {
			System.out.println("error de entrada de datos");
		}

	}

}
