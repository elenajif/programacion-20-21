package clases;

public class Carretillero extends Personal{

	private static final long serialVersionUID = 3915527913264256570L;
	private float costeHora;
	
	public Carretillero(String dni, String nombre, float costeHora) {
		super(dni, nombre);
		this.costeHora = costeHora;
	}


	public float getCosteHora() {
		return costeHora;
	}

	public void setCosteHora(float costeHora) {
		this.costeHora = costeHora;
	}

	@Override
	public int compareTo(Personal o) {
		return getDni().compareTo(o.getDni());
	}

	@Override
	public String toString() {
		return super.toString() + costeHora;
	}
	
	

}
