package principal;

import comparableserializable.GrupoPersonas;

public class Principal {

	public static void main(String[] args) {
		GrupoPersonas grupo = new GrupoPersonas();
		
		grupo.altaPersona("Mario", 187, 22);
		grupo.altaPersona("Pepe", 176, 52);
		grupo.altaPersona("Manuel", 158, 27);
		grupo.altaPersona("David", 164, 25);
		grupo.altaPersona("Maria", 159, 63);
		
		System.out.println();
		System.out.println("Guardando datos....");
		grupo.guardarDatos();
		System.out.println("Cargando datos....");
		grupo.cargarDatos();
		System.out.println();
		
		System.out.println("Mostramos datos");
		grupo.listarPersonas();
	}

}
