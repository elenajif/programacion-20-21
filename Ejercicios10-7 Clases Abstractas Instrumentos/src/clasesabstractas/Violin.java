package clasesabstractas;

public class Violin extends Instrumento{

	public Violin() {
		this.tipo="Violin";
	}
	
	@Override
	public void tocar() {
		System.out.println("Tocar el violin");
	}

}
