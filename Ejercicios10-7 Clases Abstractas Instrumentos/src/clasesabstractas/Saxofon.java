package clasesabstractas;

public class Saxofon extends Instrumento {
	
	public Saxofon() {
		this.tipo="Saxofon";
	}

	@Override
	public void tocar() {
		System.out.println("Tocar el saxofon");
		
	}

}
