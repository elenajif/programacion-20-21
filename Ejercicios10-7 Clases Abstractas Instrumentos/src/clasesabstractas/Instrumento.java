package clasesabstractas;

public abstract class Instrumento {
	//la abstraccion permite trabajar con la herencia y el polimorfismo
	// una abstracta es similar a una clase normal salvo que uno de sus m�todos es abstracto
	// no puede ser instanciada (new)
	// puede tener un metodo abstracto y otros no
	// la subclase que herede de una clase abstracta esta obligada a imlpementar TODOS los metodos
	
	//atributos
	public String tipo;
	
	//metodo abstracto
	public abstract void tocar();

}
