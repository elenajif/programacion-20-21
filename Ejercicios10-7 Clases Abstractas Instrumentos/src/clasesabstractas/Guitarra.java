package clasesabstractas;

public class Guitarra extends Instrumento {
	
	public Guitarra() {
		this.tipo="Guitarra";
	}

	@Override
	public void tocar() {
		System.out.println("Tocar la guitarra");
	}

}
